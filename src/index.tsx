import React from 'react'
import ReactDOM from 'react-dom'
import HttpsRedirect from 'react-https-redirect'
import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import ThemeDark from './assets/styles/themes/ThemeDark'
import SplashScreen from './views/infrastructure/splash-screen/SplashScreenView'
// import * as serviceWorker from './service-worker'
const WidgetDivs = document.querySelectorAll('.reddit_widget')

// Inject our React App into each
WidgetDivs.forEach(Div => {
  ReactDOM.render(
    <HttpsRedirect>
    <ThemeProvider theme={ThemeDark}>
      <React.Fragment>
        <CssBaseline />
        <SplashScreen />
      </React.Fragment>
    </ThemeProvider>
  </HttpsRedirect>,
    Div
  );
})
// ReactDOM.render(
//   <HttpsRedirect>
//     <ThemeProvider theme={ThemeDark}>
//       <React.Fragment>
//         <CssBaseline />
//         <SplashScreen />
//       </React.Fragment>
//     </ThemeProvider>
//   </HttpsRedirect>,
//   document.getElementById('root')
// )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
/*
serviceWorker.register({
  onUpdate: registration => {
    const waitingServiceWorker = registration.waiting

    if (waitingServiceWorker) {
      window.addEventListener('message', (event: MessageEvent) => {
        if (event.data && event.data.type === 'SKIP_WAITING' && navigator.serviceWorker.controller) {
          waitingServiceWorker.postMessage({ type: 'SKIP_WAITING' })
        }
      })
    }
  }
})
*/
