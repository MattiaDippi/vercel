import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'
import { smallDevices } from './../../assets/styles/mixins/Breakpoints'
import ColorUtils from './../../infrastructure/utils/ColorUtils'

const NotifierStyles = (theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.background.default,
      color: theme.palette.text.primary,
      borderRadius: 0,
      boxShadow: `0 0 8px 0 ${ColorUtils.hexToRgbString(theme.palette.text.primary, 0.3)}`
    },
    content: {
      minWidth: 400,
      maxWidth: 600,
      minHeight: 48,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      paddingRight: 12,
      '&.default': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.info.main, 0.1)
      },
      '&.info': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.info.main, 0.1)
      },
      '&.success': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.success.main, 0.1)
      },
      '&.warning': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.warning.main, 0.1)
      },
      '&.error': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.error.main, 0.1)
      }
    },
    color: {
      width: 4,
      alignSelf: 'stretch',
      '&.default': {
        backgroundColor: theme.palette.info.main
      },
      '&.info': {
        backgroundColor: theme.palette.info.main
      },
      '&.success': {
        backgroundColor: theme.palette.success.main
      },
      '&.warning': {
        backgroundColor: theme.palette.warning.main
      },
      '&.error': {
        backgroundColor: theme.palette.error.main
      }
    },
    icon: {
      marginLeft: 24,
      marginRight: 12,
      '&.default': {
        '& path': {
          fill: theme.palette.info.main
        }
      },
      '&.info': {
        '& path': {
          fill: theme.palette.info.main
        }
      },
      '&.success': {
        '& path': {
          fill: theme.palette.success.main
        }
      },
      '&.warning': {
        '& path': {
          fill: theme.palette.warning.main
        }
      },
      '&.error': {
        '& path': {
          fill: theme.palette.error.main
        }
      }
    },
    messageContainer: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
      padding: '8px 0 8px 12px'
    },
    messageTitle: {
      fontWeight: 'bold',
      marginBottom: 2
    },
    actionButton: {
      display: 'flex',
      flexDirection: 'row',
      marginLeft: 24
    },
    closeButton: {
      padding: 6,
      marginLeft: 16
    },
    [smallDevices(theme)]: {
      container: {
        minWidth: '100%',
        maxWidth: '100%'
      }
    }
  })

export default NotifierStyles
