// TODO: move Notifier in UI package
import React, { Component } from 'react'
import {
  NotificationMessageEnqueuedEventArgs,
  NotificationMessage,
  HideNotificationMessageEnqueuedEventArgs
} from 'mw-react-web-app-infrastructure'
import { Snackbar, Button, IconButton, Typography, Paper } from '@material-ui/core'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import App from './../../App'
import NotifierStyles from './Notifier.styles'
import Images from './../../assets/images'

class NotificationMessageQueueItem {
  key: string

  message: NotificationMessage

  constructor(key: string, message: NotificationMessage) {
    this.key = key
    this.message = message
  }
}

const notificationIcons: any = {
  success: Images.RCiconNotificationSuccess,
  warning: Images.RCiconNotificationWarning,
  error: Images.RCiconNotificationError,
  info: Images.RCiconNotificationInfo,
  default: undefined
}

interface NotifierState {
  open: boolean
  notification?: NotificationMessageQueueItem
}

class Notifier extends Component<WithStyles<typeof NotifierStyles>, NotifierState> {
  notificationMessageEnqueuedEventToken?: string

  hideNotificationMessageEnqueuedEventToken?: string

  notificationMessageQueue: NotificationMessageQueueItem[] = []

  constructor(props: WithStyles<typeof NotifierStyles>) {
    super(props)

    this.state = {
      open: false
    }
  }

  componentDidMount() {
    const { events } = App

    this.notificationMessageEnqueuedEventToken = events.subscribeNotificationMessageEnqueued(
      this.onNotificationMessageEnqueued
    )
    this.hideNotificationMessageEnqueuedEventToken = events.subscribeHideNotificationMessageEnqueued(
      this.onHideNotificationMessageEnqueued
    )
  }

  componentWillUnmount() {
    const { events } = App

    if (this.notificationMessageEnqueuedEventToken) {
      events.unsubscribe(this.notificationMessageEnqueuedEventToken)
    }
    if (this.hideNotificationMessageEnqueuedEventToken) {
      events.unsubscribe(this.hideNotificationMessageEnqueuedEventToken)
    }
  }

  onNotificationMessageEnqueued = (eventArgs: NotificationMessageEnqueuedEventArgs) => {
    this.showNotification(eventArgs.message)
  }

  onHideNotificationMessageEnqueued = (eventArgs: HideNotificationMessageEnqueuedEventArgs) => {
    this.hideNotification()
  }

  showNotification = (message: NotificationMessage) => {
    this.notificationMessageQueue.push(new NotificationMessageQueueItem(new Date().getTime().toString(), message))

    const { open } = this.state

    if (open) {
      // Immediately begin dismissing current message to start showing new one.
      this.setState({ open: false })
    } else {
      this.processQueue()
    }
  }

  hideNotification = () => {
    this.setState({ open: false })
  }

  handleNotificationClose = (event: React.FormEvent<HTMLFormElement>, reason: string) => {
    if (reason === 'clickaway') {
      return
    }

    this.hideNotification()
  }

  handleNotificationExited = () => {
    this.processQueue()
  }

  handleNotificationAction = (action: (() => void) | undefined) => {
    this.hideNotification()

    if (action) {
      action()
    }
  }

  processQueue = () => {
    if (this.notificationMessageQueue.length > 0) {
      this.setState({
        notification: this.notificationMessageQueue.shift(),
        open: true
      })
    }
  }

  render() {
    const { open, notification } = this.state
    if (notification) {
      const { classes } = this.props
      const { message } = notification
      const autoHideDuration =
        message.options && message.options.autoHide ? message.options.autoHideDuration : undefined
      const title = message.options && message.options.title ? message.options.title : null
      const text = message.text
      const Icon = notificationIcons[message.type]

      return (
        <Snackbar
          key={notification.key}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
          open={open}
          autoHideDuration={autoHideDuration}
          onClose={this.handleNotificationClose}
          onExited={this.handleNotificationExited}
          disableWindowBlurListener>
          <Paper className={classes.container} elevation={3}>
            <div className={clsx(classes.content, message.type)}>
              <div className={clsx(classes.color, message.type)} />
              {Icon && <Icon className={clsx(classes.icon, message.type)} />}
              <div className={classes.messageContainer}>
                {title && (
                  <Typography variant="body1" component="div" className={classes.messageTitle}>
                    {title}
                  </Typography>
                )}
                <Typography variant="body1" component="div">
                  {text}
                </Typography>
              </div>
              {message.options && message.options.action && message.options.action.handler && (
                <Button
                  key="action"
                  variant="text"
                  color="primary"
                  size="small"
                  className={classes.actionButton}
                  onClick={() =>
                    this.handleNotificationAction(
                      message.options && message.options.action && message.options.action.handler
                        ? message.options.action.handler
                        : undefined
                    )
                  }>
                  {message.options.action.text}
                </Button>
              )}
              {message.options && message.options.showCloseButton && (
                <IconButton
                  aria-label="Close"
                  color="inherit"
                  onClick={() => this.hideNotification()}
                  className={classes.closeButton}>
                  <Images.RCiconClose />
                </IconButton>
              )}
            </div>
          </Paper>
        </Snackbar>
      )
    } else {
      return null
    }
  }
}

export default withStyles(NotifierStyles)(Notifier)
