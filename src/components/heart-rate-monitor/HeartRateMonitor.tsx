import React, { useCallback, useEffect, useRef, useState } from 'react'
import useStyles from './HeartRateMonitor.styles'
import App from 'App'
import clsx from 'clsx'
import { Tooltip, Typography } from '@material-ui/core'

enum HeartRateZoneType {
  VeryLight = 'VeryLight',
  Light = 'Light',
  Moderate = 'Moderate',
  Hard = 'Hard',
  VeryHard = 'VeryHard'
}

interface HeartRateZone {
  type: HeartRateZoneType
  min: number
  max: number
  color: string
  i18nKey: string
}

const getUserHeartRateZones = (userMaxHeartRate: number): HeartRateZone[] => {
  const zones: HeartRateZone[] = []

  let tmpHr1 = 0
  let tmpHr2 = Math.round(0.6 * userMaxHeartRate)
  zones.push({ type: HeartRateZoneType.VeryLight, min: tmpHr1, max: tmpHr2, color: '#005DA9', i18nKey: 'S1037' })

  tmpHr1 = tmpHr2 + 1
  tmpHr2 = Math.round(0.7 * userMaxHeartRate)
  zones.push({ type: HeartRateZoneType.Light, min: tmpHr1, max: tmpHr2, color: '#25DD70', i18nKey: 'S1036' })

  tmpHr1 = tmpHr2 + 1
  tmpHr2 = Math.round(0.8 * userMaxHeartRate)
  zones.push({ type: HeartRateZoneType.Moderate, min: tmpHr1, max: tmpHr2, color: '#ffDD00', i18nKey: 'S1035' })

  tmpHr1 = tmpHr2 + 1
  tmpHr2 = Math.round(0.9 * userMaxHeartRate)
  zones.push({ type: HeartRateZoneType.Hard, min: tmpHr1, max: tmpHr2, color: '#FC8F00', i18nKey: 'S1034' })

  tmpHr1 = tmpHr2 + 1
  tmpHr2 = 220
  zones.push({ type: HeartRateZoneType.VeryHard, min: tmpHr1, max: tmpHr2, color: '#CE2A29', i18nKey: 'S1033' })

  return zones
}

const getHeartRateZone = (heartRate?: number, heartRateZones?: HeartRateZone[]): HeartRateZone | undefined => {
  let heartRateZone: HeartRateZone | undefined

  if (heartRate && heartRateZones && heartRateZones.length > 0) {
    heartRateZone = heartRateZones.find(item => heartRate >= item.min && heartRate <= item.max)

    if (!heartRateZone) {
      const firstHeartRateZone = heartRateZones[0]
      const lasHeartRateZone = heartRateZones[heartRateZones.length - 1]

      if (heartRate > 0 && heartRate < firstHeartRateZone.min) {
        heartRateZone = firstHeartRateZone
      } else if (heartRate > firstHeartRateZone.max) {
        heartRateZone = lasHeartRateZone
      }
    }
  }

  return heartRateZone
}

interface HeartRateMonitorProps {
  userMaxHeartRate?: number
  className?: string
  onChangeHeartRate?: (heartRate: number | undefined) => void
}

const HeartRateMonitor: React.FC<HeartRateMonitorProps> = (props: HeartRateMonitorProps) => {
  const { userMaxHeartRate, className, onChangeHeartRate } = props
  const [heartRateZones, setHeartRateZones] = useState<HeartRateZone[]>()
  const [currentHeartRate, setCurrentHeartRate] = useState<number>()
  const [bluetoothConnected, setBuetoothConnected] = useState(false)
  const bluetoothCharacteristic = useRef<BluetoothRemoteGATTCharacteristic>()

  const { i18n } = App
  const styles = useStyles()

  const handleHeartRateNotification = useCallback(
    (event: any) => {
      if (event && event.target && event.target.value) {
        let heartRate = event.target.value.getUint8(1)

        if (heartRate < 0) {
          heartRate = undefined
        }

        setCurrentHeartRate(heartRate)

        if (onChangeHeartRate) {
          onChangeHeartRate(heartRate)
        }
      }
    },
    [onChangeHeartRate]
  )

  const bluetoothConnect = useCallback(async (): Promise<boolean> => {
    let result = false
    const { logger } = App

    if (!navigator.bluetooth) {
      logger.error('Bluetooth not supported by this browser.')
    } else {
      try {
        const device = await navigator.bluetooth.requestDevice({
          filters: [{ services: ['heart_rate'] }],
          acceptAllDevices: false
        })

        if (device && device.gatt) {
          const server = await device.gatt.connect()

          if (server) {
            const service = await server.getPrimaryService('heart_rate')

            if (service) {
              bluetoothCharacteristic.current = await service.getCharacteristic('heart_rate_measurement')

              if (bluetoothCharacteristic.current) {
                bluetoothCharacteristic.current.addEventListener(
                  'characteristicvaluechanged',
                  handleHeartRateNotification
                )
                await bluetoothCharacteristic.current.startNotifications()
                result = true
              }
            }
          }
        }
      } catch (error) {
        logger.error('An error occured connecting to bluetooth device.', error)
      }
    }

    return result
  }, [handleHeartRateNotification])

  useEffect(() => {
    if (userMaxHeartRate) {
      // Update heart rate zones basing on user max heart rate.
      const zones = getUserHeartRateZones(userMaxHeartRate)
      setHeartRateZones(zones)
    } else {
      setHeartRateZones(undefined)
    }
  }, [userMaxHeartRate])

  useEffect(() => {
    if (!bluetoothConnected) {
      const initialize = async () => {
        const result = await bluetoothConnect()
        setBuetoothConnected(result)
      }

      initialize()
    }

    return () => {
      if (bluetoothConnected && bluetoothCharacteristic.current) {
        bluetoothCharacteristic.current.stopNotifications()
        bluetoothCharacteristic.current.removeEventListener('characteristicvaluechanged', handleHeartRateNotification)
        bluetoothCharacteristic.current = undefined
      }
    }
  }, [bluetoothConnect, bluetoothConnected, handleHeartRateNotification])

  const currentHeartRateZone = getHeartRateZone(currentHeartRate, heartRateZones)

  return (
    <div className={clsx(styles.root, className)}>
      {heartRateZones && heartRateZones.length > 0 && (
        <div className={styles.heartRateZonesLegendContainer}>
          {heartRateZones.map(item => {
            return (
              <Tooltip key={item.type} title={`${i18n.t(item.i18nKey)} (${item.min} - ${item.max})`}>
                <div className={styles.heartRateZonesLegendItem} style={{ backgroundColor: item.color }}></div>
              </Tooltip>
            )
          })}
        </div>
      )}
      <div className={styles.heartRateContainer}>
        <Typography variant="subtitle1" className={styles.heartRateTitle} component="div" noWrap>
          {i18n.t('physicalpropertydisplayname_hr')}
        </Typography>
        <Typography variant="h2" className={styles.heartRateValue} component="div">
          {currentHeartRate && currentHeartRate > 0 ? currentHeartRate : '-'}
        </Typography>
        <Typography variant="subtitle2" className={styles.heartRateUnitOfMeasure} color="textSecondary" component="div">
          {i18n.t('physicalpropertyum_hr')}
        </Typography>
        {heartRateZones && heartRateZones.length > 0 && (
          <div className={styles.heartRateZoneContainer}>
            {heartRateZones.map(item => {
              return (
                <div
                  key={item.type}
                  className={styles.heartRateZoneItem}
                  style={{ backgroundColor: item.type === currentHeartRateZone?.type ? item.color : undefined }}></div>
              )
            })}
          </div>
        )}
      </div>
    </div>
  )
}

export default HeartRateMonitor
