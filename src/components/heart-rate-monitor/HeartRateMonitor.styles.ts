import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'
import ColorUtils from 'infrastructure/utils/ColorUtils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: 140
    },
    heartRateZonesLegendContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 16
    },
    heartRateZonesLegendItem: {
      width: 16,
      height: 16,
      margin: 3
    },
    heartRateContainer: {
      display: 'flex',
      flexDirection: 'column',
      padding: 8,
      backgroundColor: ColorUtils.hexToRgbString(theme.palette.background.paper, 0.25)
    },
    heartRateTitle: {
      textAlign: 'center'
    },
    heartRateValue: {
      textAlign: 'center',
      lineHeight: '40px'
    },
    heartRateUnitOfMeasure: {
      textAlign: 'center'
    },
    heartRateZoneContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 8
    },
    heartRateZoneItem: {
      width: 16,
      height: 6,
      margin: '0 3px',
      backgroundColor: theme.palette.text.secondary
    }
  })
)

export default useStyles
