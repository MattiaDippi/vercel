import React, { useCallback, useState } from 'react'
import { Button, Typography, Tooltip, IconButton, Popover } from '@material-ui/core'
import clsx from 'clsx'
import Images from 'assets/images'
import useStyles, { ButtonIconSize } from './DropdownFilter.styles'

export interface DropdownFilterClasses {
  root?: string
  popover?: string
}

interface DropdownFilterProps {
  id: string
  open: boolean
  Avatar?: React.FunctionComponent
  placeholder?: string
  valueText?: string
  tooltip?: string
  classes?: DropdownFilterClasses
  className?: string
  maxPopoverHeight?: number
  clearable?: boolean
  children: any
  onOpen?: (id?: string) => void
  onClose?: (id?: string) => void
  onClear?: (id?: string) => void
}

const DropdownFilter: React.FC<DropdownFilterProps> = (props: DropdownFilterProps) => {
  const {
    id,
    open,
    Avatar,
    placeholder,
    valueText,
    tooltip,
    classes,
    className,
    clearable = true,
    maxPopoverHeight = 400,
    children,
    onOpen,
    onClose,
    onClear
  } = props
  const [popverAnchorEl, setPopoverAnchorEl] = useState<HTMLButtonElement | null>(null)
  const popoverFilterId = `dropdownfilter-popover-${id}`
  const styles = useStyles()

  const onClickButton = useCallback(
    (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      setPopoverAnchorEl(event.currentTarget)

      if (onOpen) {
        onOpen(id)
      }
    },
    [id, onOpen]
  )

  const onClickClearButton = useCallback(
    (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      event.stopPropagation()

      if (onClear) {
        onClear(id)
      }
    },
    [id, onClear]
  )

  const onClosePopover = useCallback(() => {
    setPopoverAnchorEl(null)

    if (onClose) {
      onClose(id)
    }
  }, [id, onClose])

  return (
    <React.Fragment>
      <Tooltip title={tooltip || ''}>
        <Button
          variant="outlined"
          size="small"
          classes={{
            root: clsx(
              styles.buttonRoot,
              valueText ? styles.filterApplied : undefined,
              className,
              classes && classes.root
            )
          }}
          aria-describedby={popoverFilterId}
          onClick={onClickButton}>
          {Avatar && (
            <div className={styles.avatar}>
              <Avatar />
            </div>
          )}
          <Typography className={styles.text} variant="body1" noWrap>
            {valueText || placeholder}
          </Typography>

          {clearable && valueText ? (
            <IconButton className={styles.clearButton} component="a" onClick={onClickClearButton}>
              <Images.RCiconClear width={ButtonIconSize} height={ButtonIconSize} />
            </IconButton>
          ) : (
            <Images.RCiconArrowDown width={ButtonIconSize} height={ButtonIconSize} />
          )}
        </Button>
      </Tooltip>
      <Popover
        id={popoverFilterId}
        open={open}
        anchorEl={popverAnchorEl}
        onClose={onClosePopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        transitionDuration={0}
        PaperProps={{
          style: { maxHeight: maxPopoverHeight },
          classes: { root: clsx(styles.popoverRoot, classes && classes.popover), elevation8: styles.popoverElevation }
        }}>
        {children}
      </Popover>
    </React.Fragment>
  )
}

export default DropdownFilter
