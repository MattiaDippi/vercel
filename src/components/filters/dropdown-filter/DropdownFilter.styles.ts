import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'
import ColorUtils from 'infrastructure/utils/ColorUtils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonRoot: {
      width: 240,
      padding: '0 8px 0 8px'
    },
    avatar: {
      marginRight: 8,
      display: 'flex',
      alignItems: 'center'
    },
    text: {
      flex: 1,
      textAlign: 'left',
      marginTop: -2
    },
    filterApplied: {
      border: `1px solid ${theme.palette.primary.main}`,
      '&:hover': {
        border: `1px solid ${theme.palette.primary.main}`
      }
    },
    clearButton: {
      padding: 3,
      marginRight: -4
    },
    popoverRoot: {
      width: 360,
      display: 'flex',
      flexDirection: 'column',
      overflowY: 'auto'
    },
    popoverElevation: {
      boxShadow: `0 20px 32px 0 ${ColorUtils.hexToRgbString(theme.palette.text.primary, 0.3)}`
    }
  })
)

const ButtonIconSize = 20

export { ButtonIconSize }

export default useStyles
