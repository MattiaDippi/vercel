import React, { useCallback } from 'react'
import { Button, Typography, Tooltip, IconButton, Badge } from '@material-ui/core'
import clsx from 'clsx'
import Images from 'assets/images'
import useStyles from './MoreFiltersButton.styles'

interface MoreFiltersButtonProps {
  text?: string
  tooltip?: string
  filtersApplied?: boolean
  className?: string
  disabled?: boolean
  onClick?: () => void
}

const MoreFiltersButton: React.FC<MoreFiltersButtonProps> = (props: MoreFiltersButtonProps) => {
  const { text, tooltip, filtersApplied, className, disabled, onClick } = props
  const styles = useStyles()

  const renderIcon = useCallback(
    (showBadge: boolean): React.ReactNode => {
      return (
        <Badge color="primary" variant="dot" invisible={!showBadge}>
          <Images.RCiconFilter className={styles.icon} />
        </Badge>
      )
    },
    [styles.icon]
  )

  return text ? (
    <Tooltip title={tooltip || ''}>
      <div>
        <Button
          className={clsx(styles.button, className)}
          variant="text"
          size="small"
          onClick={onClick}
          disabled={disabled}>
          {renderIcon(Boolean(filtersApplied))}
          <Typography variant="subtitle1" className={styles.text} noWrap>
            {text}
          </Typography>
        </Button>
      </div>
    </Tooltip>
  ) : (
    <Tooltip title={tooltip || ''}>
      <div>
        <IconButton className={className} onClick={onClick} disabled={disabled}>
          {renderIcon(Boolean(filtersApplied))}
        </IconButton>
      </div>
    </Tooltip>
  )
}

export default MoreFiltersButton
