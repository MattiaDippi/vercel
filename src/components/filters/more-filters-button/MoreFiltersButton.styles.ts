import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      minWidth: 'unset'
    },
    text: {
      marginLeft: 8,
      marginTop: -2,
      textAlign: 'left'
    },
    icon: {
      '& path': {
        fill: theme.palette.icon.main
      }
    }
  })
)

export default useStyles
