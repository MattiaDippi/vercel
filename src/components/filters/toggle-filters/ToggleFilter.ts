interface ToggleFilter {
  value: string
  text: string
  tooltip?: string
}

export default ToggleFilter
