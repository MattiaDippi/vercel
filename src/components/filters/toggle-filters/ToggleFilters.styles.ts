import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'
import ColorUtils from 'infrastructure/utils/ColorUtils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toggleButtonRoot: {
      height: 32,
      minWidth: 80,
      maxWidth: 240,
      padding: '0',
      color: theme.palette.text.primary,
      borderRadius: '16px !important',
      marginLeft: '0 !important',
      border: `1px solid ${theme.palette.border.main} !important`,
      marginRight: 16,
      '&:not(:first-child)': {
        border: `1px solid ${theme.palette.border.main}`
        // marginLeft: '16px !important'
      },
      '&:hover': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.text.primary, 0.1)
      }
    },
    toggleButtonSelected: {
      borderColor: `${theme.palette.highlight} !important`,
      backgroundColor: `${theme.palette.highlight} !important`
    },
    container: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    labelContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      flex: 1,
      padding: '0 12px'
    },
    label: {
      marginTop: -2,
      maxWidth: 216
    },
    smallerLabel: {
      maxWidth: 188
    },
    clearButton: {
      padding: 0,
      marginRight: 4,
      marginLeft: -8
    }
  })
)

export default useStyles
