import React, { useCallback } from 'react'
import ToggleFilter from './ToggleFilter'
import { Typography, Tooltip, IconButton } from '@material-ui/core'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import ToggleButton from '@material-ui/lab/ToggleButton'
import clsx from 'clsx'
import useStyles from './ToggleFilters.styles'
import Images from 'assets/images'

interface ToggleFiltersProps {
  filters: ToggleFilter[]
  selectedValues: string[]
  exclusive?: boolean
  onChange: (selectedFilters: string[], event?: React.MouseEvent<HTMLElement, MouseEvent>) => void
  className?: string
  clearFilters?: boolean
  onClearFilter?: (selectedFilter: string) => void
}

const ToggleFilters: React.FC<ToggleFiltersProps> = (props: ToggleFiltersProps) => {
  const { filters, selectedValues, className, onChange } = props
  const styles = useStyles()

  const onChangeToggleButton = useCallback(
    (event: React.MouseEvent<HTMLElement, MouseEvent>, value: any) => {
      event.stopPropagation()
      onChange(value as string[], event)
    },
    [onChange]
  )

  const onClear = (selectedFilter: string, event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    if (props.onClearFilter) {
      event.stopPropagation()
      props.onClearFilter(selectedFilter)
    }
  }

  return (
    <ToggleButtonGroup className={className} value={selectedValues} onChange={onChangeToggleButton}>
      {filters.map(filter => {
        const showClearButton = props.clearFilters && selectedValues.lastIndexOf(filter.value) > -1

        return (
          <ToggleButton
            value={filter.value}
            classes={{ root: styles.toggleButtonRoot, selected: styles.toggleButtonSelected }}
            key={filter.value}>
            <div className={styles.container}>
              <div className={styles.labelContainer}>
                <Tooltip title={filter.tooltip || ''}>
                  <Typography
                    className={clsx(styles.label, showClearButton && styles.smallerLabel)}
                    variant="body1"
                    noWrap>
                    {filter.text}
                  </Typography>
                </Tooltip>
              </div>

              {showClearButton && (
                <IconButton
                  component="a"
                  className={styles.clearButton}
                  onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => onClear(filter.value, event)}>
                  <Images.RCiconClear />
                </IconButton>
              )}
            </div>
          </ToggleButton>
        )
      })}
    </ToggleButtonGroup>
  )
}

export default ToggleFilters
