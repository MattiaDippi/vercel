import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      minHeight: 51,
      minWidth: 160
    },
    label: {
      textTransform: 'uppercase',
      color: theme.palette.text.secondary,
      transform: 'translate(0, 0) scale(0.857)',
      transformOrigin: 'top left'
    },
    value: {
      minHeight: 36,
      padding: '6px 0 8px',
      display: 'flex',
      alignItems: 'flex-end'
    }
  })
)

export default useStyles
