import React from 'react'
import useStyles from './ReadOnlyTextField.styles'
import { Typography } from '@material-ui/core'
import clsx from 'clsx'

interface ReadOnlyTextFieldClasses {
  root?: string
  label?: string
  value?: string
}

interface ReadOnlyTextFieldProps {
  label: string
  value: string
  classes?: ReadOnlyTextFieldClasses
  className?: string
}

const ReadOnlyTextField: React.FC<ReadOnlyTextFieldProps> = (props: ReadOnlyTextFieldProps) => {
  const { label, value, classes, className } = props
  const styles = useStyles()
  return (
    <div className={clsx(styles.container, classes && classes.root, className)}>
      <Typography variant="subtitle1" component="div" className={clsx(styles.label, classes && classes.label)}>
        {label}
      </Typography>
      <Typography variant="body1" component="div" className={clsx(styles.value, classes && classes.value)}>
        {value}
      </Typography>
    </div>
  )
}

export default ReadOnlyTextField
