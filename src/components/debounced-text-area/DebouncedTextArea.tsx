import React, { useCallback, useState } from 'react'

import { TextField, TextFieldProps } from '@material-ui/core'
import CommonUtils from './../../infrastructure/utils/CommonUtils'

type DebouncedTextAreaProps = TextFieldProps & {
  onChangeValue: (value: string, name?: string) => void
}

const DebouncedTextArea: React.FC<DebouncedTextAreaProps> = (props: DebouncedTextAreaProps) => {
  const { onChangeValue, name, ...otherProps } = props

  const [localValue, setLocalValue] = useState<string>(props.value as string)

  const debouncedOnChange = useCallback(CommonUtils.debounce(onChangeValue, 250), [onChangeValue])

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement>) => {
      const text = event.target.value
      setLocalValue(text)
      debouncedOnChange(text, name)
    },
    [debouncedOnChange, name]
  )

  return <TextField multiline {...otherProps} name={name} onChange={handleChange} value={localValue} />
}

export default DebouncedTextArea
