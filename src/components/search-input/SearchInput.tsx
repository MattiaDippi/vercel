import React, { Component, KeyboardEvent, ChangeEvent } from 'react'
import { Paper, InputBase, IconButton, CircularProgress } from '@material-ui/core'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Images from 'assets/images'
import SearchInputStyles, { ButtonIconSize } from './SearchInput.styles'
import CommonUtils from './../../infrastructure/utils/CommonUtils'
import { Cancelable } from 'lodash'

export interface SearchInputProps {
  text?: string
  placeholderText?: string
  debounce?: boolean
  debounceDelay?: number
  autoFocus?: boolean
  showProgress?: boolean
  className?: string
  disabled?: boolean
  onChange?(text: string): void
  onClear?(): void
  onSearch?(text: string): void
  onBlur?(): void
  onFocus?(): void
}

interface SearchInputState {
  text: string
}

class SearchInput extends Component<SearchInputProps & WithStyles<typeof SearchInputStyles>, SearchInputState> {
  static defaultProps = {
    debounce: false,
    debounceDelay: 500,
    autoFocus: false,
    showProgress: false
  }

  searchedText: string

  debouncedSearch: (() => void) & Cancelable

  constructor(props: SearchInputProps & WithStyles<typeof SearchInputStyles>) {
    super(props)

    this.searchedText = ''

    this.state = {
      text: props.text || ''
    }

    // Debounce search in order to avoid multiple calls.
    this.debouncedSearch = CommonUtils.debounce(this.search, props.debounceDelay)
  }

  componentDidUpdate(prevProps: SearchInputProps & WithStyles<typeof SearchInputStyles>) {
    if (this.props.text !== undefined && this.props.text !== prevProps.text && this.props.text !== this.state.text) {
      this.setState({ text: this.props.text })
    }
  }

  search = () => {
    const { onChange } = this.props

    if (onChange) {
      const { text } = this.state

      onChange(text)
    }
  }

  onTextChange = (evt: ChangeEvent<HTMLInputElement>) => {
    const { debounce } = this.props
    this.setState({ text: evt.target.value }, () => (debounce ? this.debouncedSearch() : this.search()))
  }

  onButtonClick = () => {
    const { text } = this.state

    if (text.length > 0) {
      this.clearSearch()
    }
  }

  onKeyUp = (event: KeyboardEvent<HTMLInputElement>) => {
    const { text } = this.state
    const { debounce } = this.props

    if (event.keyCode === 27) {
      // Clear search on ESC key pressed.
      this.clearSearch()
    } else if (event.keyCode === 13) {
      if (text !== this.searchedText) {
        // Immediatle search on ENTER key pressed.
        this.searchedText = text

        if (debounce) {
          this.debouncedSearch.cancel()
        }

        const { onSearch } = this.props

        if (onSearch) {
          onSearch(text)
        }
      }
    }
  }

  clearSearch = () => {
    const { text } = this.state

    if (text !== '') {
      this.setState({ text: '' })
      this.searchedText = ''

      const { onClear } = this.props

      if (onClear) {
        onClear()
      }
    }
  }

  onBlur = () => {
    const { onBlur } = this.props
    if (onBlur) onBlur()
  }

  onFocus = () => {
    const { onFocus } = this.props
    if (onFocus) onFocus()
  }

  render() {
    const { placeholderText, autoFocus, showProgress, className, classes, disabled } = this.props
    const { text } = this.state

    return (
      <Paper
        className={clsx(classes.container, className, text.length > 0 ? classes.withText : undefined)}
        elevation={0}>
        <InputBase
          className={classes.inputBase}
          placeholder={placeholderText}
          value={text}
          onChange={this.onTextChange}
          onKeyUp={this.onKeyUp}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          autoFocus={autoFocus}
          classes={{ input: classes.input }}
          fullWidth
          disabled={disabled}
        />
        {showProgress && <CircularProgress size={14} />}
        <IconButton className={classes.button} onClick={this.onButtonClick} disabled={disabled}>
          {text.length > 0 ? (
            <Images.RCiconClear width={ButtonIconSize} height={ButtonIconSize} />
          ) : (
            <Images.RCiconSearch width={ButtonIconSize} height={ButtonIconSize} />
          )}
        </IconButton>
      </Paper>
    )
  }
}

export default withStyles(SearchInputStyles)(SearchInput)
