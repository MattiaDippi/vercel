import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'
import ColorUtils from 'infrastructure/utils/ColorUtils'

const SearchInputStyles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      backgroundColor: ColorUtils.hexToRgbString(theme.palette.text.primary, 0.05),
      alignItems: 'center',
      height: 32,
      border: '1px solid transparent'
    },
    withText: {
      backgroundColor: theme.palette.background.default,
      border: `solid 1px ${theme.palette.primary.main}`
    },
    inputBase: {
      flex: 1,
      height: 32
    },
    input: {
      paddingLeft: 6
    },
    button: {
      padding: 4,
      margin: 2
    }
  })

const ButtonIconSize = 20

export { ButtonIconSize }
export default SearchInputStyles
