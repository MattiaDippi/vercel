import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'
import Images from './../../assets/images/index'
import { BreakpointType, largeDevices } from './../../assets/styles/mixins/Breakpoints'

const FooterStyles = (theme: Theme) =>
  createStyles({
    container: {
      width: '100%',
      height: 32,
      display: 'flex',
      alignItems: 'center',
      backgroundColor: 'rgba(255,255,255,0.5)'
    },
    logoContainer: {
      height: '100%'
    },
    logo: {
      backgroundImage: `url('${Images.logoTechnogym}')`,
      height: '100%',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'contain',
      marginLeft: 48,
      marginRight: 12,
      width: 60
    },
    item: {
      margin: '0 12px'
    },
    [largeDevices(theme, BreakpointType.Down)]: {
      container: {
        display: 'none'
      }
    }
  })

export default FooterStyles
