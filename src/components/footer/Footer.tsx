import React, { Component } from 'react'
import FooterStyles from './Footer.styles'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import App from './../../App'
import { Typography } from '@material-ui/core'

class Footer extends Component<WithStyles<typeof FooterStyles>> {
  render() {
    const { i18n, version } = App
    const { classes } = this.props
    return (
      <div className={classes.container}>
        <a className={classes.logoContainer} href="http://www.technogym.com" target="_blank" rel="noopener noreferrer">
          <div className={classes.logo} />
        </a>

        <Typography className={classes.item} variant="caption">
          Technogym SpA
        </Typography>
        <Typography className={classes.item} variant="caption">
          {`${i18n.t('S437')} ${version}`}
        </Typography>
      </div>
    )
  }
}
export default withStyles(FooterStyles)(Footer)
