import PagedListColumnHeader from './PagedListColumnHeader'

interface PagedListState<TItem> {
  columnHeaders?: PagedListColumnHeader[]
  items: TItem[]
  itemsTotalCount: number
  pageStart: number
  pageEnd: number
  loadingMoreItems: boolean
}

export default PagedListState
