import SortOrder from '../../../infrastructure/repository/application/models/SortOrder'

interface PagedListColumnHeader {
  id: string
  text: string
  sortable: boolean
  sortField?: string
  sortOrder?: SortOrder
  defaultSortOrder?: SortOrder
  className?: string
  isVisible(): boolean
}

export default PagedListColumnHeader
