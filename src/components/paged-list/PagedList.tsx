import React, { ReactElement, useCallback, memo, useState, useEffect, createRef } from 'react'
import PagedListStyles from './PagedList.styles'
import PagedListColumnHeader from './models/PagedListColumnHeader'
import { FixedSizeList, ListChildComponentProps, areEqual } from 'react-window'
import SortOrder from 'infrastructure/repository/application/models/SortOrder'
import { Typography, Divider, CircularProgress } from '@material-ui/core'
import InfiniteLoader from 'react-window-infinite-loader'
import Images from 'assets/images'
import clsx from 'clsx'
import App from 'App'
import ScrollbarSize, { ScrollbarSizeChangeHandlerParams } from 'react-scrollbar-size'

const ListRow = memo((props: ListChildComponentProps): ReactElement => {
  const { data, style, index } = props
  const { renderItem, items, loadingMoreItems } = data
  const isLoading = !!(items.length === index && loadingMoreItems)
  return renderItem(!isLoading ? items[index] : items[index - 1], index, style, isLoading)
}, areEqual)

interface PagedListProps {
  items: any[]
  itemHeight: number
  columnHeaders?: PagedListColumnHeader[]
  loadingMoreItems: boolean
  renderItem(item: any, itemIndex: number, style: React.CSSProperties, isLoaderPlaceholder: boolean): ReactElement
  onLoadMoreItems(): Promise<void>
  onSortChange?(sortField: string, sortOrder: SortOrder): void
  initialOffset: number
  canLoadMoreItems?: boolean
  noResultText?: string
}

const PagedList: React.FC<PagedListProps> = (props: PagedListProps) => {
  const classes = PagedListStyles()

  const { i18n } = App
  const {
    items,
    renderItem,
    itemHeight,
    columnHeaders,
    loadingMoreItems,
    initialOffset,
    noResultText,
    canLoadMoreItems,
    onLoadMoreItems
  } = props

  const listContainerRef = createRef<HTMLDivElement>()
  const listRef = createRef<HTMLDivElement>()

  const [listContainerHeight, setListContainerHeight] = useState<number>(0)
  const [scrollbarWidth, setScrollbarWidth] = useState<number>(0)

  useEffect(() => {
    const updateListContainerHeight = () => {
      if (listContainerRef.current) setListContainerHeight(listContainerRef.current.offsetHeight - 8)
    }

    updateListContainerHeight()

    const onWindowResize = () => {
      updateListContainerHeight()
    }

    window.addEventListener('resize', onWindowResize)

    return () => {
      window.removeEventListener('resize', onWindowResize)
    }
  }, [listContainerRef])

  const sort = useCallback(
    (column: PagedListColumnHeader) => {
      const { sortField, sortOrder } = column
      const { onSortChange } = props

      if (onSortChange) {
        onSortChange(sortField || '', !!sortOrder && sortOrder === SortOrder.Asc ? SortOrder.Desc : SortOrder.Asc)
      }
    },
    [props]
  )

  const onScrollbarSize = useCallback(({ width }: ScrollbarSizeChangeHandlerParams) => {
    setScrollbarWidth(width)
  }, [])

  const itemCount = canLoadMoreItems ? items.length + 1 : items.length
  const loadMoreItems = loadingMoreItems ? undefined : onLoadMoreItems
  const isItemLoaded = (index: number) => !canLoadMoreItems || index < items.length

  return (
    <div className={classes.tableContainer}>
      <ScrollbarSize onChange={onScrollbarSize} />
      {columnHeaders && columnHeaders.length > 0 && (
        <div className={classes.headerWrapper}>
          <div className={classes.headerContainer}>
            <div className={classes.header}>
              {columnHeaders.map(columnHeader => {
                const { id, className, sortable, sortOrder } = columnHeader
                return (
                  <div
                    key={id}
                    className={clsx(classes.column, className, sortable ? 'sortable' : '')}
                    onClick={sortable ? () => sort(columnHeader) : undefined}>
                    <Typography className={classes.columnHeadertext} variant="subtitle1">
                      {columnHeader.text}
                    </Typography>
                    {sortable &&
                      sortOrder &&
                      (sortOrder === SortOrder.Asc ? (
                        <Images.RCiconSortAsc className={classes.arrows} />
                      ) : (
                        <Images.RCiconSortDesc className={classes.arrows} />
                      ))}
                  </div>
                )
              })}
            </div>
            <div style={{ width: scrollbarWidth }} />
          </div>
          <Divider className={classes.divider} />
        </div>
      )}

      {items.length > 0 ? (
        <div className={classes.listContainer} ref={listContainerRef}>
          <InfiniteLoader isItemLoaded={isItemLoaded} itemCount={itemCount} loadMoreItems={loadMoreItems}>
            {(props: any) => {
              const { onItemsRendered, ref } = props
              return (
                <FixedSizeList
                  outerRef={listRef}
                  itemCount={items.length + (canLoadMoreItems ? 1 : 0)}
                  height={listContainerHeight}
                  itemData={{
                    items,
                    renderItem,
                    loadingMoreItems
                  }}
                  width={'100%'}
                  itemSize={itemHeight}
                  onItemsRendered={onItemsRendered}
                  ref={ref}
                  initialScrollOffset={initialOffset}
                  overscanCount={5}>
                  {ListRow}
                </FixedSizeList>
              )
            }}
          </InfiniteLoader>
        </div>
      ) : loadingMoreItems ? (
        <div className={classes.loaderContainer} style={{ height: itemHeight }}>
          <CircularProgress />
        </div>
      ) : (
        <Typography className={classes.noResult} variant="body1" component="div">
          {noResultText || i18n.t('S33')}
        </Typography>
      )}
    </div>
  )
}

export default PagedList
