import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'
import { lineClamp } from 'assets/styles/mixins/Text'

const PagedListStyles = makeStyles((theme: Theme) =>
  createStyles({
    tableContainer: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1
    },
    headerWrapper: {
      display: 'flex',
      flexDirection: 'column'
    },
    headerContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row'
    },
    header: {
      flex: 1,
      display: 'flex',
      alignItems: 'center'
    },
    listContainer: {
      display: 'flex',
      flex: 1
    },
    column: {
      display: 'flex',
      alignItems: 'center',
      alignSelf: 'stretch',
      cursor: 'default',
      '&.sortable': {
        cursor: 'pointer'
      }
    },
    columnText: {
      fontWeight: 700
    },
    columnHeadertext: {
      ...lineClamp(2)
    },
    arrows: {
      flexShrink: 0
    },
    divider: {
      margin: '12px 0 0 0'
    },
    noResult: {
      margin: 30,
      textAlign: 'center'
    },
    loaderContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
)

export default PagedListStyles
