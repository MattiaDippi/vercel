import React from 'react'
import useCommonUserAvatarStyles from './CommonUserAvatar.styles'
import Tag, { TagType } from 'components/tag/Tag'
import { AvatarSizeType } from 'mw-react-web-app-ui'

interface UserAvatarTagProps {
  size: AvatarSizeType
}

const UserAvatarTag: React.FC<UserAvatarTagProps> = (props: UserAvatarTagProps) => {
  const { size } = props
  const styles = useCommonUserAvatarStyles()
  return <Tag text={'new'} className={[styles.isNew, size]} type={TagType.New} />
}

export default UserAvatarTag
