import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'

const UserAvatarStyles = (theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.picture.main
    },
    placeholder: {
      color: theme.palette.picture.contrastText
    }
  })

export default UserAvatarStyles
