import { makeStyles, createStyles } from '@material-ui/core/styles'
import Theme from './../../assets/styles/themes/Theme'

const useCommonUserAvatarStyles = makeStyles((theme: Theme) =>
  createStyles({
    dropOutRisk: {
      borderRadius: '50%',
      position: 'absolute',
      '&.ExtraExtraSmall': {
        width: 8,
        height: 8,
        bottom: -1,
        right: -1,
        border: 'solid 1px ' + theme.palette.background.default
      },
      '&.ExtraSmall': {
        width: 12,
        height: 12,
        bottom: -2,
        right: -2,
        border: 'solid 1px ' + theme.palette.background.default
      },
      '&.Small': {
        width: 16,
        height: 16,
        bottom: -3,
        right: -3,
        border: 'solid 2px ' + theme.palette.background.default
      },
      '&.Medium': {
        width: 18,
        height: 18,
        bottom: -3,
        right: -3,
        border: 'solid 2px ' + theme.palette.background.default
      },
      '&.Large': {
        height: 20,
        width: 20,
        bottom: -2,
        right: -2,
        border: 'solid 2px ' + theme.palette.background.default
      },
      '&.ExtraLarge': {
        height: 22,
        width: 22,
        bottom: -3,
        right: -3,
        border: 'solid 2px ' + theme.palette.background.default
      },
      '&.ExtraExtraLarge': {
        height: 26,
        width: 26,
        bottom: 2,
        right: 2,
        border: 'solid 2px ' + theme.palette.background.default
      }
    },
    isNew: {
      position: 'absolute',
      borderRadius: 1,
      textTransform: 'uppercase',
      fontWeight: 'bold',
      '&.ExtraExtraSmall': {
        fontSize: 0,
        top: 2,
        right: -4,
        padding: 0,
        width: 12,
        height: 5
      },
      '&.ExtraSmall': {
        fontSize: 0,
        top: 4,
        right: -4,
        padding: 0,
        width: 14,
        height: 6
      },
      '&.Small': {
        fontSize: 8,
        top: 2,
        right: -5,
        padding: 2
      },
      '&.Medium': {
        fontSize: 9,
        top: 3,
        right: -5,
        padding: 2
      },
      '&.Large': {
        fontSize: 10,
        top: 4,
        right: -5,
        padding: 2
      },
      '&.ExtraLarge': {
        fontSize: 14,
        top: 5,
        right: -5,
        padding: 3
      },
      '&.ExtraExtraLarge': {
        fontSize: 16,
        top: 5,
        right: -5,
        padding: 4
      }
    }
  })
)

export default useCommonUserAvatarStyles
