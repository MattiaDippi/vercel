import React, { PureComponent } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import UserAvatarStyles from './UserAvatar.styles'
import UserAvatarTag from './UserAvatarTag'
import { AvatarSizeType, Avatar } from 'mw-react-web-app-ui'

interface UserAvatarProps {
  pictureUrl?: string
  placeholderText: string
  showPopover?: boolean
  size: AvatarSizeType
  isNew?: boolean
  tooltip?: React.ReactNode
  className?: string
  onClick?: (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    imageUrl?: string | undefined,
    element?: HTMLDivElement | undefined
  ) => void
}

class UserAvatar extends PureComponent<UserAvatarProps & WithStyles<typeof UserAvatarStyles>> {
  static defaultProps = {
    size: AvatarSizeType.Small,
    isNew: false,
    showPopover: true
  }

  render() {
    const { classes, pictureUrl, className, size, placeholderText, isNew, tooltip, showPopover, onClick } = this.props

    return (
      <Avatar
        pictureUrl={pictureUrl}
        placeholderText={placeholderText}
        showPopover={showPopover}
        tag={isNew ? <UserAvatarTag size={size} /> : undefined}
        tooltip={tooltip}
        classes={{
          root: clsx(classes.container, size, className),
          placeholder: classes.placeholder
        }}
        onClick={onClick}
      />
    )
  }
}

export default withStyles(UserAvatarStyles)(UserAvatar)
