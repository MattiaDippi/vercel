import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import TagStyles from './Tag.styles'
import clsx from 'clsx'
import { Tooltip, Typography } from '@material-ui/core'
import { TooltipProps } from '@material-ui/core/Tooltip'

export enum TagType {
  LastInteraction = 'LastInteraction',
  LastInteractionHighlight = 'LastInteractionHighlight',
  New = 'New',
  Appointment = 'Appointment',
  AppointmentFull = 'AppointmentFull',
  Expired = 'Expired',
  Primary = 'Primary',
  HasProblems = 'HasProblems',
  NoProblems = 'NoProblems',
  LiveEvent = 'LiveEvent',
  Error = 'Error',
  Info = 'Info'
}

interface TagProps {
  text: string
  tooltipText?: string
  tooltipPlacement?: TooltipProps['placement']
  type: TagType
  wrap?: boolean
  className?: string | string[]
}

class Tag extends Component<TagProps & WithStyles<typeof TagStyles>> {
  renderTag = () => {
    const { classes, className, type, text, wrap } = this.props
    return (
      <Typography variant="subtitle2" component="div" className={clsx(classes.tag, type, className)} noWrap={!wrap}>
        {text}
      </Typography>
    )
  }

  render() {
    const { tooltipText, tooltipPlacement } = this.props

    if (tooltipText) {
      return (
        <Tooltip title={tooltipText} placement={tooltipPlacement || 'bottom'}>
          {this.renderTag()}
        </Tooltip>
      )
    }

    return this.renderTag()
  }
}

export default withStyles(TagStyles)(Tag)
