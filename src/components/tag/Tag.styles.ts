import { createStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const TagStyles = (theme: Theme) =>
  createStyles({
    tag: {
      borderRadius: theme.shape.borderRadius,
      textAlign: 'center',
      padding: '0 12px',
      textTransform: 'uppercase',
      '&.New': {
        color: theme.palette.tag.new.contrastText,
        backgroundColor: theme.palette.tag.new.main
      }
    }
  })

export default TagStyles
