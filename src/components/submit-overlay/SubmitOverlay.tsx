import React, { Component } from 'react'
import SubmitOverlayStyles from './SubmitOverlay.styles'
import { WithStyles, withStyles } from '@material-ui/core/styles'

class SubmitOverlay extends Component<WithStyles<typeof SubmitOverlayStyles>> {
  render() {
    const { classes } = this.props

    return <div className={classes.container} />
  }
}

export default withStyles(SubmitOverlayStyles)(SubmitOverlay)
