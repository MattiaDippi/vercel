import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'

const SubmitOverlayStyles = (theme: Theme) =>
  createStyles({
    container: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      width: '100%',
      height: '100%',
      backgroundColor: theme.palette.background.paper,
      opacity: 0.7,
      zIndex: theme.zIndex.modal + 10
    }
  })

export default SubmitOverlayStyles
