import React from 'react'

interface NewLineTextProps {
  text?: string
}

const NewLineText: React.FC<NewLineTextProps> = (props: NewLineTextProps) => {
  const { text } = props

  return (
    <React.Fragment>
      {text &&
        text.split('\n').map((item: string, index: number) => {
          return (
            <React.Fragment key={index}>
              {item}
              <br />
            </React.Fragment>
          )
        })}
    </React.Fragment>
  )
}

export default NewLineText
