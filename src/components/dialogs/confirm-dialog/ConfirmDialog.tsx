import React, { Component } from 'react'
import BaseDialog, { BaseDialogProps } from './../base-dialog/BaseDialog'
import { DialogContentText } from '@material-ui/core'

interface ConfirmDialogProps extends BaseDialogProps {
  text?: string
}

class ConfirmDialog extends Component<ConfirmDialogProps> {
  render() {
    const { title, actions, open, onClose, text, size, smallTitle, className } = this.props

    return (
      <BaseDialog
        open={open}
        title={title}
        actions={actions}
        onClose={onClose}
        size={size}
        smallTitle={smallTitle}
        className={className}>
        {text ? <DialogContentText>{text}</DialogContentText> : null}
      </BaseDialog>
    )
  }
}

export default ConfirmDialog
