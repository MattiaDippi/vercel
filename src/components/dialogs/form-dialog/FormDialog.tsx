import React, { Component, createRef } from 'react'
import BaseDialog, { BaseDialogProps } from './../base-dialog/BaseDialog'
import { Formik, FormikActions, FormikProps, FormikState } from 'formik'
import DialogAction from '../models/DialogAction'
import CommonUtils from './../../../infrastructure/utils/CommonUtils'

export const isFormikFormValid = async <T extends unknown>(
  values: FormikState<T>['values'],
  setFieldTouched: FormikActions<T>['setFieldTouched'],
  validateForm: FormikActions<T>['validateForm']
): Promise<boolean> => {
  Object.keys(CommonUtils.flatten(values)).forEach(key => {
    setFieldTouched(key as keyof T & string, true, false)
  })
  const formValidation = await validateForm()
  return Object.keys(formValidation).length === 0
}

interface FormDialogProps<T> extends BaseDialogProps {
  actions: DialogAction<T>[]
  children: (props: FormikProps<T>) => React.ReactNode
  initialValues?: T
  validationSchema?: any | (() => any)
  formRef?: (r: Formik<T>) => void
}

interface FormDialogState {
  submitting: boolean
}

class FormDialog<T> extends Component<FormDialogProps<T>, FormDialogState> {
  baseDialog = createRef<any>()

  submitAction?: DialogAction<T>

  constructor(props: FormDialogProps<T>) {
    super(props)

    this.state = {
      submitting: false
    }
  }

  onSubmitForm = (event: React.FormEvent<HTMLFormElement>) => {
    // Do not submit form on ENTER.
    // When there's only an input in the form and you press ENTER, the form is automatically submitted.
    // With 2 or more inputs, the form is NOT automatically submitted.
    // See https://stackoverflow.com/questions/1370021/why-does-forms-with-single-input-field-submit-upon-pressing-enter-key-in-input
    event.preventDefault()
  }

  onSubmit = async (values: T, actions: FormikActions<T>) => {
    if (this.submitAction && this.submitAction.handler) {
      this.setState({ submitting: true })
      await this.submitAction.handler(values)
      this.setState({ submitting: false })
      actions.setSubmitting(false)
    }
  }

  render() {
    const {
      title,
      subtitle,
      titlePosition,
      size,
      actions,
      secondaryActions,
      open,
      onClose,
      children,
      maxWidth,
      fullScreen,
      noContentPadding,
      initialValues,
      validationSchema,
      disableEnforceFocus,
      disableBackdropClick,
      disableEscapeKeyDown,
      initializing,
      submittingForm,
      dialogHeight,
      dialogWidth,
      formRef,
      className,
      smallTitle,
      contentContainerClass,
      titleClass,
      disableSubmitting
    } = this.props
    const { submitting } = this.state

    if (actions) {
      this.submitAction = actions.find(action => !!action.submit && !!action.handler)
    }

    return (
      <Formik
        onSubmit={this.onSubmit}
        initialValues={initialValues || ({} as T)}
        validationSchema={validationSchema}
        enableReinitialize
        ref={formRef}>
        {(formikProps: FormikProps<T>) => (
          <BaseDialog
            title={title}
            subtitle={subtitle}
            titlePosition={titlePosition}
            size={size}
            smallTitle={smallTitle}
            actions={
              actions
                ? actions.map(action =>
                    // Override submit action handler by triggering form submission
                    // The actual handler will run on form submit
                    action.submit
                      ? {
                          ...action,
                          submitHandler: () => {
                            formikProps.submitForm()
                          },
                          validateHandler: async () => {
                            const isFormValid = await isFormikFormValid<T>(
                              formikProps.values,
                              formikProps.setFieldTouched,
                              formikProps.validateForm
                            )
                            return isFormValid
                          }
                        }
                      : action
                  )
                : undefined
            }
            secondaryActions={secondaryActions}
            maxWidth={maxWidth}
            fullScreen={fullScreen}
            open={open}
            onClose={onClose}
            noContentPadding={noContentPadding}
            disableEnforceFocus={disableEnforceFocus}
            disableBackdropClick={disableBackdropClick}
            disableEscapeKeyDown={disableEscapeKeyDown}
            submittingForm={submitting || submittingForm}
            initializing={initializing}
            dialogWidth={dialogWidth}
            dialogHeight={dialogHeight}
            className={className}
            contentContainerClass={contentContainerClass}
            titleClass={titleClass}
            disableSubmitting={disableSubmitting}>
            <form onSubmit={this.onSubmitForm}>{children(formikProps)}</form>
          </BaseDialog>
        )}
      </Formik>
    )
  }
}

export default FormDialog
