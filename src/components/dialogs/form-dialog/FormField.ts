interface FormField<T> {
  value?: T
  error?: string
  touched?: boolean
}

export default FormField
