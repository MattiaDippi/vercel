enum DialogActionType {
  Default = 'Default',
  Primary = 'Primary',
  Delete = 'Delete',
  Close = 'Close',
  Text = 'Text',
  PrimaryText = 'PrimaryText',
  PrimaryDisabled = 'PrimaryDisabled'
}

export default DialogActionType
