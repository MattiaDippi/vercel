import DialogActionType from './DialogActionType'

interface DialogActionConfirm {
  title: string
  text: string
  confirmButtonText?: string
  confirmButtonType?: DialogActionType
  cancelButtonText?: string
  cancelButtonType?: DialogActionType
}

export type DialogActionHandler<T = {}> = ((...args: any) => void) | ((values: T) => Promise<any>)

interface DialogAction<T = {}> {
  text: string
  handler?: DialogActionHandler<T>
  submitHandler?(): void
  validateHandler?(): Promise<boolean>
  type?: DialogActionType
  submit?: boolean
  confirm?: DialogActionConfirm
  showLoaderOnSubmit?: boolean
  icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>
  disabled?: boolean
}

export default DialogAction
