import React, { Component } from 'react'
import { WithStyles, withStyles } from '@material-ui/core/styles'
import { Typography, CircularProgress } from '@material-ui/core'
import ProgressDialogStyles from './ProgressDialog.styles'
import BaseDialog, { BaseDialogProps, BaseDialogSize } from './../base-dialog/BaseDialog'

interface ProgressDialogProps extends BaseDialogProps {
  text?: string
}

class ProgressDialog extends Component<ProgressDialogProps & WithStyles<typeof ProgressDialogStyles>> {
  render() {
    const { open, title, subtitle, text, actions, onClose, classes } = this.props

    return (
      <BaseDialog
        open={open}
        title={title}
        subtitle={subtitle}
        actions={actions}
        onClose={onClose}
        disableBackdropClick
        disableEscapeKeyDown
        size={BaseDialogSize.Small}>
        <div className={classes.content}>
          <CircularProgress />
          {text ? (
            <Typography variant="body1" className={classes.description}>
              {text}
            </Typography>
          ) : null}
        </div>
      </BaseDialog>
    )
  }
}

export default withStyles(ProgressDialogStyles)(ProgressDialog)
