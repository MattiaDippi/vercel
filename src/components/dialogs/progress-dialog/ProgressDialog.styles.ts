import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'

const ProgressDialogStyles = (theme: Theme) =>
  createStyles({
    content: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyItems: 'center',
      minHeight: 160
    },
    description: {
      marginLeft: 16
    }
  })

export default ProgressDialogStyles
