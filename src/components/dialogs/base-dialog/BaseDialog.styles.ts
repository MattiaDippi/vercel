import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'
import { largeDevices, BreakpointType } from './../../../assets/styles/mixins/Breakpoints'
import { BaseDialogProps } from './../base-dialog/BaseDialog'
import ColorUtils from './../../../infrastructure/utils/ColorUtils'

const BaseDialogStyles = (theme: Theme) =>
  createStyles({
    titleContainer: {
      paddingBottom: 12
    },
    paper: (props: BaseDialogProps) => ({
      '&.ExtraLarge': {
        minWidth: 944,
        [largeDevices(theme, BreakpointType.Exact)]: {
          minWidth: 744
        },
        [largeDevices(theme, BreakpointType.Down)]: {
          minWidth: 544
        }
      },
      '&.Large': {
        width: 944
      },
      '&.Medium': {
        width: 744
      },
      '&.Small': {
        width: 544
      },
      '&.Custom': {
        width: props.dialogWidth || 544,
        height: props.dialogHeight || 'auto'
      },
      '&.Auto': {
        width: 'unset'
      }
    }),
    deleteButtonText: {
      color: theme.palette.error.main
    },
    contentContainer: {
      paddingTop: `0px !important`
    },
    contentWrapper: {
      position: 'relative',
      flex: 1
    },
    contentWrapperNoScroll: {
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden'
    },
    contentFullScreen: {
      display: 'flex',
      flex: 1
    },
    contentNoPadding: {
      padding: 0,
      '&:first-child': {
        paddingTop: 0
      }
    },
    contentNoRightPadding: {
      padding: '0 0 0 44px'
    },
    contentTitleLeft: {
      display: 'flex',
      flexDirection: 'row',
      paddingTop: `64px !important`
    },
    contentNoScroll: {
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden'
    },
    title: {},
    subtitle: {
      marginBottom: 20,
      marginRight: 64,
      marginLeft: 64
    },
    titleLeft: {
      padding: '0 0 44px 0'
    },
    titleLeftContainer: {
      width: 337,
      display: 'flex',
      flexDirection: 'row'
    },
    titleLeftDivider: {
      marginLeft: 32,
      marginRight: 48,
      height: '100%',
      width: 1
    },
    smallTitle: {
      padding: 24
    },
    smallTitleContainer: {
      paddingBottom: 0
    },
    noPaddingTitle: {
      paddingBottom: 0
    },
    actionsContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end'
    },
    secondaryActions: {
      display: 'flex',
      marginRight: 'auto'
    },
    iconLeft: {
      marginRight: 8,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    iconPrimary: {
      '& path': {
        fill: theme.palette.primary.main
      }
    },
    iconDisabled: {
      '& path': {
        fill: ColorUtils.hexToRgbString(theme.palette.text.primary, 0.3)
      }
    },
    iconButton: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '0px 20px'
    },
    '@media (max-height: 800px)': {
      subtitle: {
        marginBottom: 12
      },
      title: {
        paddingTop: 32,
        paddingBottom: 20
      }
    }
  })

export default BaseDialogStyles
