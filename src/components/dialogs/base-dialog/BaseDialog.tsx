import React, { Component } from 'react'
import DialogAction, { DialogActionHandler } from './../models/DialogAction'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
  Typography,
  Slide,
  Divider
} from '@material-ui/core'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import DialogActionType from './../models/DialogActionType'
import BaseDialogStyles from './BaseDialog.styles'
import SubmitOverlay from './../../submit-overlay/SubmitOverlay'
import clsx from 'clsx'
import { DialogProps } from '@material-ui/core/Dialog'
import { TransitionProps } from '@material-ui/core/transitions/transition'
import ConfirmDialog from '../confirm-dialog/ConfirmDialog'
import App from './../../../App'
import { isSmallDeviceDown } from './../../../assets/styles/mixins/Breakpoints'
import withWidth, { WithWidth } from '@material-ui/core/withWidth'

function Transition(props: TransitionProps) {
  return <Slide direction="up" {...props} />
}

export enum BaseDialogSize {
  Small = 'Small', // width: 544
  Medium = 'Medium', // width: 744
  Large = 'Large', // width: 944
  ExtraLarge = 'ExtraLarge', // minWidth: 944
  Custom = 'Custom',
  Auto = 'Auto'
}

export enum BaseDialogTitlePosition {
  Top = 'Top',
  Left = 'Left'
}

export interface BaseDialogProps {
  ref?: React.RefObject<any>
  title?: string
  subtitle?: string
  titlePosition?: BaseDialogTitlePosition
  actions?: DialogAction<any>[]
  secondaryActions?: DialogAction<any>[]
  open: boolean
  disableBackdropClick?: boolean
  disableEscapeKeyDown?: boolean
  fullScreen?: DialogProps['fullScreen']
  maxWidth?: DialogProps['maxWidth']
  size?: BaseDialogSize
  noContentPadding?: boolean
  noContentRightPadding?: boolean
  onClose?(): void
  disableEnforceFocus?: boolean
  submittingForm?: boolean
  initializing?: boolean
  dialogWidth?: number | string
  dialogHeight?: number | string
  className?: string
  smallTitle?: boolean
  contentContainerClass?: string
  contentWrapperClass?: string
  titleClass?: string
  disableSubmitting?: boolean
  disableScroll?: boolean
}

interface BaseDialogState {
  submitting: boolean
  confirmDialogOpen: boolean
  confirmTitle?: string
  confirmText?: string
  confirmHandler?: DialogActionHandler
  confirmButtonText?: string
  confirmButtonType?: DialogActionType
  cancelButtonText?: string
  cancelButtonType?: DialogActionType
}

class BaseDialog extends Component<BaseDialogProps & WithStyles<typeof BaseDialogStyles> & WithWidth, BaseDialogState> {
  static defaultProps = {
    size: BaseDialogSize.Large,
    titlePosition: BaseDialogTitlePosition.Top,
    disableBackdropClick: false,
    disableEscapeKeyDown: false,
    noContentPadding: false,
    noContentRightPadding: false,
    fullScreen: false,
    disableEnforceFocus: false
  }

  didCancel: boolean

  constructor(props: BaseDialogProps & WithStyles<typeof BaseDialogStyles> & WithWidth) {
    super(props)

    this.state = {
      submitting: false,
      confirmDialogOpen: false
    }

    this.didCancel = false
  }

  componentWillUnmount() {
    this.didCancel = true
  }

  onClickButton = async (action: DialogAction) => {
    if (action.handler) {
      if (action.submit && action.submitHandler && action.validateHandler) {
        if (action.confirm) {
          const isFormValid = await action.validateHandler()
          if (isFormValid) {
            this.openConfirmDialog(action)
          }
        } else {
          action.submitHandler()
        }
      } else {
        if (action.confirm) {
          this.openConfirmDialog(action)
        } else {
          this.runHandler(action.handler)
        }
      }
    }
  }

  runHandler = async (handler: DialogActionHandler) => {
    const { disableSubmitting } = this.props
    if (!disableSubmitting) this.setState({ submitting: true })
    await handler({})
    if (!this.didCancel && !disableSubmitting) this.setState({ submitting: false })
  }

  openConfirmDialog = (action: DialogAction) => {
    if (action.confirm) {
      this.setState({
        confirmDialogOpen: true,
        confirmTitle: action.confirm.title,
        confirmText: action.confirm.text,
        confirmHandler: action.submit && action.submitHandler ? action.submitHandler : action.handler,
        confirmButtonText: action.confirm.confirmButtonText,
        confirmButtonType: action.confirm.confirmButtonType,
        cancelButtonText: action.confirm.cancelButtonText,
        cancelButtonType: action.confirm.cancelButtonType
      })
    }
  }

  closeConfirmDialog = () => {
    this.setState({
      confirmDialogOpen: false
    })
  }

  render() {
    const {
      open,
      title,
      subtitle,
      titlePosition,
      actions,
      secondaryActions,
      onClose,
      children,
      disableBackdropClick,
      disableEscapeKeyDown,
      classes,
      fullScreen,
      size,
      noContentPadding,
      noContentRightPadding,
      disableEnforceFocus,
      submittingForm,
      initializing,
      width,
      className,
      smallTitle,
      contentContainerClass,
      contentWrapperClass,
      titleClass,
      disableSubmitting,
      disableScroll
    } = this.props
    const {
      submitting,
      confirmDialogOpen,
      confirmTitle,
      confirmText,
      confirmHandler,
      confirmButtonText,
      confirmButtonType,
      cancelButtonText,
      cancelButtonType
    } = this.state
    const { i18n } = App

    let showFullScreen = Boolean(fullScreen)
    let transition: DialogProps['TransitionComponent']

    if (!showFullScreen && isSmallDeviceDown(width)) {
      showFullScreen = true
      transition = Transition
    }

    const confirmDialogActions = [
      {
        text: cancelButtonText || i18n.t('S46'),
        type: cancelButtonType || DialogActionType.Default,
        handler: this.closeConfirmDialog
      },
      {
        text: confirmButtonText || i18n.t('S45'),
        type: confirmButtonType || DialogActionType.Default,
        handler: () => {
          this.closeConfirmDialog()
          if (confirmHandler) {
            this.runHandler(confirmHandler)
          }
        }
      }
    ]

    const actionButton = (action: DialogAction, index: number, secondary?: boolean) => {
      const disabled =
        action.disabled ||
        submitting ||
        submittingForm ||
        (initializing && action.type !== DialogActionType.Close) ||
        action.type === DialogActionType.PrimaryDisabled

      return (
        <Button
          key={index}
          classes={{
            root: classes.iconButton,
            outlined: action.type === DialogActionType.Delete ? classes.deleteButtonText : undefined
          }}
          onClick={() => {
            this.onClickButton(action)
          }}
          color={
            secondary
              ? action.type === DialogActionType.Primary ||
                action.type === DialogActionType.PrimaryText ||
                action.type === DialogActionType.PrimaryDisabled
                ? 'primary'
                : 'default'
              : 'primary'
          }
          variant={
            action.type === DialogActionType.Text || action.type === DialogActionType.PrimaryText
              ? 'text'
              : action.type === DialogActionType.Primary
              ? 'contained'
              : 'outlined'
          }
          disabled={disabled}
          type={action.submit ? 'submit' : 'button'}>
          {action.icon && (
            <div
              className={clsx(classes.iconLeft, {
                [classes.iconPrimary]: action.type === DialogActionType.PrimaryText,
                [classes.iconDisabled]: disabled
              })}>
              <action.icon />
            </div>
          )}
          {((action.submit && action.showLoaderOnSubmit !== false) || action.showLoaderOnSubmit) &&
          (submitting || submittingForm) ? (
            <CircularProgress size={24} />
          ) : (
            action.text
          )}
        </Button>
      )
    }

    return (
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="dialog-title"
        aria-describedby="dialog-description"
        disableBackdropClick={disableBackdropClick}
        disableEscapeKeyDown={disableEscapeKeyDown}
        fullScreen={showFullScreen}
        TransitionComponent={transition}
        classes={{ paper: clsx(classes.paper, !fullScreen ? size : null, className) }}
        disableEnforceFocus={disableEnforceFocus}
        maxWidth="xl">
        {(title || subtitle) && titlePosition === BaseDialogTitlePosition.Top && (
          <div className={clsx(classes.titleContainer, smallTitle ? classes.smallTitleContainer : undefined)}>
            {title && (
              <DialogTitle
                id="dialog-title"
                classes={{
                  root: clsx(smallTitle ? classes.smallTitle : undefined, titleClass, classes.title)
                }}>
                {title}
              </DialogTitle>
            )}
            {subtitle && (
              <Typography variant="body1" classes={{ root: classes.subtitle }}>
                {subtitle}
              </Typography>
            )}
          </div>
        )}
        <DialogContent
          classes={{
            root: clsx(classes.contentContainer, contentContainerClass, {
              [classes.contentNoPadding]: noContentPadding,
              [classes.contentNoRightPadding]: noContentRightPadding,
              [classes.contentFullScreen]: fullScreen,
              [classes.contentTitleLeft]: titlePosition === BaseDialogTitlePosition.Left,
              [classes.contentNoScroll]: disableScroll
            })
          }}>
          {(title || subtitle) && titlePosition === BaseDialogTitlePosition.Left && (
            <div className={classes.titleLeftContainer}>
              <div>
                {title && (
                  <DialogTitle id="dialog-title" className={classes.titleLeft}>
                    {title}
                  </DialogTitle>
                )}
                {subtitle && <Typography variant="body1">{subtitle}</Typography>}
              </div>
              <Divider className={classes.titleLeftDivider} />
            </div>
          )}
          <div
            className={clsx(classes.contentWrapper, contentWrapperClass, {
              [classes.contentWrapperNoScroll]: disableScroll
            })}>
            {(submitting || submittingForm) && !disableSubmitting && <SubmitOverlay />}
            {children}
          </div>
        </DialogContent>
        <div>
          <div className={classes.actionsContainer}>
            {secondaryActions && (
              <DialogActions classes={{ root: classes.secondaryActions }}>
                {secondaryActions.map((action, index) => actionButton(action, index, true))}
              </DialogActions>
            )}
            {actions && <DialogActions>{actions.map((action, index) => actionButton(action, index))}</DialogActions>}
            {confirmTitle && confirmText && confirmHandler && (
              <ConfirmDialog
                open={confirmDialogOpen}
                title={confirmTitle}
                text={confirmText}
                onClose={this.closeConfirmDialog}
                size={BaseDialogSize.Small}
                actions={confirmDialogActions}
              />
            )}
          </div>
        </div>
      </Dialog>
    )
  }
}

export default withWidth()(withStyles(BaseDialogStyles)(BaseDialog))
