import { createStyles, Theme } from '@material-ui/core'
import { largeDevices, BreakpointType } from './../../assets/styles/mixins/Breakpoints'

const MediumDeviceHeaderStyles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'none',
      width: '100%',
      height: 48,
      backgroundColor: theme.palette.backgroundSecondary.default
    },
    actionContainer: {
      width: 48
    },
    titleContainer: {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyItems: 'center',
      padding: '0 8px'
    },
    title: {
      fontWeight: 'bold',
      width: '100%'
    },
    [largeDevices(theme, BreakpointType.Down)]: {
      container: {
        display: 'flex'
      }
    }
  })

export default MediumDeviceHeaderStyles
