export type HeaderActionHandler<T = {}> = ((...args: any) => void) | ((values: T) => Promise<any>)

interface HeaderAction<T = {}> {
  handler: HeaderActionHandler<T>
  icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>
}

export default HeaderAction
