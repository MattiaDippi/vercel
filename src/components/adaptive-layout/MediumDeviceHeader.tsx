import React, { Component } from 'react'
import { WithStyles, withStyles } from '@material-ui/core/styles'
import MediumDeviceHeaderStyles from './MediumDeviceHeaderStyles'
import HeaderAction, { HeaderActionHandler } from './models/HeaderAction'
import { IconButton, Typography } from '@material-ui/core'
import Images from './../../assets/images'
import App from './../../App'

export enum MediumDeviceTitleAlignment {
  Center = 'center',
  Left = 'left'
}

export interface MediumDeviceHeaderProps {
  showBack: boolean
  title?: string
  titleAlignment: MediumDeviceTitleAlignment
  actions?: HeaderAction[]
}

class MediumDeviceHeader extends Component<MediumDeviceHeaderProps & WithStyles<typeof MediumDeviceHeaderStyles>> {
  static defaultProps = {
    showBack: true,
    titleAlignment: MediumDeviceTitleAlignment.Center
  }

  goBack = () => {
    const { router } = App

    router.goBack()
  }

  runHandler = async (handler: HeaderActionHandler) => {
    await handler({})
  }

  render() {
    const { showBack, title, titleAlignment, actions, classes } = this.props
    return (
      <div className={classes.container}>
        {showBack && (
          <div className={classes.actionContainer}>
            <IconButton onClick={this.goBack}>
              <Images.RCiconBack />
            </IconButton>
          </div>
        )}

        <div className={classes.titleContainer}>
          {title && (
            <Typography
              variant="body1"
              component="div"
              style={{ textAlign: titleAlignment }}
              noWrap
              className={classes.title}>
              {title}
            </Typography>
          )}
        </div>

        {actions ? (
          <div className={classes.actionContainer}>
            {actions.map((value, index) => {
              return (
                <IconButton key={index} onClick={this.runHandler.bind(this, value.handler)}>
                  <Images.RCiconBack />
                </IconButton>
              )
            })}
          </div>
        ) : (
          showBack && <div className={classes.actionContainer} />
        )}
      </div>
    )
  }
}

export default withStyles(MediumDeviceHeaderStyles)(MediumDeviceHeader)
