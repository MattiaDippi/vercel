import React from 'react'
import useStyles from './ProgressCircle.styles'
import clsx from 'clsx'

interface ProgressCircleClasses {
  root?: string
  svg?: string
  circle?: string
  innerCircle?: string
  content?: string
}

interface ProgressCircleProps {
  size?: number
  strokeWidth?: number
  innerStrokeWidth?: number
  progress?: number
  classes?: ProgressCircleClasses
}

const ProgressCircle: React.FC<ProgressCircleProps> = (props: React.PropsWithChildren<ProgressCircleProps>) => {
  const { size = 70, strokeWidth = 6, innerStrokeWidth = 4, progress = 0, children, classes } = props
  const styles = useStyles()

  const radius = size / 2
  const normalizedRadius = radius - strokeWidth / 2
  const circumference = normalizedRadius * 2 * Math.PI
  const strokeDashoffset = circumference - ((isNaN(progress) ? 0 : progress) / 100) * circumference

  return (
    <div className={clsx(styles.container, classes && classes.root)}>
      <svg width={size} height={size} className={clsx(classes && classes.svg)}>
        <circle
          className={clsx(styles.innerCircle, classes && classes.innerCircle)}
          strokeWidth={innerStrokeWidth}
          r={normalizedRadius}
          cx={radius}
          cy={radius}
        />
        <circle
          className={clsx(styles.circle, classes && classes.circle)}
          strokeWidth={strokeWidth}
          strokeDasharray={`${circumference}  ${circumference}`}
          style={{ strokeDashoffset }}
          r={normalizedRadius}
          cx={radius}
          cy={radius}
        />
      </svg>
      {children && <div className={clsx(styles.componentContainer, classes && classes.content)}>{children}</div>}
    </div>
  )
}

export default ProgressCircle
