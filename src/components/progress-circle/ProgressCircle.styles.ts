import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: 'relative'
    },
    circle: {
      stroke: theme.palette.secondary.light,
      fill: 'transparent',
      transform: 'rotate(-90deg)',
      transformOrigin: 'center center',
      strokeLinecap: 'round',
      transition: 'stroke-dashoffset 0.4s ease'
    },
    innerCircle: {
      stroke: theme.palette.border.light,
      fill: 'transparent'
    },
    componentContainer: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
)

export default useStyles
