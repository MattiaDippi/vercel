import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const ToggleButtonSingleStyles = makeStyles((theme: Theme) =>
  createStyles({
    toggleButton: {
      borderRadius: theme.shape.borderRadius,
      ...theme.typography.button,
      color: theme.palette.text.primary,
      border: `1px solid ${theme.palette.border.main}`,
      height: 32
    },
    toggleButtonSelected: {
      color: `${theme.palette.primary.main} !important`,
      backgroundColor: `${theme.palette.highlight} !important`,
      border: 'none'
    },
    toggleButtonFullWidth: {
      width: '100%'
    }
  })
)

export default ToggleButtonSingleStyles
