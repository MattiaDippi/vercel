import React, { PropsWithChildren } from 'react'
import ToggleButtonSingleStyles from './ToggleButtonSingle.styles'
import ToggleButton from '@material-ui/lab/ToggleButton'
import clsx from 'clsx'

interface ToggleButtonSingleProps {
  selected: boolean
  fullWidth?: boolean
  className?: string
  onToggle: () => void
}

const ToggleButtonSingle: React.FC<PropsWithChildren<ToggleButtonSingleProps>> = (
  props: PropsWithChildren<ToggleButtonSingleProps>
) => {
  const classes = ToggleButtonSingleStyles()

  const { children, selected, className, fullWidth, onToggle } = props

  return (
    <div className={clsx({ [classes.toggleButtonFullWidth]: fullWidth }, className)}>
      <ToggleButton
        value={selected}
        onChange={onToggle}
        classes={{
          root: clsx(classes.toggleButton, {
            [classes.toggleButtonFullWidth]: fullWidth,
            [classes.toggleButtonSelected]: selected
          })
        }}>
        {children}
      </ToggleButton>
    </div>
  )
}

export default ToggleButtonSingle
