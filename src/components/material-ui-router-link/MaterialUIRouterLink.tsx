import React, { Component } from 'react'
import { ReactRouterLink } from 'mw-react-web-app-infrastructure'
import { Link } from '@material-ui/core'
import { LinkBaseProps } from '@material-ui/core/Link'

interface MaterialUIRouterLinkRoute {
  route: string
  params?: object
}

interface MaterialUIRouterLinkProps extends LinkBaseProps {
  className?: string
  to: string | MaterialUIRouterLinkRoute
}

const CollisionLink = React.forwardRef((props: any, ref: any) => (
  <ReactRouterLink innerRef={ref} to={props.to} {...props} />
))

class MaterialUIRouterLink extends Component<MaterialUIRouterLinkProps> {
  static defaultProps = {
    variant: 'body1'
  }

  render() {
    const { className, children, to } = this.props
    const linkTo =
      typeof to === 'string'
        ? to
        : {
            pathname: to.route,
            state: to.params
          }

    return (
      <Link component={CollisionLink} className={className} {...this.props} to={linkTo}>
        {children}
      </Link>
    )
  }
}
export default MaterialUIRouterLink
