import React from 'react'
import { Helmet } from 'react-helmet'

interface DynamicHeadProps {
  title?: string
  version?: string
  disableViewport?: boolean
}

const DynamicHead: React.FC<DynamicHeadProps> = (props: DynamicHeadProps) => {
  const { title, version, disableViewport = false } = props

  return (
    <Helmet>
      {version && <meta name="version" content={version} />}
      <meta
        name="viewport"
        content={
          disableViewport
            ? ''
            : 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no'
        }
      />
      {title && <title>{title}</title>}
    </Helmet>
  )
}

export default DynamicHead
