import React from 'react'
import { Typography } from '@material-ui/core'
import App from './../../App'
import TextUtils from './../../infrastructure/utils/TextUtils'
import useStyles from './HomeView.styles'

const HomeView: React.FC = () => {
  const { i18n, repo } = App
  const loggedUser = repo.getLoggedUser()
  const styles = useStyles()

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Typography variant="h3" component="div">
          {i18n.t('S47')}
        </Typography>
        <Typography variant="body1" component="div" className={styles.titleSeparator}>
          •
        </Typography>
        {loggedUser && (
          <Typography variant="body1" component="div" className={styles.loggedUser}>{`${TextUtils.getUserDisplayName(
            loggedUser.lastName,
            loggedUser.firstName,
            loggedUser.email
          )}`}</Typography>
        )}
      </div>
    </div>
  )
}

export default HomeView
