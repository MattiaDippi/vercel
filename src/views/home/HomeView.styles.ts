import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      height: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      paddingTop: 16,
      paddingLeft: 24 - 4, // Shadow spacing
      paddingRight: 24,
      paddingBottom: 16
    },
    header: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      minHeight: 48,
      paddingLeft: 4
    },
    content: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      overflowY: 'hidden',
      paddingBottom: 48
    },
    titleSeparator: {
      marginLeft: 16,
      marginRight: 16
    },
    loggedUser: {
      marginTop: 4
    }
  })
)

export default useStyles
