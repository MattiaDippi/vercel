interface LoginExternalParams {
  username?: string
  redirectFrom?: string
}

export default LoginExternalParams
