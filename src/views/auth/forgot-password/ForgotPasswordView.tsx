import React, { useRef, useState, useCallback, useEffect } from 'react'
import useStyles from './ForgotPasswordView.styles'
import { Button, TextField, CircularProgress, Typography, InputAdornment } from '@material-ui/core'
import Images from './../../../assets/images'
import App from './../../../App'
import Footer from './../../../components/footer/Footer'
import { Formik, FormikActions } from 'formik'
import * as Yup from 'yup'
import MediumDeviceHader, { MediumDeviceTitleAlignment } from './../../../components/adaptive-layout/MediumDeviceHeader'
import { useScript } from './../../../infrastructure/utils/HooksUtils'

const recaptchaSiteKey = '6LeM5KQZAAAAAPEM1V1R0cuOB6zNbKuU6yYcZUDG'

const recaptchaScriptUrl = `https://www.google.com/recaptcha/api.js?render=${recaptchaSiteKey}`

type ForgotPasswordValues = {
  username: string
}

export interface ForgotPasswordViewProps {
  username?: string
}

const ForgotPasswordView: React.FC<ForgotPasswordViewProps> = (props: ForgotPasswordViewProps) => {
  const { username } = props
  const { i18n } = App
  const styles = useStyles()

  const [loaded, error] = useScript(recaptchaScriptUrl)

  const [passwordResetted, setPasswordResetted] = useState<boolean>(false)

  const initialValues = useRef<ForgotPasswordValues>({
    username: username || ''
  })
  const validationSchema = useRef(
    Yup.object({
      username: Yup.string()
        .email(i18n.t('S20'))
        .required(i18n.t('S19'))
    })
  )

  const goBack = useCallback(() => {
    const { router } = App
    router.goBack()
  }, [])

  const submitForm = useCallback(async (values: ForgotPasswordValues, actions: FormikActions<ForgotPasswordValues>) => {
    const { repo, notifier } = App
    notifier.hideMessage()

    try {
      const onGrecaptchaReady = () => new Promise(resolve => grecaptcha.ready(() => resolve()))

      await onGrecaptchaReady()

      const recaptchaToken = await grecaptcha.execute(recaptchaSiteKey, { action: 'submit' })

      const response = await repo.forgotPassword({
        username: values.username,
        recaptchaToken
      })

      if (!response.hasErrors && response.data) {
        setPasswordResetted(true)
      } else {
        notifier.showErrorMessage(response.getErrorMessage())
      }
    } catch (err) {
      const { notifier, i18n } = App

      notifier.showErrorMessage(i18n.t('S7'))
    }

    actions.setSubmitting(false)
  }, [])

  useEffect(() => {
    if (error) {
      const { notifier, i18n } = App

      notifier.showErrorMessage(i18n.t('S7'))
    }
  }, [error])

  return (
    <div className={styles.container}>
      <div>
        <MediumDeviceHader title={i18n.t('S21')} showBack titleAlignment={MediumDeviceTitleAlignment.Center} />
        <div className={styles.header}>
          <Button className={styles.back} onClick={goBack}>
            <Images.RCiconBack />
            {i18n.t('S6')}
          </Button>
        </div>
        <div className={styles.scrollWrapper}>
          <div className={styles.scrollContainer}>
            <Typography variant="h3" component="div" className={styles.title}>
              {i18n.t('S21')}
            </Typography>

            <Typography className={styles.description} component="div" variant="body1">
              {i18n.t(passwordResetted ? 'S40' : 'S5')}
            </Typography>

            {!passwordResetted && (
              <Formik
                enableReinitialize
                initialValues={initialValues.current}
                validationSchema={validationSchema.current}
                onSubmit={submitForm}>
                {({ values, handleSubmit, handleChange, handleBlur, isSubmitting, errors, touched }) => (
                  <form onSubmit={handleSubmit}>
                    <TextField
                      id="username"
                      name="username"
                      placeholder={i18n.t('S1')}
                      type="email"
                      value={values.username}
                      autoFocus={values.username === ''}
                      autoComplete="username"
                      autoCapitalize="none"
                      autoCorrect="off"
                      spellCheck={false}
                      fullWidth
                      onChange={handleChange}
                      onBlur={handleBlur}
                      helperText={touched.username && errors.username ? errors.username : ' '}
                      error={touched.username && Boolean(errors.username)}
                      variant="filled"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Images.RCiconEmail />
                          </InputAdornment>
                        )
                      }}
                    />
                    <Typography
                      variant="caption"
                      component="div"
                      className={styles.recaptchaTerms}
                      dangerouslySetInnerHTML={{
                        __html: i18n.t('S1238')
                      }}
                    />
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      fullWidth
                      disabled={isSubmitting || !loaded || error}
                      className={styles.submitButton}>
                      {isSubmitting ? <CircularProgress size={24} /> : i18n.t('S38')}
                    </Button>
                  </form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </div>

      <Footer />
    </div>
  )
}

export default ForgotPasswordView
