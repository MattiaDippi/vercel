import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from './../../../assets/styles/themes/Theme'
import Images from './../../../assets/images'
import { largeDevices, BreakpointType } from './../../../assets/styles/mixins/Breakpoints'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateRows: '1fr',
      gridTemplateColumns: '1fr',
      alignItems: 'center',
      justifyItems: 'center',
      width: '100%',
      height: '100%',
      background: `url(${Images.backgroundAuthViews}) no-repeat`,
      backgroundSize: 'cover'
    },
    back: {
      marginBottom: 20,
      marginLeft: -22,
      textTransform: 'uppercase'
    },
    description: {
      maxWidth: 400,
      marginTop: 20,
      marginBottom: 20
    },
    subtitle: {
      marginTop: 20
    },
    captcha: {
      width: 220,
      height: 48,
      marginBottom: 12,
      marginTop: 12
    },
    enterCaptcha: {
      marginBottom: 40
    },
    submitButton: {
      marginTop: 32
    },
    recaptchaTerms: {
      maxWidth: 400
    },
    [largeDevices(theme, BreakpointType.Down)]: {
      container: {
        background: theme.palette.backgroundSecondary.default,
        height: '100%'
      },
      title: {
        display: 'none'
      },
      form: {
        width: '100%',
        height: 'calc(100vh - 32px)',
        margin: 0,
        paddingTop: 0,
        paddingBottom: 0,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
      scrollWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        overflow: 'auto'
      },
      scrollContainer: {
        width: 'calc(100% - 32px)',
        padding: '8px 16px 16px 16px',
        maxWidth: 480
      },
      header: {
        display: 'none'
      },
      back: {
        paddingBottom: 10,
        marginBottom: 0
      },
      description: {
        marginTop: 0
      }
    }
  })
)

export default useStyles
