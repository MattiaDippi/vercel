import App from './../../App'

class LoginUtils {
  static redirectToDefaultPage(redirectFrom?: string) {
    const { router } = App
    if (redirectFrom !== undefined) {
      router.goTo(redirectFrom)
    } else {
      router.goToHome()
    }
  }
}

export default LoginUtils
