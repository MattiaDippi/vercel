import React, { useEffect } from 'react'
import App from './../../../App'
import ViewUtils from './../../../infrastructure/utils/ViewUtils'

export interface LogoutViewProps {
  sessionExpired?: boolean
}

const LogoutView: React.FC<LogoutViewProps> = (props: LogoutViewProps) => {
  useEffect(() => {
    const logout = async () => {
      const { repo, router } = App

      ViewUtils.clearAllState()
      const user = repo.getLoggedUser()
      await repo.logout()

      router.goToLogin({ username: user ? user.username : '' })
    }

    logout()
  }, [])

  return <div></div>
}

export default LogoutView
