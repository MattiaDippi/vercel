import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'
import Images from './../../../assets/images'
import { largeDevices, BreakpointType } from './../../../assets/styles/mixins/Breakpoints'

const ChangePasswordViewStyles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateRows: '1fr',
      gridTemplateColumns: '1fr',
      alignItems: 'center',
      justifyItems: 'center',
      width: '100%',
      height: '100%',
      background: `url(${Images.backgroundAuthViews}) no-repeat`,
      backgroundSize: 'cover'
    },
    back: {
      marginBottom: 32,
      marginLeft: -22,
      textTransform: 'uppercase'
    },
    description: {
      maxWidth: 400,
      marginTop: 20,
      marginBottom: 20
    },
    password: {
      flex: 1
    },
    field: {
      display: 'flex',
      alignItems: 'center',
      marginBottom: 20
    },
    [largeDevices(theme, BreakpointType.Down)]: {
      container: {
        background: theme.palette.backgroundSecondary.default,
        height: '100vh'
      },
      title: {
        ...theme.typography.h5
      },
      form: {
        width: '100%',
        height: 'calc(100vh)',
        margin: 0,
        paddingTop: 0,
        paddingBottom: 0,
        display: 'flex',
        flexDirection: 'column'
      },
      header: {
        display: 'none'
      },
      scrollWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        overflow: 'auto'
      },
      scrollContainer: {
        padding: '8px 16px 16px 16px',
        maxWidth: 480
      },
      description: {
        marginTop: 0,
        maxWidth: '100%'
      },
      back: {
        marginBottom: 0
      }
    }
  })

export default ChangePasswordViewStyles
