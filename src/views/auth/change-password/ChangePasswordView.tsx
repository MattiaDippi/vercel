import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import ChangePasswordViewStyles from './ChangePasswordView.styles'
import Footer from './../../../components/footer/Footer'
import { Button, TextField, IconButton, CircularProgress, Typography, InputAdornment } from '@material-ui/core'
import App from './../../../App'
import { Formik } from 'formik'
import * as Yup from 'yup'
import Images from './../../../assets/images'
import MediumDeviceHader, { MediumDeviceTitleAlignment } from './../../../components/adaptive-layout/MediumDeviceHeader'
import LoginUtils from '../LoginUtils'

export interface ChangePasswordViewProps {
  username: string
  facilityId?: string
}

interface ChangePasswordViewState {
  oldPasswordVisible: boolean
  passwordVisible: boolean
  repeatPasswordVisible: boolean
}

class ChangePasswordView extends Component<
  ChangePasswordViewProps & WithStyles<typeof ChangePasswordViewStyles>,
  ChangePasswordViewState
> {
  captchaCode = ''

  private validationSchema: Yup.ObjectSchema<any>

  constructor(props: ChangePasswordViewProps & WithStyles<typeof ChangePasswordViewStyles>) {
    super(props)

    const { i18n } = App

    this.validationSchema = Yup.object({
      oldPassword: Yup.string().required(i18n.t('S19')),
      password: Yup.string()
        .required(i18n.t('S19'))
        // Password must contain between 8 and 20 characters,
        // and must include at least one upper-case letter and one lower-case letter,
        // and one number or a non-alphabetic character (for example, !, $, #, %). Spaces are not allowed.
        .matches(/(?=^.\S{8,20}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, i18n.t('S417')),
      repeatPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], i18n.t('S24'))
        .required(i18n.t('S19'))
    })

    this.state = {
      oldPasswordVisible: false,
      passwordVisible: false,
      repeatPasswordVisible: false
    }
  }

  componentDidMount() {
    const { username } = this.props
    const { router } = App

    // if props missing go to login
    if (username === undefined) {
      router.goToLogin()
    }
  }

  goBack = () => {
    const { router } = App
    router.goBack()
  }

  submitForm = async (values: any, actions: any) => {
    const { repo, notifier, router, i18n, session } = App
    const { username, oldPassword, password } = values

    notifier.hideMessage()

    const response = await repo.changePassword({
      email: username,
      oldPassword: oldPassword,
      newPassword: password
    })

    if (!response.hasErrors && response.data) {
      notifier.showSuccessMessage(i18n.t('S39'))

      // Login automatically.
      const loginResponse = await repo.login({
        username: username,
        password: password,
        keepUserLoggedIn: session.keepUserLoggedIn
      })

      if (!loginResponse.hasErrors && loginResponse.data) {
        const { user } = loginResponse.data

        if (user) {
          await i18n.changeCulture(user.culture)

          LoginUtils.redirectToDefaultPage()
        } else {
          notifier.showErrorMessage(i18n.t('S7'))

          // Fallback to login.
          router.goToLogin()
        }
      } else {
        notifier.showErrorMessage(loginResponse.getErrorMessage())

        // Fallback to login.
        router.goToLogin()
      }
    } else {
      notifier.showErrorMessage(response.getErrorMessage())
    }

    actions.setSubmitting(false)
  }

  toggleOldPasswordVisibility = () => {
    this.setState({ oldPasswordVisible: !this.state.oldPasswordVisible })
  }

  togglePasswordVisibility = () => {
    this.setState({ passwordVisible: !this.state.passwordVisible })
  }

  toggleRepeatPasswordVisibility = () => {
    this.setState({ repeatPasswordVisible: !this.state.repeatPasswordVisible })
  }

  render() {
    const { i18n } = App
    const { classes, username } = this.props
    const { oldPasswordVisible, passwordVisible, repeatPasswordVisible } = this.state

    return (
      <div className={classes.container}>
        <div>
          <MediumDeviceHader title={i18n.t('S25')} showBack titleAlignment={MediumDeviceTitleAlignment.Center} />
          <div className={classes.header}>
            <Button className={classes.back} onClick={this.goBack}>
              <Images.RCiconBack />
              {i18n.t('S6')}
            </Button>
            <Typography variant="h3" component="div" className={classes.title}>
              {i18n.t('S25')}
            </Typography>
          </div>
          <div className={classes.scrollWrapper}>
            <div className={classes.scrollContainer}>
              <div className={classes.description}>
                <Typography variant="body1" component="div">
                  {i18n.t('S26')}
                </Typography>
                <Typography variant="body1" component="div">
                  {i18n.t('S27')}
                </Typography>
              </div>

              <Formik
                enableReinitialize
                initialValues={{
                  oldPassword: '',
                  password: '',
                  repeatPassword: '',
                  username: username
                }}
                validationSchema={this.validationSchema}
                onSubmit={this.submitForm}>
                {({ values, handleSubmit, handleChange, handleBlur, isSubmitting, errors, touched }) => (
                  <form onSubmit={handleSubmit}>
                    <TextField
                      id="username"
                      name="username"
                      type="email"
                      value={values.username}
                      disabled
                      style={{ display: 'none' }}
                      variant="filled"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Images.RCiconEmail />
                          </InputAdornment>
                        )
                      }}
                    />
                    <div className={classes.field}>
                      <TextField
                        className={classes.password}
                        id="oldPassword"
                        name="oldPassword"
                        placeholder={i18n.t('S28')}
                        autoFocus
                        autoComplete="password"
                        autoCapitalize="none"
                        autoCorrect="off"
                        spellCheck={false}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        helperText={touched.oldPassword && errors.oldPassword ? errors.oldPassword : ' '}
                        error={touched.oldPassword && Boolean(errors.oldPassword)}
                        type={oldPasswordVisible ? 'text' : 'password'}
                        variant="filled"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <Images.RCiconLock />
                            </InputAdornment>
                          )
                        }}
                      />

                      <IconButton onClick={this.toggleOldPasswordVisibility} tabIndex={-1}>
                        {oldPasswordVisible ? <Images.RCiconShow /> : <Images.RCiconHide />}
                      </IconButton>
                    </div>
                    <div className={classes.field}>
                      <TextField
                        className={classes.password}
                        id="password"
                        name="password"
                        placeholder={i18n.t('S29')}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        helperText={touched.password && errors.password ? errors.password : ' '}
                        error={touched.password && Boolean(errors.password)}
                        type={passwordVisible ? 'text' : 'password'}
                        variant="filled"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <Images.RCiconLock />
                            </InputAdornment>
                          )
                        }}
                      />

                      <IconButton onClick={this.togglePasswordVisibility} tabIndex={-1}>
                        {passwordVisible ? <Images.RCiconShow /> : <Images.RCiconHide />}
                      </IconButton>
                    </div>
                    <div className={classes.field}>
                      <TextField
                        className={classes.password}
                        id="repeatPassword"
                        name="repeatPassword"
                        placeholder={i18n.t('S30')}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        helperText={touched.repeatPassword && errors.repeatPassword ? errors.repeatPassword : ' '}
                        error={touched.repeatPassword && Boolean(errors.repeatPassword)}
                        type={repeatPasswordVisible ? 'text' : 'password'}
                        variant="filled"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <Images.RCiconLock />
                            </InputAdornment>
                          )
                        }}
                      />
                      <IconButton onClick={this.toggleRepeatPasswordVisibility} tabIndex={-1}>
                        {repeatPasswordVisible ? <Images.RCiconShow /> : <Images.RCiconHide />}
                      </IconButton>
                    </div>

                    <Button type="submit" variant="contained" color="primary" fullWidth disabled={isSubmitting}>
                      {isSubmitting ? <CircularProgress size={24} /> : i18n.t('S31')}
                    </Button>
                  </form>
                )}
              </Formik>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default withStyles(ChangePasswordViewStyles)(ChangePasswordView)
