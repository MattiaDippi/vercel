import React, { Component } from 'react'
import App from './../../../App'
import LoginView from '../login/LoginView'
import BaseDialog from './../../../components/dialogs/base-dialog/BaseDialog'
import Routes from './../../../infrastructure/routing/Routes'

export interface SessionExpiredDialogProps {
  open: boolean
}

class SessionExpiredDialog extends Component<SessionExpiredDialogProps> {
  componentDidMount() {
    const { open } = this.props

    if (open) {
      this.trackPageView()
    }
  }

  componentDidUpdate(prevProps: SessionExpiredDialogProps) {
    if (this.props.open && this.props.open !== prevProps.open) {
      this.trackPageView()
    }
  }

  trackPageView = () => {
    const { tracker } = App
    tracker.trackPageView(Routes.SessionExpired)
  }

  render() {
    const { open } = this.props

    return (
      <BaseDialog open={open} fullScreen noContentPadding disableBackdropClick disableEscapeKeyDown>
        <LoginView sessionExpired />
      </BaseDialog>
    )
  }
}

export default SessionExpiredDialog
