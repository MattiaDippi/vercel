import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import {
  FormControlLabel,
  CircularProgress,
  Button,
  TextField,
  Switch,
  Typography,
  InputAdornment
} from '@material-ui/core'
import Images from './../../../assets/images'
import * as Yup from 'yup'
import { Formik } from 'formik'
import App from './../../../App'
import LoginViewStyles from './LoginView.styles'
import Footer from './../../../components/footer/Footer'
import LoginUtils from './../LoginUtils'
import Routes from './../../../infrastructure/routing/Routes'
import RouterLink from './../../../components/material-ui-router-link/MaterialUIRouterLink'
import LoginExternalParams from '../LoginExternalParams'
import MediumDeviceHader from './../../../components/adaptive-layout/MediumDeviceHeader'

export interface LoginViewProps extends LoginExternalParams {
  canGoBack?: boolean
  username?: string
  sessionExpired?: boolean
}

interface LoginViewState {
  username: string
  password: string
  isLoggingOut: boolean
}

class LoginView extends Component<LoginViewProps & WithStyles<typeof LoginViewStyles>, LoginViewState> {
  static defaultProps = {
    switchUser: false,
    canGoBack: false,
    sessionExpired: false
  }

  private validationSchema: Yup.ObjectSchema<any>

  private cookiesEnabled: boolean

  constructor(props: LoginViewProps & WithStyles<typeof LoginViewStyles>) {
    super(props)

    const { config, i18n, storage } = App
    const { username, sessionExpired } = props

    this.validationSchema = Yup.object({
      username: Yup.string()
        .required(i18n.t('S19'))
        .email(i18n.t('S20')),
      password: Yup.string().required(i18n.t('S19'))
    })

    this.cookiesEnabled = storage.technicalCookiesEnabled

    const defaultUsername = username || config.defaultAccountUsername
    const defaultPassword = config.defaultAccountPassword

    this.state = {
      username: defaultUsername,
      password: sessionExpired ? '' : defaultPassword,
      isLoggingOut: false
    }
  }

  componentDidMount() {
    const { repo } = App
    const { sessionExpired, redirectFrom } = this.props

    if (!sessionExpired) {
      if (repo.userIsAuthenticated()) {
        // User is already authenticated, redirect to default page.
        LoginUtils.redirectToDefaultPage(redirectFrom)
      }
    }
  }

  login = async (username: string, password: string, keepUserLoggedIn: boolean): Promise<boolean> => {
    const { redirectFrom } = this.props
    let result = false
    const { i18n, repo, router, notifier, session } = App
    notifier.hideMessage()

    session.keepUserLoggedIn = keepUserLoggedIn
    const loginResponse = await repo.login({ username, password, keepUserLoggedIn })

    if (!loginResponse.hasErrors && loginResponse.data) {
      const { user } = loginResponse.data

      if (user) {
        await i18n.changeCulture(user.culture)
        result = true

        LoginUtils.redirectToDefaultPage(redirectFrom)
      } else {
        this.setState({ password: '' })
        notifier.showErrorMessage(i18n.t('S7'))
      }
    } else {
      if (loginResponse.errors && loginResponse.errors[0].field === 'PasswordMustBeChanged') {
        router.goToChangePassword({ username: username })
      } else {
        notifier.showErrorMessage(loginResponse.getErrorMessage())
        this.setState({ password: '' })
      }
    }

    return result
  }

  restoreSession = async (username: string, password: string): Promise<boolean> => {
    const { redirectFrom } = this.props
    const result = false
    const { repo, notifier, router, session } = App
    const loginResponse = await repo.login({
      username: username,
      password: password,
      keepUserLoggedIn: session.keepUserLoggedIn
    })

    if (!loginResponse.hasErrors && loginResponse.data) {
      LoginUtils.redirectToDefaultPage(redirectFrom)
    } else {
      if (loginResponse.errors && loginResponse.errors[0].field === 'PasswordMustBeChanged') {
        await repo.logout()
        router.goToChangePassword({ username })
      } else {
        notifier.showErrorMessage(loginResponse.getErrorMessage())
        this.setState({ password: '' })
      }
    }

    return result
  }

  goBack = () => {
    const { router } = App

    router.goBack()
  }

  goToLogout = () => {
    this.setState({ isLoggingOut: true })

    const { router } = App
    router.goToLogout()
  }

  submitForm = async (values: any, actions: any) => {
    let result = false
    const { sessionExpired } = this.props

    if (!sessionExpired) {
      result = await this.login(values.username, values.password, values.keepUserLoggedIn)
    } else {
      result = await this.restoreSession(values.username, values.password)
    }

    // Clear password field in case of login failed
    if (!result) {
      actions.setFieldValue('password', '')
      actions.setSubmitting(false)
    }
  }

  render() {
    const { classes, canGoBack, sessionExpired } = this.props
    const { i18n } = App
    const { username, password, isLoggingOut } = this.state

    return (
      <div className={classes.container}>
        <Formik
          initialValues={{ username: username, password: password, keepUserLoggedIn: false }}
          enableReinitialize
          validationSchema={this.validationSchema}
          onSubmit={this.submitForm}>
          {({ values, handleSubmit, handleChange, handleBlur, isSubmitting, errors, touched }) => (
            <form method="post" onSubmit={handleSubmit} noValidate className={classes.formContainer}>
              {canGoBack && <MediumDeviceHader showBack />}
              <div className={classes.formWrapper}>
                <div className={classes.form}>
                  <div className={classes.header}>
                    {canGoBack && (
                      <Button className={classes.back} onClick={this.goBack}>
                        <Images.RCiconBack />
                        {i18n.t('S6')}
                      </Button>
                    )}
                  </div>

                  {sessionExpired && (
                    <Typography variant="h3" className={classes.title}>
                      {i18n.t('S75')}
                    </Typography>
                  )}

                  {this.cookiesEnabled ? (
                    <div>
                      <div className={classes.formField}>
                        <TextField
                          id="username"
                          name="username"
                          placeholder={i18n.t('S1')}
                          type="email"
                          value={values.username}
                          autoFocus={values.username === ''}
                          autoComplete="username"
                          autoCapitalize="none"
                          autoCorrect="off"
                          spellCheck={false}
                          fullWidth
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.username && errors.username ? errors.username : ' '}
                          error={touched.username && Boolean(errors.username)}
                          variant="filled"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <Images.RCiconEmail />
                              </InputAdornment>
                            )
                          }}
                        />
                      </div>
                      <div className={classes.formField}>
                        <TextField
                          id="password"
                          name="password"
                          placeholder={i18n.t('S2')}
                          type="password"
                          value={values.password}
                          autoFocus={values.username !== ''}
                          autoComplete="current-password"
                          autoCapitalize="none"
                          autoCorrect="off"
                          spellCheck={false}
                          fullWidth
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.password && errors.password ? errors.password : ' '}
                          error={touched.password && Boolean(errors.password)}
                          variant="filled"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <Images.RCiconLock />
                              </InputAdornment>
                            )
                          }}
                        />
                      </div>
                      {!sessionExpired && (
                        <div>
                          <FormControlLabel
                            control={
                              <Switch
                                checked={values.keepUserLoggedIn}
                                id="keepUserLoggedIn"
                                name="keepUserLoggedIn"
                                onChange={handleChange}
                                value="checked"
                                color="primary"
                              />
                            }
                            label={i18n.t('##Keep me logged in')}
                          />

                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                            disabled={isSubmitting}
                            className={classes.loginButton}>
                            {isSubmitting ? <CircularProgress size={24} /> : i18n.t('S4')}
                          </Button>

                          <div className={classes.formLink}>
                            <RouterLink
                              to={{
                                route: Routes.ForgotPassword,
                                params: { username: username }
                              }}>
                              {i18n.t('S21')}
                            </RouterLink>
                          </div>
                        </div>
                      )}
                      <div>
                        {sessionExpired && (
                          <React.Fragment>
                            <Button
                              variant="outlined"
                              color="primary"
                              onClick={this.goToLogout}
                              disabled={isSubmitting || isLoggingOut}>
                              {isLoggingOut ? <CircularProgress size={24} /> : i18n.t('S74')}
                            </Button>
                            <Button
                              type="submit"
                              variant="contained"
                              color="primary"
                              disabled={isSubmitting || isLoggingOut}
                              className={classes.onRight}>
                              {isSubmitting ? <CircularProgress size={24} /> : i18n.t('S4')}
                            </Button>
                          </React.Fragment>
                        )}
                      </div>
                    </div>
                  ) : (
                    <div>
                      <Typography className={classes.cookiesDescription} variant="body1">
                        {i18n.t('S42')}
                      </Typography>
                      <div>
                        <RouterLink to={Routes.CookiesSettings}>{i18n.t('S43')}</RouterLink>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </form>
          )}
        </Formik>
        {!sessionExpired && <Footer />}
      </div>
    )
  }
}

export default withStyles(LoginViewStyles)(LoginView)
