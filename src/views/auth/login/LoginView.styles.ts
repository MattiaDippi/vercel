import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'
import Images from './../../../assets/images'
import { largeDevices, BreakpointType } from './../../../assets/styles/mixins/Breakpoints'

const LoginViewStyles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'grid',
      gridTemplateRows: '1fr',
      gridTemplateColumns: '1fr',
      alignItems: 'center',
      justifyItems: 'center',
      width: '100%',
      height: '100%',
      background: `url(${Images.backgroundAuthViews}) no-repeat`,
      backgroundSize: 'cover'
    },
    title: {
      marginBottom: 20
    },
    form: {
      width: 400
    },
    formField: {
      marginBottom: 16
    },
    formLink: {
      marginTop: 12,
      marginBottom: 12
    },
    userLoggedInfo: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 32
    },
    usernameContainer: {
      marginLeft: 16
    },
    back: {
      marginBottom: 24,
      marginLeft: -22,
      textTransform: 'uppercase'
    },
    cookiesDescription: {
      marginTop: 20,
      marginBottom: 20
    },
    avatar: {
      backgroundColor: theme.palette.background.paper
    },
    onRight: {
      float: 'right'
    },
    loginButton: {
      marginTop: 16,
      marginBottom: 16
    },
    hintText: {
      marginBottom: 24
    },
    [largeDevices(theme, BreakpointType.Down)]: {
      container: {
        background: theme.palette.backgroundSecondary.default,
        height: '100%'
      },
      header: {
        display: 'none'
      },
      title: {
        ...theme.typography.h5
      },
      formContainer: {
        width: '100%',
        height: 'calc(100vh)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
      form: {
        width: '100%',
        maxWidth: 480,
        height: '100%',
        margin: 0,
        padding: 20
      },
      formWrapper: {
        overflow: 'auto',
        width: '100%',
        height: '100%',
        margin: 0,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 20
      },
      userLoggedInfo: {
        flexDirection: 'column'
      },
      usernameContainer: {
        marginLeft: 0
      }
    }
  })

export default LoginViewStyles
