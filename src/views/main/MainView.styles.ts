import { createStyles } from '@material-ui/core'
import Theme from './../../assets/styles/themes/Theme'
import { smallDevices } from './../../assets/styles/mixins/Breakpoints'

const MainViewStyles = (theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      height: '100%'
    },
    contentContainerWrapper: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
      overflow: 'hidden'
    },
    contentContainer: {
      display: 'flex',
      flex: 1,
      overflow: 'hidden'
    },
    newVersionBannerContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      minHeight: 40,
      padding: '4px 52px 4px 8px',
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      position: 'relative',
      paddingRight: 52
    },
    closeNewVersionBannerButton: {
      padding: 6,
      position: 'absolute',
      right: 8
    },
    closeNewVersionBannerButtonIcon: {
      '& path': {
        fill: theme.palette.primary.contrastText
      }
    },
    updateNewVersionButton: {
      color: theme.palette.primary.contrastText,
      marginLeft: 8
    },
    [smallDevices(theme)]: {
      container: {
        flexDirection: 'column-reverse'
      }
    }
  })

export default MainViewStyles
