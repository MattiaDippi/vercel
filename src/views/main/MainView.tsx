import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import {
  ReactRouter as Router,
  ReactRouterSwitch as Switch,
  ReactRouterPublicRoute as PublicRoute,
  ReactRouterPrivateRoute as PrivateRoute,
  DefaultRoutes,
  reactI18NextWithTranslation as withTranslation,
  getCultureMapping,
  CultureMapping,
  CultureChangedEventArgs,
  DeviceType
} from 'mw-react-web-app-infrastructure'
import { Typography, IconButton, Button } from '@material-ui/core'
import DynamicHead from './../../components/dynamic-head/DynamicHead'
import Notifier from './../../components/notifier/Notifier'
import TrackingCategoryType from './../../infrastructure/tracking/TrackingCategoryType'
import TrackingActionType from './../../infrastructure/tracking/TrackingActionType'
import App from './../../App'
import Images from './../../assets/images'
import MainViewStyles from './MainView.styles'
import Routes from './../../infrastructure/routing/Routes'
import SessionExpiredDialog from './../../views/auth/session-expired/SessionExpiredDialog'
import HomeView from './../home/HomeView'
import LoginView from './../auth/login/LoginView'
import LogoutView from './../auth/logout/LogoutView'
import AccessDeniedView from './../infrastructure/access-denied/AccessDeniedView'
import ViewUtils from './../../infrastructure/utils/ViewUtils'
import ErrorView from './../../views/infrastructure/error/ErrorView'
import ForgotPasswordView from './../../views/auth/forgot-password/ForgotPasswordView'
import ChangePasswordView from './../../views/auth/change-password/ChangePasswordView'
import SendFeedbackDialog from './../../views/help/send-feedback/SendFeedbackDialog'

const browsersMinimumVersions: { [key: string]: number } = {
  Safari: 11,
  Edge: 79
}

const isBrowserUnsupported = (browserName: string, browserMajorVersion: number): boolean => {
  const browserMinimumVersion = browsersMinimumVersions[browserName]
  return browserMinimumVersion ? browserMajorVersion < browserMinimumVersion : false
}

interface MainViewState {
  userIsAuthenticated: boolean
  sessionExpired: boolean
  pickersLocale: string
  showNewVersionBanner: boolean
}

class MainView extends Component<WithStyles<typeof MainViewStyles>, MainViewState> {
  private userAuthenticatedEventToken = ''

  private userNotAuthenticatedEventToken = ''

  private sessionExpiredEventToken = ''

  private cultureChangedEventToken = ''

  constructor(props: WithStyles<typeof MainViewStyles>) {
    super(props)

    const { i18n } = App

    this.state = {
      userIsAuthenticated: this.userIsAuthenticated(),
      sessionExpired: false,
      pickersLocale: this.getPickersLocale(i18n.currentCulture),
      showNewVersionBanner: false
    }
  }

  componentDidMount() {
    const { events, device, notifier, i18n } = App

    if (device.type === DeviceType.Desktop && isBrowserUnsupported(device.browserName, device.browserMajorVersion)) {
      notifier.showErrorMessage(i18n.t('S426', true, device.browserName), {
        autoHide: false,
        showCloseButton: true
      })
    }

    this.userAuthenticatedEventToken = events.subscribeUserAuthenticated(this.onUserAuthenticated)
    this.userNotAuthenticatedEventToken = events.subscribeUserNotAuthenticated(this.onUserNotAuthenticated)
    this.sessionExpiredEventToken = events.subscribeSessionExpired(this.onSessionExpired)
    this.cultureChangedEventToken = events.subscribeCultureChanged(this.onCultureChanged)

    window.addEventListener('sw-installed', this.onNewServiceWorkerInstalled, false)
  }

  componentWillUnmount() {
    const { events } = App

    events.unsubscribe(this.userAuthenticatedEventToken)
    events.unsubscribe(this.userNotAuthenticatedEventToken)
    events.unsubscribe(this.sessionExpiredEventToken)
    events.unsubscribe(this.cultureChangedEventToken)
    window.removeEventListener('sw-installed', this.onNewServiceWorkerInstalled)
  }

  onUserAuthenticated = () => {
    this.setState({ userIsAuthenticated: true, sessionExpired: false })
  }

  onUserNotAuthenticated = async () => {
    this.setState({ userIsAuthenticated: false, sessionExpired: false })

    const { i18n } = App
    await i18n.changeCulture(App.getDeviceCulture())
  }

  onSessionExpired = () => {
    this.setState({ sessionExpired: true })
  }

  onCultureChanged = (eventArgs: CultureChangedEventArgs) => {
    this.setState({
      pickersLocale: this.getPickersLocale(eventArgs.culture)
    })
  }

  onNewServiceWorkerInstalled = () => {
    this.setState({
      showNewVersionBanner: true
    })
  }

  onRefreshToInstallNewVersion = () => {
    if (navigator.serviceWorker.controller) {
      this.setState({
        showNewVersionBanner: false
      })

      const { tracker } = App
      tracker.trackEvent(TrackingCategoryType.Application, TrackingActionType.UpdateApplication)

      window.postMessage({ type: 'SKIP_WAITING' }, window.location.origin)
    }
  }

  onCloseInstallNewVersionBanner = () => {
    this.setState({
      showNewVersionBanner: false
    })
  }

  onNavigatingFromMenu = () => {
    ViewUtils.clearAllState()
  }

  userIsAuthenticated = (): boolean => {
    const { repo } = App

    return repo.userIsAuthenticated()
  }

  getPickersLocale = (culture: string): string => {
    const mapping: CultureMapping = getCultureMapping(culture)

    return mapping.momentLocale
  }

  render() {
    const { router, i18n, version } = App
    const { history } = router
    const { sessionExpired, pickersLocale, showNewVersionBanner } = this.state
    const { classes } = this.props

    return (
      <React.Fragment>
        <DynamicHead version={version} />
        <MuiPickersUtilsProvider
          utils={i18n.date.thirdPartyLibraryUtils}
          libInstance={i18n.date.thirdPartyLibraryInstance}
          locale={pickersLocale}>
          <div className={classes.container}>
            <Notifier />

            <div className={classes.contentContainerWrapper}>
              {showNewVersionBanner && (
                <div className={classes.newVersionBannerContainer}>
                  <Typography variant="body1">{i18n.t('S418')}</Typography>
                  <Button
                    variant="text"
                    size="small"
                    className={classes.updateNewVersionButton}
                    onClick={this.onRefreshToInstallNewVersion}>
                    {i18n.t('S436')}
                  </Button>
                  <IconButton
                    className={classes.closeNewVersionBannerButton}
                    onClick={this.onCloseInstallNewVersionBanner}>
                    <Images.RCiconClear className={classes.closeNewVersionBannerButtonIcon} />
                  </IconButton>
                </div>
              )}
              <div className={classes.contentContainer}>
                <Router history={history}>
                  <React.Fragment>
                    <Switch history={history}>
                      {/* Auth */}
                      <PublicRoute path={DefaultRoutes.Login} exact component={LoginView} errorComponent={ErrorView} />
                      <PublicRoute
                        path={DefaultRoutes.Logout}
                        exact
                        component={LogoutView}
                        errorComponent={ErrorView}
                      />
                      <PrivateRoute
                        path={DefaultRoutes.AccessDenied}
                        exact
                        component={AccessDeniedView}
                        errorComponent={ErrorView}
                        userIsAuthenticated={this.userIsAuthenticated}
                        userCanAccess={() => true}
                      />
                      <PublicRoute
                        path={Routes.ForgotPassword}
                        exact
                        component={ForgotPasswordView}
                        errorComponent={ErrorView}
                      />
                      <PublicRoute
                        path={Routes.ChangePassword}
                        exact
                        component={ChangePasswordView}
                        errorComponent={ErrorView}
                      />

                      {/* Home */}
                      <PrivateRoute
                        path={Routes.Home}
                        exact
                        component={HomeView}
                        errorComponent={ErrorView}
                        userIsAuthenticated={this.userIsAuthenticated}
                        userCanAccess={() => true}
                      />

                      {/* Fallback: redirect to home page if path doesn't match any route. */}
                      <PrivateRoute
                        component={HomeView}
                        errorComponent={ErrorView}
                        userIsAuthenticated={this.userIsAuthenticated}
                        userCanAccess={() => true}
                      />
                    </Switch>

                    {/* Modals: put modal routes here outside switch. */}
                    <PrivateRoute
                      path={Routes.SendFeeback}
                      exact
                      component={SendFeedbackDialog}
                      errorComponent={ErrorView}
                      userIsAuthenticated={this.userIsAuthenticated}
                      userCanAccess={() => true}
                    />
                  </React.Fragment>
                </Router>
                <SessionExpiredDialog open={sessionExpired} />
              </div>
            </div>
          </div>
        </MuiPickersUtilsProvider>
      </React.Fragment>
    )
  }
}

export default withTranslation(withStyles(MainViewStyles)(MainView))
