interface SendFeedbackCategory {
  id: string
  text: string
}

export default SendFeedbackCategory
