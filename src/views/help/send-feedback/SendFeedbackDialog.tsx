import React, { Component } from 'react'
import { WithStyles, withStyles, Select, FormControl, InputLabel, MenuItem, FormHelperText } from '@material-ui/core'
import SendFeedbackDialogStyles from './SendFeedbackDialog.styles'
import FormDialog, { isFormikFormValid } from './../../../components/dialogs/form-dialog/FormDialog'
import App from '../../../App'
import * as Yup from 'yup'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import ToggleButton from '@material-ui/lab/ToggleButton'
import clsx from 'clsx'
import DialogActionType from './../../../components/dialogs/models/DialogActionType'
import DialogAction from './../../../components/dialogs/models/DialogAction'
import { BaseDialogSize, BaseDialogTitlePosition } from './../../../components/dialogs/base-dialog/BaseDialog'
import SendFeedbackCategory from './SendFeedbackCategory'
import NavigationConfirmationDialog from './../../../views/infrastructure/navigation-confirmation-dialog/NavigationConfirmationDialog'
import SendFeedbackRequest from './../../../infrastructure/repository/application/models/SendFeedbackRequest'
import { Formik } from 'formik'
import DebouncedTextArea from './../../../components/debounced-text-area/DebouncedTextArea'
import Images from './../../../assets/images'

export enum FeedbackType {
  Issue = 'Issue',
  Suggestion = 'Suggestion'
}

export interface SendFeedbackProps {
  type?: FeedbackType
}

interface SendFeedbackValues {
  toggleValues: FeedbackType[]
  category: string
  body: string
}

class SendFeedbackDialog extends Component<SendFeedbackProps & WithStyles<typeof SendFeedbackDialogStyles>> {
  formRef?: Formik<SendFeedbackValues>

  private validationSchema: Yup.ObjectSchema<any>

  private actions: DialogAction<SendFeedbackValues>[]

  private categories: SendFeedbackCategory[]

  sendFeedbackRequest: SendFeedbackRequest

  initialValues: SendFeedbackValues

  constructor(props: SendFeedbackProps & WithStyles<typeof SendFeedbackDialogStyles>) {
    super(props)

    const { i18n } = App

    this.validationSchema = Yup.object({
      category: Yup.string().required(i18n.t('S19')),
      body: Yup.string().required(i18n.t('S19'))
    })

    this.actions = [
      {
        text: i18n.t('S167'),
        type: DialogActionType.Close,
        handler: this.closeDialog
      },
      {
        text: i18n.t('S168'),
        handler: this.sendFeedback,
        type: DialogActionType.Primary,
        submit: true
      }
    ]

    this.categories = []

    this.initialValues = {
      toggleValues: [props.type || FeedbackType.Suggestion],
      category: '',
      body: ''
    }

    this.sendFeedbackRequest = this.getSendFeedbackRequest(this.initialValues)
  }

  async componentDidMount() {
    const { router } = App
    router.blockNavigation(this.shouldPromptNavigationConfirmation)
  }

  componentWillUnmount() {
    const { router } = App
    router.stopBlockingNavigation()
  }

  shouldPromptNavigationConfirmation = (): boolean => {
    if (this.sendFeedbackRequest && this.formRef) {
      const currentSendFeedbackRequest = this.getSendFeedbackRequest(this.formRef.state.values)
      return JSON.stringify(currentSendFeedbackRequest) !== JSON.stringify(this.sendFeedbackRequest)
    }

    return false
  }

  closeDialog = () => {
    const { router } = App
    router.goBack()
  }

  getSendFeedbackRequest = (values: SendFeedbackValues): SendFeedbackRequest => {
    const { device, version, name } = App

    const body = `${values.toggleValues[0]}, ${values.category}\n\n${values.body}\n\n${name} ${version}, ${
      device.type
    }, ${device.vendor !== '' ? device.vendor + ', ' : ''}${device.osName} ${device.osVersion}, ${
      device.model !== '' ? device.model + ', ' : ''
    }${device.browserName} ${device.browserVersion}, ${device.browserEngineName} ${device.browserEngineVersion}, ${
      device.cpuArchitecture !== '' ? device.cpuArchitecture + ', ' : ''
    }${device.orientation}, ${device.culture}`

    return { subject: 'Professional user feedback', body: body }
  }

  sendFeedback = async (values: SendFeedbackValues): Promise<boolean> => {
    const { repo, notifier, i18n, router } = App
    const user = repo.getLoggedUser()
    let error = true

    if (user) {
      const response = await repo.sendFeedback(this.getSendFeedbackRequest(values), user.id)

      if (!response.hasErrors && response.data) {
        notifier.showSuccessMessage(i18n.t('S169'))
        error = false
      } else {
        notifier.showErrorMessage(response.getErrorMessage())
      }
    }

    if (!error) {
      router.stopBlockingNavigation()
      this.closeDialog()

      return true
    }
    return false
  }

  onSelectFeedbackType = (value: string, setFieldValue: any) => {
    if (value.length > 0) {
      setFieldValue('toggleValues', [value[value.length - 1]], true)
    }
  }

  onSelectCategory = (value: any, setFieldValue: any) => {
    setFieldValue('category', value, true)
  }

  render() {
    const { classes } = this.props
    const { i18n } = App

    return (
      <FormDialog<SendFeedbackValues>
        open
        onClose={this.closeDialog}
        title={i18n.t('S163')}
        subtitle={i18n.t('S164')}
        titlePosition={BaseDialogTitlePosition.Left}
        initialValues={this.initialValues}
        validationSchema={this.validationSchema}
        actions={this.actions}
        size={BaseDialogSize.Large}
        formRef={r => (this.formRef = r)}>
        {({ values, handleBlur, errors, touched, setFieldValue, setFieldTouched, validateForm }) => (
          <div className={classes.container}>
            <NavigationConfirmationDialog
              dialogTitle={i18n.t('S677')}
              dialogText=""
              closeWithoutSavingText={i18n.t('S678')}
              saveText={i18n.t('S168')}
              onSave={async () => {
                const isFormValid = await isFormikFormValid<SendFeedbackValues>(values, setFieldTouched, validateForm)

                let result = false
                if (isFormValid) {
                  result = await this.sendFeedback(values)
                }
                return result
              }}
            />

            <ToggleButtonGroup
              id="toggleValues"
              value={values.toggleValues}
              onChange={(e, value) => this.onSelectFeedbackType(value, setFieldValue)}>
              <ToggleButton
                value={FeedbackType.Suggestion}
                className={clsx(
                  classes.toggleButton,
                  values.toggleValues.indexOf(FeedbackType.Suggestion) > -1 ? 'selected' : ''
                )}>
                <Images.RCiconFeedbackIdea className={classes.toggleButtonIcon} />
                <span className={classes.toggleButtonLabel}>{i18n.t('S161')}</span>
              </ToggleButton>
              <ToggleButton
                value={FeedbackType.Issue}
                className={clsx(
                  classes.toggleButton,
                  values.toggleValues.indexOf(FeedbackType.Issue) > -1 ? 'selected' : ''
                )}>
                <Images.RCiconFeedbackIssue className={classes.toggleButtonIcon} />
                <span className={classes.toggleButtonLabel}>{i18n.t('S162')}</span>
              </ToggleButton>
            </ToggleButtonGroup>
            <div className={classes.selectCategoryContainer}>
              <FormControl className={classes.selectCategory}>
                <InputLabel htmlFor="category"> {i18n.t('S165')}</InputLabel>
                <Select
                  placeholder={i18n.t('S165')}
                  value={values.category}
                  onChange={e => this.onSelectCategory(e.target.value, setFieldValue)}
                  onBlur={handleBlur}
                  error={touched.category && Boolean(errors.category)}
                  IconComponent={props => <Images.RCiconArrowDown className={classes.selectIcon} />}
                  inputProps={{
                    id: 'category',
                    name: 'category'
                  }}>
                  <MenuItem key={0} value="" disabled>
                    {i18n.t('S165')}
                  </MenuItem>
                  {this.categories.map(item => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.text}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              {touched.category && Boolean(errors.category) && (
                <FormHelperText error>{errors.category ? errors.category : ''}</FormHelperText>
              )}
            </div>

            <DebouncedTextArea
              id="body"
              name="body"
              variant="outlined"
              placeholder={i18n.t(values.toggleValues.indexOf(FeedbackType.Issue) > -1 ? 'S166' : 'S431')}
              className={classes.textArea}
              fullWidth
              multiline
              rows={8}
              rowsMax={8}
              onChangeValue={(value: string, name?: string) => {
                if (name) setFieldValue(name, value)
              }}
              onBlur={handleBlur}
              error={touched.body && Boolean(errors.body)}
              value={values.body}
              helperText={touched.body && errors.body ? errors.body : ' '}
              inputProps={{ maxLength: 4000 }}
              FormHelperTextProps={{
                variant: 'standard'
              }}
            />
          </div>
        )}
      </FormDialog>
    )
  }
}

export default withStyles(SendFeedbackDialogStyles)(SendFeedbackDialog)
