import { createStyles } from '@material-ui/core'
import Theme from '../../../assets/styles/themes/Theme'
import ColorUtils from './../../../infrastructure/utils/ColorUtils'

const SendFeedbackDialogStyles = (theme: Theme) =>
  createStyles({
    container: {
      minHeight: 428
    },
    toggleButton: {
      width: 224,
      height: 96,
      borderRadius: 1,
      color: theme.palette.primary.main,
      border: `solid 1px ${theme.palette.border.main}`,
      lineHeight: 'normal',
      textTransform: 'none',
      backgroundColor: theme.palette.background.paper,
      justifyContent: 'start',
      textAlign: 'left',
      paddingLeft: 0,
      '&:hover': {
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.text.secondary, 0.1)
      },
      '&.selected': {
        color: theme.palette.primary.main,
        border: `solid 1px ${ColorUtils.hexToRgbString(theme.palette.primary.main, 0.1)}`,
        backgroundColor: ColorUtils.hexToRgbString(theme.palette.primary.main, 0.1),
        cursor: 'default',
        pointerEvents: 'none'
      },
      '&.MuiToggleButton-root:last-child': {
        paddingLeft: 0
      },
      '&.MuiToggleButton-root:not(:last-child)': {
        marginRight: 32
      },
      '&.MuiToggleButton-root:not(first-child):not(.selected)': {
        'border-left': `solid 1px #cccccc`
      }
    },
    toggleButtonIcon: {
      width: 94,
      height: 94,
      '& g': {
        stroke: theme.palette.primary.main
      }
    },
    toggleButtonLabel: {
      flex: 1
    },
    selectCategoryContainer: {
      marginTop: 24,
      marginBottom: 24
    },
    selectCategory: {
      width: 480
    },
    selectIcon: {
      top: 'calc(50% - 12px)',
      right: 4,
      position: 'absolute',
      pointerEvents: 'none'
    },
    textArea: {
      display: 'block',
      marginBottom: 5,
      width: 480
    },
    buttonContainer: {
      display: 'flex',
      justifyContent: 'flex-end'
    }
  })

export default SendFeedbackDialogStyles
