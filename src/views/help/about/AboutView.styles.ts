import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flex: 1,
      overflowY: 'auto',
      padding: 24
    },
    infoContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'flex-start'
    },
    appInfoContainer: {
      marginLeft: 16
    },
    appLogo: {
      width: 140,
      height: 140
    },
    appInfoList: {
      listStyleType: 'none',
      margin: '8px 0 0 0',
      padding: 0
    }
  })
)

export default useStyles
