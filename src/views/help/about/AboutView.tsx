import React from 'react'
import Images from 'assets/images'
import App from 'App'
import useStyles from './AboutView.styles'
import { Typography } from '@material-ui/core'

const AboutView: React.FC = () => {
  const { device } = App
  const styles = useStyles()

  return (
    <div className={styles.container}>
      <div className={styles.infoContainer}>
        <img src={Images.logoEndUser} className={styles.appLogo} alt="Professional" />
        <div className={styles.appInfoContainer}>
          <Typography variant="h3">Professional</Typography>
          <ul className={styles.appInfoList}>
            <li>
              <Typography variant="body1">{`Version: ${App.version}`}</Typography>
            </li>
            <li>
              <Typography variant="body1">{`Browser: ${device.browserName} ${device.browserVersion}`}</Typography>
            </li>
            <li>
              <Typography variant="body1">{`OS: ${device.osName} ${device.osVersion}`}</Typography>
            </li>
            <li>
              <Typography variant="body1">{`Device type: ${device.type}`}</Typography>
            </li>
            {device.vendor !== '' ||
              (device.model !== '' && (
                <li>
                  {`Device: ${device.vendor !== '' ? device.vendor + ', ' : ''}
                ${device.model !== '' ? device.model : ''}`}
                </li>
              ))}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default AboutView
