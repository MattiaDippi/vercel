import { createStyles, makeStyles } from '@material-ui/core/styles'
import Theme from 'assets/styles/themes/Theme'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    confirmDialog: {
      zIndex: theme.zIndex.modal + 2
    }
  })
)

export default useStyles
