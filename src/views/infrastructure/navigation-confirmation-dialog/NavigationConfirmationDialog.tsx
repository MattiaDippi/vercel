import React, { useState, useCallback, useRef, useEffect } from 'react'
import useStyles from './NavigationConfirmationDialog.styles'
import ConfirmDialog from './../../../components/dialogs/confirm-dialog/ConfirmDialog'
import App from './../../../App'
import DialogActionType from './../../../components/dialogs/models/DialogActionType'
import CommonUtils from './../../../infrastructure/utils/CommonUtils'
import { ReactRouterPrompt, Location } from 'mw-react-web-app-infrastructure'
import DialogAction from './../../../components/dialogs/models/DialogAction'
import { BaseDialogSize } from './../../../components/dialogs/base-dialog/BaseDialog'
import clsx from 'clsx'

interface NavigationConfirmationDialogProps {
  dialogTitle?: string
  dialogText?: string
  closeWithoutSavingText?: string
  cancelText?: string
  saveText?: string
  hideSaveButton?: boolean
  className?: string
  onSave?: () => Promise<boolean>
  onContinueNavigating?: () => void
}

const NavigationConfirmationDialog: React.FC<NavigationConfirmationDialogProps> = React.memo(
  (props: NavigationConfirmationDialogProps) => {
    const { i18n } = App
    const styles = useStyles()

    const {
      dialogTitle = i18n.t('S673'),
      dialogText = i18n.t('S676'),
      closeWithoutSavingText = i18n.t('S675'),
      cancelText = i18n.t('S167'),
      saveText = i18n.t('S674'),
      hideSaveButton,
      className,
      onSave,
      onContinueNavigating
    } = props

    const [open, setOpen] = useState<boolean>(false)
    const trigger = useRef(Symbol.for(`PreventTransitionPrompt_${CommonUtils.getId()}`))
    const navigationCallback = useRef<((result: boolean) => void) | undefined>()

    const onClose = useCallback(
      async (continueNavigating: boolean, save: boolean) => {
        setOpen(false)

        let result = true
        if (save && onSave) {
          result = await onSave()
        }

        if (continueNavigating && onContinueNavigating) onContinueNavigating()

        if (navigationCallback.current && result) navigationCallback.current(continueNavigating)
      },
      [onContinueNavigating, onSave]
    )

    const actions = ([
      {
        text: closeWithoutSavingText,
        handler: () => onClose(true, false)
      },
      {
        text: cancelText,
        handler: () => onClose(false, false),
        type: hideSaveButton ? DialogActionType.Primary : DialogActionType.Default
      }
    ] as DialogAction[]).concat(
      hideSaveButton
        ? []
        : [
            {
              text: saveText,
              handler: () => onClose(true, true),
              type: DialogActionType.Primary
            }
          ]
    )

    const show = (callback: (result: boolean) => void) => {
      setOpen(true)
      navigationCallback.current = callback
    }

    useEffect(() => {
      const currentTrigger = trigger.current
      window[currentTrigger] = show

      return () => {
        window[currentTrigger] = undefined
      }
    }, [])

    const handleTransition = (location: Location<any>) => {
      /* Ovverrides based on location should be put here */
      const triggerKey = Symbol.keyFor(trigger.current)

      return triggerKey || true
    }

    return (
      <React.Fragment>
        <ReactRouterPrompt when message={handleTransition} />
        <ConfirmDialog
          title={dialogTitle}
          open={open}
          onClose={() => onClose(false, false)}
          text={dialogText}
          actions={actions}
          size={BaseDialogSize.Small}
          className={clsx(className, styles.confirmDialog)}
        />
      </React.Fragment>
    )
  }
)

export default NavigationConfirmationDialog
