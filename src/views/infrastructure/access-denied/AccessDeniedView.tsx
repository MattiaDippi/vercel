import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import AccessDeniedViewStyles from './AccessDeniedView.styles'
import App from './../../../App'
import { Typography } from '@material-ui/core'

class AccessDeniedView extends Component<WithStyles<typeof AccessDeniedViewStyles>> {
  render() {
    const { classes } = this.props
    const { i18n } = App

    return (
      <div className={classes.container}>
        <Typography variant="body1">{i18n.t('S262')}</Typography>
      </div>
    )
  }
}

export default withStyles(AccessDeniedViewStyles)(AccessDeniedView)
