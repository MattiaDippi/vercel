import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'

const AccessDeniedViewStyles = (theme: Theme) =>
  createStyles({
    container: {
      padding: '12px 24px 24px 24px'
    }
  })

export default AccessDeniedViewStyles
