import React, { useState, useEffect, useCallback } from 'react'
import App from './../../../App'
import MainView from './../../../views/main/MainView'
import SplashScreenViewStyles from './SplashScreenView.styles'
import { CircularProgress, Typography } from '@material-ui/core'

// This component is used to initialize application
// and must not have localized strings because ReactI18nextService
// has not been initialized yet.
const SplashScreenView: React.FC = () => {
  const classes = SplashScreenViewStyles()
  const { device } = App

  const [loading, setLoading] = useState(true)
  const [appInitialized, setAppInitialized] = useState(false)

  const initializeApp = useCallback(async () => {
    const appInitialized = await App.initialize()

    setLoading(false)
    setAppInitialized(appInitialized)
  }, [])

  useEffect(() => {
    initializeApp()
  }, [initializeApp])

  return (
    <React.Fragment>
      {loading ? (
        <div className={classes.container}>
          <CircularProgress color="primary" size={32} />
        </div>
      ) : appInitialized ? (
        <MainView />
      ) : (
        <div className={classes.container}>
          <Typography variant="body1" component="div">
            {!device.cookiesEnabled
              ? 'Cookies are disabled in the application. With cookies disabled you can not access the application. Enable cookies from the browser settings, close and reopen the browser.'
              : 'An error occured. Please try again'}
          </Typography>
        </div>
      )}
    </React.Fragment>
  )
}

export default SplashScreenView
