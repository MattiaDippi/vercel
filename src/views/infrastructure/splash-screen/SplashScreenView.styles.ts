import Theme from './../../../assets/styles/themes/Theme'
import GlobalStyles from './../../../assets/styles/Global.styles'
import { makeStyles, createStyles } from '@material-ui/core/styles'

const SplashScreenViewStyles = makeStyles((theme: Theme) =>
  createStyles({
    ...GlobalStyles(theme),
    container: {
      display: 'grid',
      gridTemplateRows: '1fr',
      gridTemplateColumns: '1fr',
      alignItems: 'center',
      justifyItems: 'center',
      width: '100%',
      height: '100%'
    }
  })
)

export default SplashScreenViewStyles
