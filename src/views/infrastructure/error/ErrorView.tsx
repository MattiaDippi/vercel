import React, { Component } from 'react'
import { withStyles, WithStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import { ErrorRouteProps } from 'mw-react-web-app-infrastructure'
import App from './../../../App'
import ErrorViewStyles from './ErrorView.styles'
import { FeedbackType } from './../../../views/help/send-feedback/SendFeedbackDialog'

type ErrorViewProps = ErrorRouteProps

class ErrorView extends Component<ErrorViewProps & WithStyles<typeof ErrorViewStyles>> {
  componentDidMount() {
    const { error, errorInfo } = this.props

    if (error || errorInfo) {
      const exceptionDescription = this.getExceptionDescription(error, errorInfo)

      if (exceptionDescription) {
        const { tracker } = App
        tracker.trackException(exceptionDescription, false)
      }
    }
  }

  getExceptionDescription = (error?: any, errorInfo?: any): string => {
    let exceptionDescription = ''

    if (error) {
      exceptionDescription = error.toString()
    }

    if (errorInfo && errorInfo.componentStack) {
      exceptionDescription += errorInfo.componentStack
    }

    return exceptionDescription
  }

  onSendFeedback = () => {
    const { router } = App
    router.goToSendFeedback({ type: FeedbackType.Issue })
  }

  render() {
    const { classes, error, errorInfo } = this.props
    const { i18n } = App

    return (
      <div className={classes.container}>
        <Typography variant="h3" component="div" className={classes.title}>
          {i18n.t('S425')}
        </Typography>
        <Typography variant="body1" component="div" className={classes.subTitle}>
          {i18n.t('S423')}
        </Typography>
        <Typography variant="body1" component="div">
          {i18n.t('S424')}
        </Typography>

        <Typography variant="body1" component="div" className={classes.details}>
          <details className={classes.detailsContent}>{this.getExceptionDescription(error, errorInfo)}</details>
        </Typography>
      </div>
    )
  }
}

export default withStyles(ErrorViewStyles)(ErrorView)
