import { createStyles } from '@material-ui/core'
import Theme from './../../../assets/styles/themes/Theme'

const ErrorViewStyles = (theme: Theme) =>
  createStyles({
    container: {
      padding: '12px 24px 24px 24px'
    },
    title: {
      marginBottom: 24
    },
    subTitle: {
      marginBottom: 8
    },
    details: {
      marginTop: 24
    },
    detailsContent: {
      whiteSpace: 'pre-wrap'
    },
    sendFeedbackButton: {
      marginTop: 32
    }
  })

export default ErrorViewStyles
