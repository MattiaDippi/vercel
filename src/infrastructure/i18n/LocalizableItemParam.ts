interface LocalizableItemParam {
  value: any
  isLocalizedKey?: boolean
}

export default LocalizableItemParam
