import LocalizableItemParam from './LocalizableItemParam'

interface LocalizableItem {
  key: string
  params?: LocalizableItemParam[]
}

export default LocalizableItem
