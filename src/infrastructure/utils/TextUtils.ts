import _ from 'lodash'
import LocalizableItem from 'infrastructure/i18n/LocalizableItem'
import App from './../../App'

export const DatePickerMask = {
  char: '_',
  separator: '/'
}

class TextUtils {
  static getFullAddress(address?: string, zipCode?: string, city?: string, stateProvince?: string) {
    let fullAddress = ''

    if (address) {
      fullAddress = address

      if (zipCode) {
        fullAddress += ', ' + zipCode
      }

      if (city) {
        fullAddress += ', ' + city

        if (stateProvince) {
          fullAddress += ' (' + stateProvince + ')'
        }
      }
    }

    return fullAddress
  }

  static getUserDisplayName(lastName?: string, firstName?: string, email?: string) {
    let displayName = ''

    if (lastName && firstName) {
      displayName = firstName + ' ' + lastName
    } else if (email) {
      displayName = email
    }

    if (displayName) {
      displayName = displayName.trim()
    }

    return displayName
  }

  static getUserPicturePlaceholderText(lastName?: string, firstName?: string, emailOrFullname?: string) {
    let placeholderText = ''

    if (lastName && firstName) {
      placeholderText = firstName.trim().substring(0, 1) + lastName.trim().substring(0, 1)
    } else if (emailOrFullname) {
      if (emailOrFullname.indexOf('@') > -1) {
        const emailParts = emailOrFullname.split('@')
        placeholderText = emailParts[0].trim().substring(0, 1) + emailParts[1].trim().substring(0, 1)
      } else {
        if (emailOrFullname.indexOf(' ') > -1) {
          const fullNameParts = emailOrFullname.split(' ')
          placeholderText =
            fullNameParts[0].trim().substring(0, 1) + fullNameParts[fullNameParts.length - 1].trim().substring(0, 1)
        } else {
          placeholderText = emailOrFullname.trim().substring(0, 2)
        }
      }
    } else if (lastName) {
      placeholderText = lastName.trim().substring(0, 1)
    } else if (firstName) {
      placeholderText = firstName.trim().substring(0, 1)
    } else {
      placeholderText = ' '
    }

    placeholderText = placeholderText.toUpperCase()

    return placeholderText
  }

  static capitalize(text: string): string {
    return _.capitalize(text)
  }

  static getLocalizableItemValue(item?: LocalizableItem): string | undefined {
    let value
    const { i18n } = App

    if (item) {
      if (item.params) {
        const parsedParams = item.params.map(param => {
          return param.isLocalizedKey ? i18n.t(param.value) : param.value
        })

        value = i18n.t(item.key, false, ...parsedParams)
      } else {
        value = i18n.t(item.key)
      }
    }

    return value
  }

  static formatBytes(bytes: number, decimals = 0): string {
    if (bytes === 0) return '0 bytes'

    const k = 1024
    const dm = decimals + 1 || 3
    const sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
  }
}

export default TextUtils
