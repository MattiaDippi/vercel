import App from './../../App'
import Routes from './../../infrastructure/routing/Routes'

export enum ViewStateKey {
  ContactList = 'contactlistview_state',
  ContactDeleted = 'contactdeleted_state',
  ContactUpdated = 'contactupdated_state',
  AdvancedSearch = 'advancedsearchview_state'
}

class ViewUtils {
  static getState<T>(key: string): T | undefined {
    const { session } = App
    let viewState: T | undefined

    if (session.has(key)) {
      viewState = session.get<T>(key)
    }

    return viewState
  }

  static saveState<T>(key: string, state: T) {
    const { session } = App

    session.set<T>(key, state, false)
  }

  static restoreState<T>(key: string): T | undefined {
    const viewState: T | undefined = ViewUtils.getState<T>(key)

    if (viewState) {
      const { session } = App
      session.remove(key)
    }

    return viewState
  }

  static clearState(key: string): boolean {
    const { session } = App
    let result = false

    if (session.has(key)) {
      result = session.remove(key)
    }

    return result
  }

  static clearAllState(): boolean {
    for (const key in ViewStateKey) {
      ViewUtils.clearState(ViewStateKey[key])
    }

    return true
  }

  static routeSupportsMobileView(currentPath: string): boolean {
    const { router } = App

    // Routes for which the viewport should be set to device width
    const routesSupportingMobileView = [Routes.Auth]

    return routesSupportingMobileView.some(route => router.matchPath(currentPath, route, false))
  }
}

export default ViewUtils
