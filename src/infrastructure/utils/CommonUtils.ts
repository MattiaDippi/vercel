import _ from 'lodash'
import shortid from 'shortid'
import { RepositoryResponse, RepositoryResponseError } from 'mw-react-web-app-infrastructure'
import ReactFastCompare from 'react-fast-compare'
import guid from 'uuid/v4'

class CommonUtils {
  static debounce(callback: (...args: any) => void, wait = 500) {
    return _.debounce(callback, wait)
  }

  static throttle(callback: (...args: any) => void, wait = 500) {
    return _.throttle(callback, wait)
  }

  static deepCopy(obj: any) {
    return _.cloneDeep(obj)
  }

  static deepEqual(objA: any, objB: any): boolean {
    return ReactFastCompare(objA, objB)
  }

  static mergeRepositoryResponses<T>(
    repositoryResponses: RepositoryResponse<any>[],
    responseMapper: (dataArray: any) => T
  ): RepositoryResponse<T> {
    const response = new RepositoryResponse<T>()

    if (
      !repositoryResponses.some((r: RepositoryResponse<any>) => r.hasErrors) &&
      repositoryResponses.every((r: RepositoryResponse<any>) => r.data)
    ) {
      response.data = responseMapper(repositoryResponses.map((r: RepositoryResponse<any>) => r.data))
      const tokenResponse = repositoryResponses.find((r: RepositoryResponse<any>) => !!r.token)

      if (tokenResponse) {
        response.token = tokenResponse.token
        response.tokenValidity = tokenResponse.tokenValidity
        response.tokenNotValid = tokenResponse.tokenNotValid
      }
    } else {
      const errors: RepositoryResponseError[] = []
      repositoryResponses.forEach((r: RepositoryResponse<any>) => {
        if (r.errors && Array.isArray(r.errors)) {
          errors.concat(r.errors)
        }
      })
      response.errors = errors
    }

    return response
  }

  static getId(): string {
    return shortid.generate()
  }

  static getGuid(): string {
    return guid()
  }

  static stringToGuid(s: string): string | undefined {
    let guid

    if (s && s.length >= 32) {
      guid = `${s.substr(0, 8)}-${s.substr(8, 4)}-${s.substr(12, 4)}-${s.substr(16, 4)}-${s.substr(20)}`
    }

    return guid
  }

  static flatten(obj: any, path: any = []): any {
    return _.isPlainObject(obj) || _.isArray(obj)
      ? _.reduce(
          _.toPairs(obj),
          (acc: any, [k, v]: any): any => _.merge(acc, CommonUtils.flatten(v, [...path, k])),
          {} as any
        )
      : { [path.join('.')]: obj }
  }

  static mapObjectMissingKeysWithDefaultValues<T>(object: T | undefined | Map<string, string>, defaults: T): T {
    return Object.keys(defaults).reduce((obj, key) => {
      obj[key] = object && object[key] !== undefined ? object[key] : defaults[key]
      return obj
    }, {} as T)
  }

  static getKeyByValue(object: any, value: any) {
    return Object.keys(object).find(key => object[key] === value)
  }

  static arrayMoveMutate<T>(array: T[], fromIndex: number, toIndex: number): void {
    array.splice(toIndex < 0 ? array.length + toIndex : toIndex, 0, array.splice(fromIndex, 1)[0])
  }

  static arrayMove<T>(array: T[], fromIndex: number, toIndex: number): T[] {
    array = array.slice()
    CommonUtils.arrayMoveMutate(array, fromIndex, toIndex)
    return array
  }

  static getTimestamp(): string {
    return new Date().getTime().toString()
  }

  static getArrayChunks<T>(array: T[], chunkSize = 5): T[][] {
    const chunkedArray: T[][] = []
    for (let i = 0; i < array.length; i += chunkSize) {
      chunkedArray.push(array.slice(i, i + chunkSize))
    }
    return chunkedArray
  }

  static putRequestWithProgress = (
    url: string,
    file: Blob,
    progressCallback?: (progressPercentage: number, secondsRemaining?: number) => void
  ) => {
    const xhr = new XMLHttpRequest()

    const startTime = new Date().getTime()

    if (progressCallback) {
      xhr.upload.onprogress = (event: ProgressEvent<EventTarget>) => {
        if (event.lengthComputable) {
          const now = new Date().getTime()
          const elapsedTime = (now - startTime) / 1000

          const secondsRemaning = Math.round((event.total / event.loaded) * elapsedTime - elapsedTime)

          progressCallback(Math.round((event.loaded / event.total) * 100), secondsRemaning)
        }
      }
    }
    const response: Promise<boolean> = new Promise((resolve, reject) => {
      xhr.upload.onerror = () => reject()
      xhr.upload.onload = () => resolve(true)
    })

    xhr.open('PUT', url)
    xhr.setRequestHeader('Content-type', file.type)
    xhr.send(file)

    return { xhr, response }
  }

  static arrayInsert = <T extends {}>(arr: T[], i: number, item: T) => arr.slice(0, i).concat(item, arr.slice(i))
}

export default CommonUtils
