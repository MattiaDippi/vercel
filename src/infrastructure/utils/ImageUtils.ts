export enum PhysicalActivityImageFormat {
  Large = '_large'
}

class ImageUtils {
  static replaceUrl(stringUrl: string, suffix: string) {
    const stringParts = stringUrl.split('.')
    const extensionString = stringParts[stringParts.length - 1]
    return stringUrl.replace(`.${extensionString}`, `${suffix}.${extensionString}`)
  }

  static getImageUrlFormatPhysicalActivity(stringUrl: string, format: PhysicalActivityImageFormat) {
    if (stringUrl.includes('_large')) return stringUrl
    return ImageUtils.replaceUrl(stringUrl, format)
  }

  static base64ToBinary(base64String: string): ArrayBuffer | SharedArrayBuffer {
    const binary = atob(base64String.replace(/data:image\/.*;base64,/, ''))
    const len = binary.length
    const buffer = new ArrayBuffer(len)
    const view = new Uint8Array(buffer)

    for (let i = 0; i < len; i++) {
      view[i] = binary.charCodeAt(i)
    }

    return view.buffer
  }

  static base64toBlob(base64String: string, extension: string) {
    const byteString = atob(base64String.split(',')[1])
    const ab = new ArrayBuffer(byteString.length)
    const ia = new Uint8Array(ab)

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i)
    }
    return new Blob([ab], { type: 'image/' + extension })
  }

  static getResizedImageFileResolutions = (
    file: File,
    resolutions: string[]
  ): Promise<{ blob: Blob; resolution: string }[]> => {
    const fileReader = new FileReader()

    return new Promise((resolve, reject) => {
      fileReader.onerror = () => {
        fileReader.abort()
        reject()
      }

      fileReader.onload = () => {
        const image = new Image()

        image.onload = async () => {
          const resizedImages: { blob: Blob; resolution: string }[] = []

          let width = image.width
          let height = image.height

          for (const resolution of resolutions) {
            const canvas = document.createElement('canvas')

            const imageDimensions = resolution.split('x')
            if (imageDimensions.length === 2) {
              const w = parseInt(imageDimensions[0])
              const h = parseInt(imageDimensions[1])
              if (width > w && height > h) {
                width = w
                height = h
              }
            }
            canvas.width = width
            canvas.height = height

            const ctx = canvas.getContext('2d')
            if (ctx) {
              ctx.drawImage(image, 0, 0, width, height)

              if (!HTMLCanvasElement.prototype.toBlob) {
                const binaryString = atob(canvas.toDataURL(file.type).split(',')[1])
                const len = binaryString.length
                const arr = new Uint8Array(len)

                for (let i = 0; i < len; i++) {
                  arr[i] = binaryString.charCodeAt(i)
                }
                const blob = new Blob([arr], { type: file.type || 'image/*' })

                resizedImages.push({ blob, resolution })
              } else {
                const getBlob = (): Promise<Blob | undefined> =>
                  new Promise(resolve => {
                    ctx.canvas.toBlob(
                      blob => {
                        if (blob) {
                          resolve(blob)
                        }
                        resolve()
                      },
                      file.type,
                      0.8
                    )
                  })

                const blob = await getBlob()

                if (blob) resizedImages.push({ blob, resolution })
              }
            }
          }

          resolve(resizedImages)
        }

        image.src = fileReader.result as string
      }

      fileReader.readAsDataURL(file)
    })
  }
}

export default ImageUtils
