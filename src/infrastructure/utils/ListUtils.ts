import SortOrder from '../repository/application/models/SortOrder'
import _, { ListIterator, NotVoid } from 'lodash'

class ListUtils {
  static getPaginatedItems(items: any[], from: number, to: number) {
    return items.slice(from - 1, to)
  }

  static orderBy(items: any[], iteratee: ListIterator<any, NotVoid>, sortOrder: SortOrder) {
    return _.orderBy(items, iteratee, sortOrder === SortOrder.Asc ? 'asc' : 'desc')
  }

  static flatMap<T>(items: T[], iteratee: ListIterator<T, T[]>): T[] {
    return _.flatMap(items, iteratee)
  }
}

export default ListUtils
