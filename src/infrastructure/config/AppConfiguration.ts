import { DotEnvAppConfiguration } from 'mw-react-web-app-infrastructure'

class AppConfiguration extends DotEnvAppConfiguration {
  public apiUrlMeet: string

  constructor() {
    super()

    const hostExtension = this.getHostExtension()

    this.apiUrl = this.replaceUrlHostExtension(process.env.REACT_APP_API_URL || '', hostExtension)
    this.apiUrlMeet = this.replaceUrlHostExtension(process.env.REACT_APP_API_URL_MEET || '', hostExtension)
  }

  // Replace host extension to have correct api url for China.
  private replaceUrlHostExtension(url: string, newHostExtension: string): string {
    let newUrl = url

    try {
      const sourceUrl = new URL(url)
      const hostTokens = sourceUrl.hostname.split('.')
      const hostExtension = hostTokens[hostTokens.length - 1]

      if (newHostExtension !== hostExtension) {
        hostTokens[hostTokens.length - 1] = newHostExtension
        sourceUrl.hostname = hostTokens.join('.')

        newUrl = sourceUrl.toString().replace(/\/+$/, '')
      }
    } catch (error) {
      console.log(
        `An error occured replacing host extension for url '${url}' with new extension '${newHostExtension}'.`,
        error
      )
    }

    return newUrl
  }

  private getHostExtension(): string {
    let extension = ''

    try {
      const tokens = window.location.hostname.split('.')
      extension = tokens[tokens.length - 1]
    } catch (error) {
      console.log('An error occured getting host extension.', error)
    }

    return extension
  }
}

export default AppConfiguration
