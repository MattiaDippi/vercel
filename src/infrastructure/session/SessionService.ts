import { StorageSessionService } from 'mw-react-web-app-infrastructure'

/*
enum SessionKeys {  
}
*/

class SessionService extends StorageSessionService {
  /*
  @Override<typeof StorageSessionService>()
  public restore (): boolean {
    let result = false

    try {
      result = super.restore()

      // Restore pro session items.
    } catch (error) {
      this.logError('An error occured restoring session.', error)
    }

    return result
  }
  */
}

export default SessionService
