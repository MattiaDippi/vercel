import App from './../../../App'
import Appointment from './models/Appointment'
import AppointmentType from './models/AppointmentType'
import ClassLayoutItem from './models/ClassLayoutItem'

class CalendarMapper {
  static mapCalendarEventDetailResponse(data: any, type: AppointmentType, date: Date): Appointment {
    const { i18n } = App

    let startTime
    let endTime
    if (data.recurrence) {
      startTime = i18n.date.getWithTime(date, data.recurrence.startHour, data.recurrence.startMinutes, 0)
      endTime = i18n.date.getWithTime(date, data.recurrence.endHour, data.recurrence.endMinutes, 0)
    }

    return {
      allDay: false,
      assignedToId: data.assignedTo,
      assignedToUserId: data.assignedToUserId,
      assignedToName: data.assignedToDescription,
      date: date,
      endTime: endTime,
      id: data.id,
      maxNumberOfContacts: data.maxParticipants,
      sendReminder: false,
      startTime: startTime,
      typeId: data.eventTypeId,
      enrolledUsers: [],
      waitingUsers: [],
      instructions: data.instructions,
      title: data.name,
      pictureUrl: data.pictureUrl,
      type: type,
      liveEvent: data.liveEvent,
      roomName: data.room,
      roomId: data.roomId,
      bookingAvailable: data.bookingAvailable,
      layout: data.layout
        ? {
            columns: data.layout.cols,
            rows: data.layout.rows,
            items: data.layout.places
              ? data.layout.places.map(
                  (place: any) =>
                    ({
                      x: place.x,
                      y: place.y,
                      type: place.type,
                      label: place.label,
                      isAvailable: place.isAvailable,
                      equipmentCode: place.equipmentCode
                    } as ClassLayoutItem)
                )
              : []
          }
        : undefined

      // scheduling: {}
    }
  }
}

export default CalendarMapper
