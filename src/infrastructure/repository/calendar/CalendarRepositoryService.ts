import App from './../../../App'

import {
  DateFormatType,
  HttpMethodType,
  MywellnessCloudRepositoryService,
  RepositoryResponse
} from 'mw-react-web-app-infrastructure'
import CalendarMapper from './CalendarMapper'
import Appointment from './models/Appointment'
import GetCalendarEventDetailRequest from './models/GetCalendarEventDetailRequest'

class CalendarRepositoryService extends MywellnessCloudRepositoryService {
  public async getCalendarEventDetail(
    request: GetCalendarEventDetailRequest
  ): Promise<RepositoryResponse<Appointment>> {
    const httpRequestUrl = `${this.options.apiUrl}/${this.getCurrentFacilityUrl()}/Core/CalendarEvent/${
      request.id
    }/Details`
    const { i18n } = App
    const httpRequestContent: any = {
      partitionDate: i18n.date.format(request.date, DateFormatType.ApiPartitionDate)
    }

    const response = await this.send<GetCalendarEventDetailRequest, Appointment>(
      { method: HttpMethodType.Post, url: httpRequestUrl, content: httpRequestContent },
      data => {
        data.id = request.id
        data.date = request.date
        return CalendarMapper.mapCalendarEventDetailResponse(data, request.type, request.date)
      }
    )

    return response
  }
}

export default CalendarRepositoryService
