import ClassLayoutItemType from './ClassLayoutItemType'

interface ClassLayoutItem {
  x: number
  y: number
  type: ClassLayoutItemType
  label?: string
  isAvailable: boolean
  equipmentCode?: number
}

export default ClassLayoutItem
