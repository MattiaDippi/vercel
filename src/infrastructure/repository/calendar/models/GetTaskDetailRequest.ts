interface GetTaskDetailRequest {
  taskId: string
}

export default GetTaskDetailRequest
