import UserItem from 'infrastructure/repository/users/models/UserItem'

interface AppointmentUserItem extends UserItem {
  participated: boolean
  checkInDate?: Date
  joinDate?: Date
  station?: string
}

export default AppointmentUserItem
