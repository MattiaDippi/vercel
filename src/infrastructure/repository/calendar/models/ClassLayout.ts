import ClassLayoutItem from './ClassLayoutItem'

interface ClassLayout {
  columns: number
  rows: number
  items: ClassLayoutItem[]
}

export default ClassLayout
