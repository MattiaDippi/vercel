enum AppointmentType {
  Task = 'Task',
  Service = 'Service',
  Class = 'Class'
}

export default AppointmentType
