enum ClassLayoutItemType {
  MultipleStations = 'MultipleStations',
  Station = 'Station',
  InstructorStation = 'InstructorStation',
  Instructor = 'Instructor',
  BigScreen = 'BigScreen',
  TV = 'TV',
  Speaker = 'Speaker',
  Mirror = 'Mirror',
  Fan = 'Fan'
}

export default ClassLayoutItemType
