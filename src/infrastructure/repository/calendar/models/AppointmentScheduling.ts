import Recurrence from './Recurrence'

interface AppointmentScheduling {
  bookingAvailable: boolean
  bookingAvailableForGroups: number[]
  recurrence: Recurrence
}

export default AppointmentScheduling
