enum AppointmentCategoryType {
  TrainingProgram = 'TrainingProgram',
  Meeting = 'Meeting',
  Message = 'Message',
  Talking = 'Talking',
  Measurement = 'Measurement',
  ActionPlan = 'ActionPlan',
  Custom = 'Custom',
  Opportunity = 'Opportunity',
  PhysicalActivityTest = 'PhysicalActivityTest',
  AutomaticMessage = 'AutomaticMessage',
  Induction = 'Induction',
  Appointment = 'Appointment',
  GuestPass = 'GuestPass',
  InvitationMessage = 'InvitationMessage',
  Campaign = 'Campaign',
  Assessment = 'Assessment'
}

export default AppointmentCategoryType
