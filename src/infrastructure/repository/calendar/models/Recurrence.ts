interface Recurrence {
  monday: boolean
  tuesday: boolean
  wednesday: boolean
  thursday: boolean
  friday: boolean
  saturday: boolean
  sunday: boolean
  startDate: Date
  endDate: Date
  startTime: Date
  endTime: Date
}

export default Recurrence
