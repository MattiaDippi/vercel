import AppointmentType from './AppointmentType'

interface GetCalendarEventDetailRequest {
  id: string
  date: Date
  type: AppointmentType
}

export default GetCalendarEventDetailRequest
