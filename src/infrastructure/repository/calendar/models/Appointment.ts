import AppointmentUserItem from './AppointmentUserItem'
import AppointmentScheduling from './AppointmentScheduling'
import AppointmentType from './AppointmentType'
import ClassLayout from './ClassLayout'
import AppointmentCategoryType from './AppointmentCategoryType'

interface AppointmentExtendedInfo {
  testPhysicalActivityId?: string
  measurementDescriptorId?: string
  [key: string]: any
}

interface Appointment {
  allDay: boolean
  assignedToId: string
  assignedToUserId: string
  assignedToName: string
  color?: string
  date: Date
  endTime?: Date
  id: string
  maxNumberOfContacts: number
  sendReminder: boolean
  reminderNotes?: string
  startTime?: Date
  type: AppointmentType
  typeId: string
  instructions?: string
  notes?: string
  title: string
  pictureUrl?: string
  enrolledUsers: AppointmentUserItem[]
  waitingUsers?: AppointmentUserItem[]
  categoryType?: AppointmentCategoryType

  roomName?: string
  roomId?: string
  bookingAvailable?: boolean
  layout?: ClassLayout

  done?: boolean
  doneByStaffId?: string
  doneByStaffFullName?: string
  doneDate?: Date

  extendedInfo?: AppointmentExtendedInfo

  scheduling?: AppointmentScheduling

  liveEvent?: boolean
}

export default Appointment
