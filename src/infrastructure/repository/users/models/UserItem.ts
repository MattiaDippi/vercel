import { GenderType } from 'mw-react-web-app-infrastructure'

interface UserItem {
  id: string
  lastName?: string
  firstName?: string
  fullName?: string
  nickName?: string
  birthDate?: Date
  gender?: GenderType
  pictureUrl?: string
  email?: string
}

export default UserItem
