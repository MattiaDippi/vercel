import { MywellnessCloudRepositoryService, RepositoryResponse } from 'mw-react-web-app-infrastructure'
import AuthRepositoryService from './auth/AuthRepositoryService'
import ApplicationRepositoryService from './application/ApplicationRepositoryService'
import ForgotPasswordRequest from './auth/models/ForgotPasswordRequest'
import ChangePasswordRequest from './auth/models/ChangePasswordRequest'
import SendFeedbackRequest from './application/models/SendFeedbackRequest'
import MeetRepositoryService from './meet/MeetRepositoryService'
import JoinMeetingResponse from './meet/models/JoinMeetingResponse'
import JoinMeetingRequest from './meet/models/JoinMeetingRequest'
import CalendarRepositoryService from './calendar/CalendarRepositoryService'
import GetCalendarEventDetailRequest from './calendar/models/GetCalendarEventDetailRequest'
import Appointment from './calendar/models/Appointment'

class RepositoryService extends MywellnessCloudRepositoryService
  implements AuthRepositoryService, ApplicationRepositoryService, MeetRepositoryService, CalendarRepositoryService {
  // Auth
  forgotPassword: (request: ForgotPasswordRequest) => Promise<RepositoryResponse<boolean>> =
    AuthRepositoryService.prototype.forgotPassword

  userCanAccessDesignSystem: () => boolean = AuthRepositoryService.prototype.userCanAccessDesignSystem

  changePassword: (request: ChangePasswordRequest) => Promise<RepositoryResponse<boolean>> =
    AuthRepositoryService.prototype.changePassword

  // Application
  sendFeedback: (request: SendFeedbackRequest, userId: string) => Promise<RepositoryResponse<boolean>> =
    ApplicationRepositoryService.prototype.sendFeedback

  // Meet
  joinMeeting: (request: JoinMeetingRequest) => Promise<RepositoryResponse<JoinMeetingResponse>> =
    MeetRepositoryService.prototype.joinMeeting

  // Calendar
  getCalendarEventDetail: (request: GetCalendarEventDetailRequest) => Promise<RepositoryResponse<Appointment>> =
    CalendarRepositoryService.prototype.getCalendarEventDetail
}

export default RepositoryService
