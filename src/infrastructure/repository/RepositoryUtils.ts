import App from 'App'

class RepositoryUtils {
  static getCacheKey(key: string, cultureDependant = false, loggedUserDependant = false): string {
    const { repo, i18n } = App
    const loggedUser = repo.getLoggedUser()
    let cacheKey = key

    if (cultureDependant) {
      cacheKey += `_[culture=${i18n.currentCulture}]`
    }

    if (loggedUserDependant && loggedUser && loggedUser.id) {
      cacheKey += `_[userId=${loggedUser.id}]`
    }

    return cacheKey
  }

  static getMenuItemsCacheKey(): string {
    return RepositoryUtils.getCacheKey('MenuItems', true, true)
  }

  static getFeatureItemsCacheKey(): string {
    return RepositoryUtils.getCacheKey('FeatureItems', true, true)
  }
}

export default RepositoryUtils
