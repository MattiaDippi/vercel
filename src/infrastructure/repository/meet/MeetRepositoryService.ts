import App from './../../../App'

import { HttpMethodType, MywellnessCloudRepositoryService, RepositoryResponse } from 'mw-react-web-app-infrastructure'
import MeetMapper from './MeetMapper'
import JoinMeetingRequest from './models/JoinMeetingRequest'
import JoinMeetingResponse from './models/JoinMeetingResponse'

class MeetRepositoryService extends MywellnessCloudRepositoryService {
  public async joinMeeting(request: JoinMeetingRequest): Promise<RepositoryResponse<JoinMeetingResponse>> {
    const { config } = App

    const httpRequestUrl = `${config.apiUrlMeet}/meeting/${request.meetingId}/join`

    const httpRequestContent: any = {
      attendee: {
        nickName: request.attendee.nickName,
        firstName: request.attendee.firstName,
        lastName: request.attendee.lastName,
        privateProfile: request.attendee.privateProfile,
        pictureUrl: request.attendee.pictureUrl,
        region: request.region
      }
    }

    const response = await this.send<JoinMeetingRequest, JoinMeetingResponse>(
      { method: HttpMethodType.Put, url: httpRequestUrl, content: httpRequestContent },
      MeetMapper.mapJoinMeetingResponse
    )
    return response
  }
}

export default MeetRepositoryService
