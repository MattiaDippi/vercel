interface JoinMeetingAttendee {
  nickName: string
  firstName: string
  lastName: string
  privateProfile: boolean
  pictureUrl: string
}

export default JoinMeetingAttendee
