import JoinMeetingAttendee from './JoinMeetingAttendee'

interface JoinMeetingRequest {
  meetingId: string
  region: string
  attendee: JoinMeetingAttendee
}

export default JoinMeetingRequest
