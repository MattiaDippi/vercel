interface MeetingInfo {
  MeetingId: string
  MediaRegion: string
  MediaPlacement: {
    AudioHostUrl: string
    AudioFallbackUrl: string
    ScreenDataUrl: string
    ScreenSharingUrl: string
    ScreenViewingUrl: string
    SignalingUrl: string
    TurnControlUrl: string
  }
}

export default MeetingInfo
