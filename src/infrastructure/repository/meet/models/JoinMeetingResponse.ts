import MeetingInfo from './MeetingInfo'

interface JoinMeetingResponse {
  joinInfo: {
    meeting: MeetingInfo
    attendee: {
      ExternalUserId: string
      AttendeeId: string
      JoinToken: string
    }
  }
}

export default JoinMeetingResponse
