import JoinMeetingResponse from './models/JoinMeetingResponse'

class MeetMapper {
  static mapJoinMeetingResponse(data: any): JoinMeetingResponse {
    return {
      joinInfo: data.joinInfo
    }
  }
}

export default MeetMapper
