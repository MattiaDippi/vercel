import {
  MywellnessCloudRepositoryService,
  RepositoryResponse,
  HttpMethodType,
  EnvironmentType
} from 'mw-react-web-app-infrastructure'
import ForgotPasswordRequest from './models/ForgotPasswordRequest'
import ChangePasswordRequest from './models/ChangePasswordRequest'
import AuthMapper from './AuthMapper'
import App from './../../../App'

class AuthRepositoryService extends MywellnessCloudRepositoryService {
  public async forgotPassword(request: ForgotPasswordRequest): Promise<RepositoryResponse<boolean>> {
    const httpRequestUrl = `${this.options.apiUrl}/Application/${this.options.apiAppId}/ForgotPassword`
    const httpRequestContent: any = {
      username: request.username,
      captchaCode: request.captchaCode,
      captchaText: request.captchaText,
      recaptchaToken: request.recaptchaToken
    }

    const response = await this.send<ForgotPasswordRequest, boolean>(
      { method: HttpMethodType.Post, url: httpRequestUrl, content: httpRequestContent },
      AuthMapper.mapForgotPasswordResponse
    )

    return response
  }

  public async changePassword(request: ChangePasswordRequest): Promise<RepositoryResponse<boolean>> {
    const httpRequestUrl = `${this.options.apiUrl}/Application/${this.options.apiAppId}/ChangePassword`
    const httpRequestContent: any = {
      email: request.email,
      oldPassword: request.oldPassword,
      newPassword: request.newPassword
    }

    const response = await this.send<ChangePasswordRequest, boolean>(
      { method: HttpMethodType.Post, url: httpRequestUrl, content: httpRequestContent },
      AuthMapper.mapChangePasswordResponse
    )

    return response
  }

  public userCanAccessDesignSystem(): boolean {
    const { config } = App

    return config.environment !== EnvironmentType.Production
  }
}

export default AuthRepositoryService
