interface ForgotPasswordRequest {
  username: string
  captchaCode?: string
  captchaText?: string
  recaptchaToken: string
}

export default ForgotPasswordRequest
