interface ChangePasswordRequest {
  email: string
  oldPassword: string
  newPassword: string
}

export default ChangePasswordRequest
