import { MywellnessCloudRepositoryService, RepositoryResponse, HttpMethodType } from 'mw-react-web-app-infrastructure'
import SendFeedbackRequest from './models/SendFeedbackRequest'
import ApplicationMapper from './ApplicationMapper'

class ApplicationRepositoryService extends MywellnessCloudRepositoryService {
  public async sendFeedback(request: SendFeedbackRequest, userId: string): Promise<RepositoryResponse<boolean>> {
    const httpRequestUrl = `${this.options.apiUrl}/${this.getCurrentFacilityUrl()}/Core/User/${userId}/SendNotifyEmail`
    const httpRequestContent: any = {
      mailSubject: request.subject,
      mailBody: request.body
    }

    const response = await this.send<SendFeedbackRequest, boolean>(
      { method: HttpMethodType.Post, url: httpRequestUrl, content: httpRequestContent },
      ApplicationMapper.mapSendFeedbackResponse
    )
    return response
  }
}

export default ApplicationRepositoryService
