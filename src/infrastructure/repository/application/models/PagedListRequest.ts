interface PagedListRequest {
  from: number
  to: number
}

export default PagedListRequest
