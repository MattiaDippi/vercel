interface ListResponse<TItem> {
  items: TItem[]
}

export default ListResponse
