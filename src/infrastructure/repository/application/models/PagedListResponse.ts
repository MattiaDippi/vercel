import ListResponse from './ListResponse'

interface PagedListResponse<TItem> extends ListResponse<TItem> {
  itemsTotalCount: number
}

export default PagedListResponse
