interface SendFeedbackRequest {
  subject: string
  body: string
}

export default SendFeedbackRequest
