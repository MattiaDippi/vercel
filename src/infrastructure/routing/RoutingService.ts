import { ReactRouterRoutingService, Override, DefaultRoutes } from 'mw-react-web-app-infrastructure'
import Routes from './Routes'
import { LoginViewProps } from './../../views/auth/login/LoginView'
import { LogoutViewProps } from './../../views/auth/logout/LogoutView'
import { ForgotPasswordViewProps } from 'views/auth/forgot-password/ForgotPasswordView'
import { ChangePasswordViewProps } from 'views/auth/change-password/ChangePasswordView'
import { SendFeedbackProps } from 'views/help/send-feedback/SendFeedbackDialog'

class RoutingService extends ReactRouterRoutingService {
  // Auth
  @Override<typeof ReactRouterRoutingService>()
  public goToLogin(params?: LoginViewProps) {
    return this.goTo(DefaultRoutes.Login, params)
  }

  @Override<typeof ReactRouterRoutingService>()
  public goToLogout(params?: LogoutViewProps) {
    return this.goTo(DefaultRoutes.Logout, params)
  }

  public goToForgotPassword(params?: ForgotPasswordViewProps) {
    this.goTo(Routes.ForgotPassword, params)
  }

  public goToChangePassword(params?: ChangePasswordViewProps) {
    this.goTo(Routes.ChangePassword, params)
  }

  // Home
  public goToHome(replace = false) {
    this.goTo(Routes.Home, undefined, false, replace)
  }

  // Help
  public goToSendFeedback(params?: SendFeedbackProps) {
    this.goTo(Routes.SendFeeback, params, true)
  }
}

export default RoutingService
