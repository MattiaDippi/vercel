enum Routes {
  Auth = '/auth',
  ForgotPassword = '/auth/forgotpassword',
  ChangePassword = '/auth/changepassword',
  SessionExpired = '/auth/sessionexpired',
  Home = '/home',
  AccountPersonalData = '/account/personaldata',
  CookiesInformation = '/cookies/information',
  CookiesSettings = '/cookies/settings',
  SendFeeback = '/help/sendfeedback',
  About = '/help/about',
  LiveStreamClass = '/calendar/livestreamclass',
  DesignSystem = '/application/designsystem'
}

export default Routes
