enum Events {
  ToggleMenuRequested = 'ToggleMenuRequested',
  ToggleMenuOpen = 'ToggleMenuOpen'
}

export default Events
