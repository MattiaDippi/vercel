interface ToggleMenuRequestedEventArgs {
  elementId: string
  open: boolean
}

export default ToggleMenuRequestedEventArgs
