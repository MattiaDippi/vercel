import { PubSubEventAggregatorService } from 'mw-react-web-app-infrastructure'
import ToggleMenuRequestedEventArgs from './models/ToggleMenuRequestedEventArgs'
import Events from './models/Events'
import ToggleMenuOpenEventArgs from './models/ToggleMenuOpenEventArgs'

class EventAggregatorService extends PubSubEventAggregatorService {
  public publishToggleMenuRequested(eventArgs: ToggleMenuRequestedEventArgs): boolean {
    return this.publish<ToggleMenuRequestedEventArgs>(Events.ToggleMenuRequested, eventArgs)
  }

  public subscribeToggleMenuRequested(callback: (eventArgs: ToggleMenuRequestedEventArgs) => void): string {
    return this.subscribe(Events.ToggleMenuRequested, callback)
  }

  public publishToggleMenuOpen(eventArgs: ToggleMenuOpenEventArgs): boolean {
    return this.publish<ToggleMenuOpenEventArgs>(Events.ToggleMenuOpen, eventArgs)
  }

  public subscribeToggleMenuOpen(callback: (eventArgs: ToggleMenuOpenEventArgs) => void): string {
    return this.subscribe(Events.ToggleMenuOpen, callback)
  }
}

export default EventAggregatorService
