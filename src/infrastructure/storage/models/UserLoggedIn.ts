interface UserLoggedIn {
  lastName: string
  firstName: string
  pictureUrl: string
  username: string
  lastLogin: Date
}

export default UserLoggedIn
