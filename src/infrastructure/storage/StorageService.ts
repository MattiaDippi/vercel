import { LocalStorageService } from 'mw-react-web-app-infrastructure'

enum StorageKeys {
  TechnicalCookiesEnabled = 'technicalCookiesEnabled'
}

class StorageService extends LocalStorageService {
  get technicalCookiesEnabled(): boolean {
    const cookiesEnabled = this.getApplicationItem(StorageKeys.TechnicalCookiesEnabled)

    return cookiesEnabled !== undefined ? cookiesEnabled : true
  }

  set technicalCookiesEnabled(enabled: boolean) {
    this.setApplicationItem(StorageKeys.TechnicalCookiesEnabled, enabled)
  }
}

export default StorageService
