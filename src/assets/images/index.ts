import logoTechnogym from './logo-technogym.png'
import logoEndUser from './logo-enduser.png'
import backgroundAuthViews from './background-auth-views.jpg'

import { ReactComponent as RCiconMenuHome } from './menu-icon-home.svg'

import { ReactComponent as RCiconNotificationInfo } from './notification-icon-info.svg'
import { ReactComponent as RCiconNotificationSuccess } from './notification-icon-success.svg'
import { ReactComponent as RCiconNotificationWarning } from './notification-icon-warning.svg'
import { ReactComponent as RCiconNotificationError } from './notification-icon-error.svg'
import { ReactComponent as RCiconNotificationErrorOutlined } from './notification-icon-error-outlined.svg'

import { ReactComponent as RCiconClear } from './button-icon-clear.svg'
import { ReactComponent as RCiconBack } from './icon-back.svg'
import { ReactComponent as RCiconHide } from './icon-hide.svg'
import { ReactComponent as RCiconShow } from './icon-show.svg'
import { ReactComponent as RCiconArrowDown } from './icon-arrow-down.svg'
import { ReactComponent as RCiconArrowLarge } from './icon-arrow-large.svg'
import { ReactComponent as RCiconFeedbackIdea } from './feedback-icon-idea.svg'
import { ReactComponent as RCiconFeedbackIssue } from './feedback-icon-issue.svg'
import { ReactComponent as RCiconFilter } from './icon-filter.svg'
import { ReactComponent as RCiconMore } from './icon-filter.svg'
import { ReactComponent as RCiconClose } from './icon-close.svg'
import { ReactComponent as RCiconSortAsc } from './icon-sort-asc.svg'
import { ReactComponent as RCiconSortDesc } from './icon-sort-desc.svg'
import { ReactComponent as RCiconSearch } from './icon-search.svg'
import { ReactComponent as RCiconDown } from './icon-down.svg'
import { ReactComponent as RCiconMicOutlined } from './icon-mic-outlined.svg'
import { ReactComponent as RCiconMicOffOutlined } from './icon-mic-off-outlined.svg'
import { ReactComponent as RCiconVideoCameraOff } from './icon-video-camera-off.svg'
import { ReactComponent as RCiconVideoCameraOutlined } from './icon-video-camera-outlined.svg'
import { ReactComponent as RCiconEmail } from './icon-email.svg'
import { ReactComponent as RCiconLock } from './icon-lock.svg'

const Images = {
  logoTechnogym,
  logoEndUser,
  backgroundAuthViews,

  // Menu
  RCiconMenuHome,

  // Notifications
  RCiconNotificationInfo,
  RCiconNotificationSuccess,
  RCiconNotificationWarning,
  RCiconNotificationError,
  RCiconNotificationErrorOutlined,

  // Icons
  RCiconClear,
  RCiconBack,
  RCiconHide,
  RCiconShow,
  RCiconArrowDown,
  RCiconArrowLarge,
  RCiconFeedbackIdea,
  RCiconFeedbackIssue,
  RCiconFilter,
  RCiconMore,
  RCiconClose,
  RCiconSortAsc,
  RCiconSortDesc,
  RCiconSearch,
  RCiconDown,
  RCiconMicOutlined,
  RCiconMicOffOutlined,
  RCiconVideoCameraOff,
  RCiconVideoCameraOutlined,
  RCiconEmail,
  RCiconLock
}

export default Images
