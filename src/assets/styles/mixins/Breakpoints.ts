import Theme from './../themes/Theme'
import { isWidthDown, isWidthUp } from '@material-ui/core/withWidth'
import { Breakpoint } from '@material-ui/core/styles/createBreakpoints'

export enum BreakpointType {
  Down,
  Exact,
  Up
}

export function smallDevices(theme: Theme, comparer: BreakpointType = BreakpointType.Exact): string {
  switch (comparer) {
    case BreakpointType.Down:
    case BreakpointType.Exact:
      // width < 600px
      return theme.breakpoints.down('xs')

    case BreakpointType.Up:
      // width >= 600px
      return theme.breakpoints.up('sm')
  }
}

export function mediumDevices(theme: Theme, comparer: BreakpointType = BreakpointType.Exact): string {
  switch (comparer) {
    case BreakpointType.Down:
      // width < 600px
      return theme.breakpoints.down('xs')

    case BreakpointType.Exact:
    default:
      // width >= 600 and < 960px
      return theme.breakpoints.between('sm', 'sm')

    case BreakpointType.Up:
      // >= 960px
      return theme.breakpoints.up('md')
  }
}

export function largeDevices(theme: Theme, comparer: BreakpointType = BreakpointType.Exact): string {
  switch (comparer) {
    case BreakpointType.Down:
      // width < 960px
      return theme.breakpoints.down('sm')

    case BreakpointType.Exact:
    default:
      // width >= 960 and < 1280px
      return theme.breakpoints.between('md', 'md')

    case BreakpointType.Up:
      // >= 1280px
      return theme.breakpoints.up('lg')
  }
}

export function extraLargeDevices(theme: Theme, comparer: BreakpointType = BreakpointType.Exact): string {
  switch (comparer) {
    case BreakpointType.Down:
      // width < 1280px
      return theme.breakpoints.down('md')

    case BreakpointType.Exact:
    default:
      // width >= 1280 and < 1600px
      return theme.breakpoints.between('lg', 'lg')

    case BreakpointType.Up:
      // >= 1600px
      return theme.breakpoints.up('xl')
  }
}

export function extraExtraLargeDevices(theme: Theme, comparer: BreakpointType = BreakpointType.Exact): string {
  switch (comparer) {
    case BreakpointType.Down:
      // width < 1600px
      return theme.breakpoints.down('lg')

    case BreakpointType.Exact:
    case BreakpointType.Up:
    default:
      // width >= 1600px
      return theme.breakpoints.up('xl')
  }
}

export function isSmallDeviceDown(width: Breakpoint): boolean {
  return isWidthDown('xs', width)
}

export function isExtraExtraLargeDeviceUp(width: Breakpoint): boolean {
  return isWidthUp('xl', width, true)
}
