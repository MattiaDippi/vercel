import { CSSProperties } from '@material-ui/core/styles/withStyles'

export function lineClamp(lines: number): CSSProperties {
  return {
    lineClamp: lines,
    WebkitLineClamp: lines,
    WebkitBoxOrient: 'vertical',
    overflow: 'hidden',
    display: '-webkit-box'
  }
}
