import { Theme } from '@material-ui/core/styles/createMuiTheme'

declare module '@material-ui/core/styles/createMuiTheme' {
  // eslint-disable-next-line
  interface Theme {}

  // Allow configuration using `createMuiTheme`
  // eslint-disable-next-line
  interface ThemeOptions {}
}

declare module '@material-ui/core/styles/zIndex' {
  interface ZIndex {
    navigationMenu?: {
      drawerOffset: number
      drawerBackdropOffset: number
      quickSearchOffset: number
    }
  }
}

declare module '@material-ui/core/styles/createPalette' {
  interface TypeBackground {
    default: string
    paper: string
    light?: string
  }

  interface Palette {
    backgroundSecondary: TypeBackground
    icon: PaletteColor
    picture: PaletteColor
    border: PaletteColor
    highlight: string
    hover: string
    navigationMenu: {
      background: string
      drawerBackground: string
      drawerText: string
      drawerBorder: string
      drawerBorderActive: string
      icon: string
      iconSelected: string
      iconHover: string
      iconHoverBackground: string
      itemHover: string
      itemSelected: string
      badge: string
      divider: string
    }
    tag: {
      new: PaletteColor
    }
  }

  // Allow configuration using `createMuiTheme`
  interface PaletteOptions {
    backgroundSecondary?: TypeBackground
    icon?: PaletteColor
    picture?: PaletteColor
    border?: PaletteColor
    highlight?: string
    hover?: string
    navigationMenu?: {
      background: string
      drawerBackground: string
      drawerText: string
      drawerBorder: string
      drawerBorderActive: string
      icon: string
      iconSelected: string
      iconHover: string
      iconHoverBackground: string
      itemHover: string
      itemSelected: string
      badge: string
      divider: string
    }
    tag?: {
      new: PaletteColor
    }
  }
}

declare module '@material-ui/core/styles/createTypography' {
  interface Typography extends Record<Variant, TypographyStyle>, FontStyle, TypographyUtils {
    customTypography: {
      fontSize: number
      color: string
    }
  }

  // Allow configuration using `createMuiTheme`
  interface TypographyOptions extends Partial<Record<Variant, TypographyStyleOptions> & FontStyleOptions> {
    customTypography?: {
      fontSize?: number
      color?: string
    }
  }
}

export default Theme
