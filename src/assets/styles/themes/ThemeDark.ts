import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import createPalette, { Palette } from '@material-ui/core/styles/createPalette'
import createTypography from '@material-ui/core/styles/createTypography'
import Images from './../../../assets/images'
import ColorUtils from './../../../infrastructure/utils/ColorUtils'

export enum Colors {
  Black = '#000000',
  DarkGrey = '#1B1B1B',
  TgGrey = '#2C2C2E',
  TgGreyBlackAlpha20 = '#1B1B1B',
  TgGreyWhiteAlpha20 = '#565658',
  UserGrey = '#6F6F6F',
  Black30 = '#4D4D4D',
  Black50 = '#808080',
  Black75 = '#BFBFBF',
  IceBlue = '#ECF6FB',
  SilverGrey = '#CCCCCC',
  SilverGrey50 = '#ECECEC',
  DirtyWhite = '#F3F3F3',
  DirtyWhite50 = '#F9F9F9',
  White = '#FFFFFF',
  White50 = '#808080',
  Malibu = '#43AAE0',
  PaleBlueSky = '#C6E5F5',
  Cherry = '#FF3B30',
  Cherry10 = '#FF453A',
  Fern = '#55B454',
  SunYellow = '#FFE01E',
  SunYellow75 = '#FFE756',
  Transparent = 'transparent'
}

const borderRadius = 8

const palette: Palette = createPalette({
  getContrastText: (color: string): string => {
    return ColorUtils.backgroundColorRequiresDarkContent(color) ? Colors.Black : Colors.White
  },
  background: {
    default: Colors.Black,
    paper: Colors.TgGrey
  },
  backgroundSecondary: {
    default: Colors.DarkGrey,
    paper: Colors.Black,
    light: Colors.Black50
  },
  text: {
    primary: Colors.White,
    secondary: Colors.White50
  },
  primary: {
    main: Colors.SunYellow,
    contrastText: Colors.Black,
    light: Colors.SunYellow75
  },
  secondary: {
    main: Colors.Black,
    contrastText: Colors.White,
    light: Colors.Black75
  },
  success: {
    main: Colors.Fern,
    light: Colors.Fern,
    dark: Colors.Fern,
    contrastText: Colors.White
  },
  info: {
    main: Colors.Malibu,
    light: Colors.Malibu,
    dark: Colors.Malibu,
    contrastText: Colors.White
  },
  warning: {
    main: Colors.SunYellow,
    light: Colors.SunYellow,
    dark: Colors.SunYellow,
    contrastText: Colors.White
  },
  error: {
    main: Colors.Cherry,
    light: Colors.Cherry10,
    dark: Colors.Cherry,
    contrastText: Colors.White
  },
  icon: {
    main: Colors.DirtyWhite,
    light: Colors.DarkGrey,
    dark: Colors.Black50,
    contrastText: Colors.White
  },
  picture: {
    dark: Colors.SilverGrey50,
    main: Colors.SilverGrey50,
    light: Colors.SilverGrey50,
    contrastText: Colors.Black
  },
  divider: Colors.DarkGrey,
  border: {
    main: Colors.SilverGrey,
    light: Colors.SilverGrey50,
    dark: Colors.Black30,
    contrastText: Colors.White
  },
  highlight: Colors.PaleBlueSky,
  hover: Colors.IceBlue,
  action: {
    active: Colors.White
  },
  navigationMenu: {
    background: Colors.White,
    drawerBackground: Colors.DirtyWhite50,
    drawerText: Colors.Black,
    drawerBorder: Colors.SilverGrey50,
    drawerBorderActive: Colors.Black50,
    icon: ColorUtils.hexToRgbString(Colors.Black, 0.3),
    iconSelected: Colors.Black,
    iconHover: ColorUtils.hexToRgbString(Colors.White, 0.5),
    iconHoverBackground: Colors.Black75,
    itemHover: Colors.DarkGrey,
    itemSelected: Colors.PaleBlueSky,
    badge: Colors.SunYellow,
    divider: Colors.Black75
  },
  tag: {
    new: {
      main: Colors.SunYellow,
      light: Colors.SunYellow,
      dark: Colors.SunYellow,
      contrastText: Colors.Black
    }
  }
})

const typography = createTypography(palette, {
  fontFamily: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    '"Roboto"',
    '"Oxygen"',
    '"Ubuntu"',
    '"Cantarell"',
    '"Fira Sans"',
    '"Droid Sans"',
    '"Arial"',
    '"sans-serif"'
  ].join(','),
  // Tell Material-UI what's the font-size on the html element is.
  htmlFontSize: 10,
  h1: {
    fontSize: '4.2rem',
    fontWeight: 700,
    lineHeight: 'normal'
  },
  h2: {
    fontSize: '3.6rem',
    fontWeight: 600,
    lineHeight: 'normal'
  },
  h3: {
    fontSize: '2.4rem',
    fontWeight: 700,
    lineHeight: 'normal'
  },
  h4: {
    fontSize: '2.4rem',
    fontWeight: 600,
    lineHeight: 'normal'
  },
  h5: {
    fontSize: '2.0rem',
    fontWeight: 700,
    lineHeight: 'normal'
  },
  h6: {
    fontSize: '2.0rem',
    fontWeight: 600,
    lineHeight: 'normal'
  },
  body1: {
    fontSize: '1.6rem',
    lineHeight: 'normal'
  },
  body2: {
    fontSize: '1.4rem',
    lineHeight: 'normal'
  },
  caption: {
    fontSize: '1.2rem',
    lineHeight: 'normal'
  },
  button: {
    fontSize: '1.6rem',
    fontWeight: 700,
    lineHeight: '2rem',
    textTransform: 'none'
  },
  overline: {
    fontSize: '1.2rem',
    fontWeight: 700,
    textTransform: 'none',
    lineHeight: 'normal'
  },
  subtitle1: {
    fontSize: '1.4rem',
    fontWeight: 700,
    lineHeight: 'normal'
  },
  subtitle2: {
    fontSize: '1.2rem',
    fontWeight: 700,
    lineHeight: 'normal'
  }
})

const ThemeLight = createMuiTheme({
  palette: palette,
  typography: typography,
  shape: {
    borderRadius: borderRadius
  },
  zIndex: {
    navigationMenu: {
      drawerOffset: 2,
      drawerBackdropOffset: 3,
      quickSearchOffset: 1
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1600
    }
  },
  shadows: [
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none'
  ],
  overrides: {
    MuiInputBase: {
      input: {
        height: '2.2rem',
        '&.Mui-disabled': {
          color: ColorUtils.hexToRgbString(Colors.White, 0.6)
        }
      }
    },
    MuiInput: {
      root: {
        '&$disabled': {
          color: Colors.White50
        }
      },
      underline: {
        '&:before': {
          borderBottom: `1px solid ${Colors.White}`
        }
      }
    },
    MuiInputLabel: {
      root: {
        fontSize: typography.body2.fontSize,
        color: Colors.Black50
      },
      shrink: {
        textTransform: 'uppercase',
        fontWeight: 700,
        color: Colors.White50,
        transform: 'translate(0, 1.5px) scale(0.857)'
      }
    },
    MuiOutlinedInput: {
      input: {
        height: 'unset'
      }
    },
    MuiFilledInput: {
      root: {
        backgroundColor: Colors.TgGrey,
        border: `1px solid ${Colors.TgGrey}`,
        borderRadius: borderRadius,
        height: 40,
        minHeight: 40,
        lineHeight: '40px',
        '&:hover': {
          backgroundColor: Colors.TgGreyBlackAlpha20
        },
        '&.Mui-focused': {
          backgroundColor: Colors.TgGreyWhiteAlpha20,
          border: `1px solid ${ColorUtils.hexToRgbString(Colors.White, 0.5)}`
        },
        '&.Mui-disabled': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.TgGrey, 0.8),
          color: ColorUtils.hexToRgbString(Colors.White, 0.5)
        },
        '&.Mui-error': {
          border: `1px solid ${ColorUtils.hexToRgbString(Colors.Cherry, 0.6)}`
        }
      },
      input: {
        padding: '0 10px'
      },
      underline: {
        '&:before': {
          borderBottom: 0
        },
        '&:after': {
          borderBottom: 0
        },
        '&:hover': {
          '&:before': {
            borderBottom: 0
          }
        }
      }
    },
    MuiInputAdornment: {
      root: {
        '& path': {
          fill: palette.text.primary
        }
      },
      filled: {
        '&.MuiInputAdornment-positionStart:not(.MuiInputAdornment-hiddenLabel)': {
          marginTop: 0
        }
      }
    },
    MuiLink: {
      root: {
        color: palette.action.active
      }
    },
    MuiMenu: {
      list: {
        padding: 0
      }
    },
    MuiMenuItem: {
      root: {
        minHeight: 48,
        fontSize: typography.body1.fontSize,
        fontWeight: typography.body1.fontWeight,
        color: ColorUtils.hexToRgbString(Colors.White, 0.6),
        borderBottom: `1px solid ${Colors.DarkGrey}`,
        '&.Mui-selected': {
          backgroundColor: Colors.White,
          color: Colors.Black,
          borderBottom: `1px solid ${Colors.White}`,
          '&:hover': {
            backgroundColor: Colors.White
          }
        },
        '&:hover': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.White, 0.6),
          color: Colors.Black
        },
        '@media (min-width: 600px)': {
          minHeight: 48
        }
      }
    },
    MuiListItem: {
      root: {
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 16,
        paddingRight: 16
      }
    },
    MuiButton: {
      root: {
        minWidth: 100,
        minHeight: 40,
        padding: '8px 20px',
        '& path': {
          fill: palette.text.primary
        },
        '&.Mui-disabled': {
          '& path': {
            fill: ColorUtils.hexToRgbString(Colors.White, 0.5)
          }
        }
      },
      sizeSmall: {
        fontSize: typography.button.fontSize,
        minHeight: 32,
        padding: '6px 12px'
      },
      contained: {
        color: palette.text.primary,
        backgroundColor: Colors.TgGrey,
        '&:hover': {
          backgroundColor: Colors.TgGreyBlackAlpha20
        },
        '&.Mui-disabled': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.TgGrey, 0.8),
          color: ColorUtils.hexToRgbString(Colors.White, 0.5)
        }
      },
      containedPrimary: {
        backgroundColor: palette.primary.main,
        color: palette.primary.contrastText,
        '&:hover': {
          backgroundColor: palette.primary.light
        }
      },
      containedSecondary: {
        backgroundColor: palette.secondary.main,
        color: palette.secondary.contrastText,
        '&:hover': {
          backgroundColor: palette.secondary.light
        }
      },
      outlined: {
        padding: '10px 20px',
        borderColor: Colors.White,
        color: Colors.White,
        '&:hover': {
          borderColor: Colors.SilverGrey,
          backgroundColor: ColorUtils.hexToRgbString(Colors.Black, 0.1)
        },
        '&.Mui-disabled': {
          color: ColorUtils.hexToRgbString(Colors.White, 0.3)
        }
      },
      outlinedPrimary: {
        borderColor: Colors.SunYellow,
        '&:hover': {
          borderColor: Colors.SunYellow,
          backgroundColor: ColorUtils.hexToRgbString(Colors.White, 0.1)
        }
      },
      outlinedSecondary: {
        color: Colors.White,
        borderColor: Colors.White,
        '&:hover': {
          borderColor: Colors.White50,
          backgroundColor: ColorUtils.hexToRgbString(Colors.White, 0.1)
        }
      },
      text: {
        padding: '10px 20px',
        '&:hover': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.Black, 0.1)
        },
        '&.Mui-disabled': {
          color: ColorUtils.hexToRgbString(Colors.Black, 0.3)
        }
      },
      textPrimary: {
        '&:hover': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.Black, 0.1)
        }
      },
      textSecondary: {
        '&:hover': {
          backgroundColor: ColorUtils.hexToRgbString(Colors.Black, 0.1)
        }
      }
    },
    MuiDialogTitle: {
      root: {
        padding: '44px 64px 24px 64px'
      }
    },
    MuiDialogContent: {
      root: {
        padding: '0 64px'
      }
    },
    MuiDialogActions: {
      spacing: {
        padding: 24,
        '& > * + *': {
          marginLeft: 16
        }
      }
    },
    MuiSelect: {
      icon: {
        color: palette.text.primary,
        fill: palette.text.primary,
        '& path': {
          fill: palette.text.primary
        },
        '&.Mui-disabled': {
          fill: ColorUtils.hexToRgbString(Colors.White, 0.6),
          '& path': {
            fill: ColorUtils.hexToRgbString(Colors.White, 0.6)
          }
        }
      }
    },
    MuiPopover: {
      paper: {
        backgroundColor: Colors.TgGrey
      }
    },
    MuiTabs: {
      root: {
        minHeight: 40
      },
      flexContainer: {
        minHeight: 40
      },
      indicator: {
        height: 2,
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        '& > div': {
          height: 2,
          maxWidth: 40,
          width: '100%',
          backgroundColor: palette.primary.main
        }
      }
    },
    MuiTab: {
      root: {
        minHeight: 28,
        ...typography.body1,
        '@media (min-width: 960px)': {
          ...typography.body1
        },
        '&$selected': {
          ...typography.button
        }
      }
    },
    MuiAccordion: {
      root: {
        boxShadow: 'unset',
        margin: 0,
        '&.Mui-expanded': {
          margin: 0
        }
      }
    },
    MuiAccordionSummary: {
      root: {
        minHeight: '20px',
        margin: '8px 0',
        padding: '0 16px',
        '&.Mui-expanded': {
          minHeight: '20px'
        }
      },
      content: {
        margin: 0,
        '&.Mui-expanded': {
          margin: 0
        }
      },
      expandIcon: {
        padding: 4
      }
    },
    MuiAccordionDetails: {
      root: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 8,
        paddingBottom: 8
      }
    },
    MuiSwitch: {
      switchBase: {
        '&.Mui-checked + .MuiSwitch-track': {
          opacity: 1
        },
        '&.Mui-checked .MuiSwitch-thumb': {
          boxShadow: `2px 2px 4px 4px ${ColorUtils.hexToRgbString(Colors.Black, 0.2)}`
        }
      },
      root: {
        width: 60,
        height: 40,
        padding: '9px 12px 11px 12px'
      },
      track: {
        backgroundColor: Colors.TgGrey,
        opacity: 1,
        borderRadius: 10,
        '&:hover': {
          backgroundColor: Colors.TgGreyBlackAlpha20
        }
      },
      colorPrimary: {
        '&.Mui-checked': {
          color: Colors.White
        }
      }
    }
  },
  props: {
    MuiLink: {
      underline: 'always'
    },
    MuiCircularProgress: {
      size: 32
    },
    MuiTabs: {
      textColor: 'primary'
    },
    MuiSelect: {
      IconComponent: Images.RCiconArrowDown
    }
  }
})

export default ThemeLight
