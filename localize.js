const fs = require('fs')
const yauzl = require('yauzl')
const request = require('request-promise-native')
const { LokaliseApi } = require('@lokalise/node-api')

const localizeApiKey = '1e9eab1dd94c08a96b0b89753b7824b81d59970a'
const localizeProjectId = '597092555cd5337fe6eab9.37474713'
const localizationStringsPath = './public/static/i18n'
const localizationCacheFilePath = './src/infrastructure/i18n/CultureFilesVersion.ts'

const lokaliseApi = new LokaliseApi({ apiKey: localizeApiKey })

const pad2 = n => (n < 10 ? '0' + n : n)
const formatDate = date =>
  date.getFullYear().toString() +
  pad2(date.getMonth() + 1) +
  pad2(date.getDate()) +
  pad2(date.getHours()) +
  pad2(date.getMinutes()) +
  pad2(date.getSeconds())

const downloadLocalizationStrings = async () => {
  const response = await lokaliseApi.files.download(localizeProjectId, {
    format: 'json',
    original_filenames: false,
    bundle_structure: '%LANG_ISO%.%FORMAT%',
    export_sort: 'a_z',
    export_empty_as: 'skip',
    plural_format: 'array',
    placeholder_format: 'icu',
    indentation: '2sp',
    replace_breaks: false,
    language_mapping: [
      {
        original_language_iso: 'en_GB',
        custom_language_iso: 'en-GB'
      },
      {
        original_language_iso: 'zh_CN',
        custom_language_iso: 'zh-CN'
      },
      {
        original_language_iso: 'zh_TW',
        custom_language_iso: 'zh-TW'
      },
      {
        original_language_iso: 'da',
        custom_language_iso: 'da-DK'
      },
      {
        original_language_iso: 'nl',
        custom_language_iso: 'nl-NL'
      },
      {
        original_language_iso: 'en',
        custom_language_iso: 'en-US'
      },
      {
        original_language_iso: 'fi',
        custom_language_iso: 'fi-FI'
      },
      {
        original_language_iso: 'fr',
        custom_language_iso: 'fr-FR'
      },
      {
        original_language_iso: 'de',
        custom_language_iso: 'de-DE'
      },
      {
        original_language_iso: 'it',
        custom_language_iso: 'it-IT'
      },
      {
        original_language_iso: 'ja',
        custom_language_iso: 'ja-JP'
      },
      {
        original_language_iso: 'ko',
        custom_language_iso: 'ko-KR'
      },
      {
        original_language_iso: 'no',
        custom_language_iso: 'nb-NO'
      },
      {
        original_language_iso: 'pt_BR',
        custom_language_iso: 'pt-BR'
      },
      {
        original_language_iso: 'ro',
        custom_language_iso: 'ro-RO'
      },
      {
        original_language_iso: 'ru',
        custom_language_iso: 'ru-RU'
      },
      {
        original_language_iso: 'es',
        custom_language_iso: 'es-ES'
      },
      {
        original_language_iso: 'sv',
        custom_language_iso: 'sv-SE'
      },
      {
        original_language_iso: 'tr',
        custom_language_iso: 'tr-TR'
      }
    ]
  })

  if (response && response.bundle_url) {
    const body = await request({
      method: 'GET',
      uri: response.bundle_url,
      encoding: null
    })

    yauzl.fromBuffer(body, { lazyEntries: true }, function (err, zipfile) {
      if (err) throw err
      zipfile.readEntry()
      zipfile
        .on('entry', function (entry) {
          if (/\/$/.test(entry.fileName)) {
            zipfile.readEntry()
          } else {
            var writeStream = fs.createWriteStream(localizationStringsPath + '/' + entry.fileName)
            zipfile.openReadStream(entry, function (err, readStream) {
              if (err) throw err
              readStream.on('end', function () {
                zipfile.readEntry()
              })
              readStream.pipe(writeStream)
            })
          }
        })
        .once('end', () => {
          fs.writeFileSync(
            localizationCacheFilePath,
            `export const i18nCultureFilesVersion = '${formatDate(new Date())}'\n`
          )
        })
    })
  } else {
    throw new Error('Something went wrong while trying to download localization strings.')
  }
}

downloadLocalizationStrings()
