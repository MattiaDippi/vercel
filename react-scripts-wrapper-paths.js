// This is a workaround to change build output folder without ejecting react scripts.
// REACT_APP_ENVIRONMENT in a variable defined in .env files.
const paths = require('react-scripts/config/paths')
const path = require('path')

paths.appBuild = path.resolve(paths.appPath, `builds/${process.env.REACT_APP_ENVIRONMENT}`)

module.exports = paths
