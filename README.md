# skillathletic web

> mywellness professional progressive web app built with React

## IDE Setup

In Visual Studio Code click the Extensions button in the Activity Bar and install the following extensions:
Babel ES6/ES7
Better Comments
Debugger for Chrome
DotENV
ESLint
Prettier Code Formatter

## Setup

```bash
# map skillathleticlocal.mywellness.com to 127.0.0.1 in hosts file

# create .env.local in root folder and copy this settings
REACT_APP_DEFAULT_ACCOUNT_USERNAME=greenbrooke@mwcloud.com
REACT_APP_DEFAULT_ACCOUNT_PASSWORD=tgsTGS123
HOST=skillathleticlocal.mywellness.com
PORT=14443
HTTPS=true
BROWSER=chrome

# install dependencies
npm install

# serve with hot reload at skillathleticlocal.mywellness.com
npm start
```

## Testing, linting and formatting

````bash
# run all tests
npm run test

# run lint
npm run lint

# run lint and autofix problems
npm run lint:fix

## Build

```bash
# build for development environment
npm run build:development

# build for test environment
npm run build:test

# build for production environment
npm run build:production
````

## Analyze

```bash
# Analyzes development build JavaScript bundles using the source maps. This helps you understand where code bloat is coming from.
npm run analyze:development

# Analyzes test build JavaScript bundles using the source maps. This helps you understand where code bloat is coming from.
npm run analyze:test

# Analyzes production build JavaScript bundles using the source maps. This helps you understand where code bloat is coming from.
npm run analyze:production
```

## Deploy

```bash

```